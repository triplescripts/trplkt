import { test } from "$TEST_HELPER";

import { Token } from "$PROJECT/src/Token.src";
import { TokenScanner } from "$PROJECT/src/TokenScanner.src";

test(() => {
  let error, text = "" +
         "/////////////////////////////////////////////////////////////////" +
  '\n' + "/// import" +
  '\n' + "//////////" +
  '\n' + "//? <script>" +
  '\n' + "////";

  let scanner = new TokenScanner(text, null, ((which, ...$) => {
    error = which
  }));

  scanner.keepWhitespace = false;
  scanner.keepCommentns = true;

  test.is(scanner.readNextToken(), Token.COMMENT);
  test.is(scanner.readNextToken(), Token.IMPORT);
  test.is(scanner.readNextToken(), Token.COMMENT);
  test.is(scanner.readNextToken(), Token.OPEN_DELIMITER);
  test.is(scanner.readNextToken(), Token.COMMENT);
  test.ok(!scanner.readNextToken());

  test.is(error, undefined);
})
