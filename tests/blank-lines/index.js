import { test } from "$TEST_HELPER";

import { SourceProcessor } from "$PROJECT/src/SourceProcessor.src";
import { Token } from "$PROJECT/src/Token.src";

test(function assert_test_mods_are_kosher() {
  let instance = new SourceProcessor("");
  test.ok(!("nextTokenIs" in instance));
  test.ok(!("nextTokenIsIdentifier" in instance));
})

function SourceProcessorForTesting(...args) {
  let result = new SourceProcessor(...args);
  result.nextTokenIs =
    nextTokenIs.bind(result, result);
  result.nextTokenIsIdentifier =
    nextTokenIsIdentifier.bind(result, result);
  return result;
}

function nextTokenIs(source, kind) {
  test.is(source.nextToken(), kind);
}

function nextTokenIsIdentifier(source, name) {
  test.is(source.nextToken(), Token.IDENTIFIER_NAME);
  test.is(source.token.content, name);
}

test(function no_stanzas() {
  let text =
         "foo" +
  '\n' + "bar" +
  '\n' + "baz" +
  '\n';

  let source = new SourceProcessorForTesting(text);

  source.nextTokenIsIdentifier("foo");

  test.is(source.nextToken(true), Token.WHITESPACE);
  test.is(source.isAtNewStanza(), false);
  test.is(source.getMarginTail(), null);

  source.nextTokenIsIdentifier("bar");

  test.is(source.nextToken(true), Token.WHITESPACE);
  test.is(source.isAtNewStanza(), false);
  test.is(source.getMarginTail(), null);

  source.nextTokenIsIdentifier("baz");

  test.is(source.nextToken(true), Token.WHITESPACE);
  test.is(source.isAtNewStanza(), false);
  test.is(source.getMarginTail(), null);

  let eof = source.nextToken();
  test.is(eof, null);
})

test(function with_stanzas() {
  let text =
         "foo" +
  '\n' +
  '\n' + "bar" +
  '\n' +
  '\n' +
  '\n' + "baz" +
  '\r\n' +
  '\n' + "fum" +
  '\n';

  let source = new SourceProcessorForTesting(text);

  source.nextTokenIsIdentifier("foo");

  test.is(source.nextToken(true), Token.WHITESPACE);
  test.is(source.isAtNewStanza(), false);
  test.is(source.getMarginTail(), null);
  test.is(source.nextToken(true), Token.WHITESPACE);
  test.is(source.isAtNewStanza(), false);
  test.is(source.getMarginTail(), null);

  source.nextTokenIsIdentifier("bar");
  test.is(source.isAtNewStanza(), true);
  test.is(source.getMarginTail().position, text.indexOf("\nbar"));

  test.is(source.nextToken(true), Token.WHITESPACE);
  test.is(source.isAtNewStanza(), false);
  test.is(source.getMarginTail(), null);
  test.is(source.nextToken(true), Token.WHITESPACE);
  test.is(source.isAtNewStanza(), false);
  test.is(source.getMarginTail(), null);
  test.is(source.nextToken(true), Token.WHITESPACE);
  test.is(source.isAtNewStanza(), false);
  test.is(source.getMarginTail(), null);

  source.nextTokenIsIdentifier("baz");
  test.is(source.isAtNewStanza(), true);
  test.is(source.getMarginTail().position, text.indexOf("\nbaz"));

  test.is(source.nextToken(true), Token.WHITESPACE);
  test.is(source.isAtNewStanza(), false);
  test.is(source.getMarginTail(), null);
  test.is(source.nextToken(true), Token.WHITESPACE);
  test.is(source.isAtNewStanza(), false);
  test.is(source.getMarginTail(), null);

  source.nextTokenIsIdentifier("fum");
  test.is(source.isAtNewStanza(), true);
  test.is(source.getMarginTail().position, text.indexOf("\nfum"));

  let eof = source.nextToken();
  test.is(eof, null);
})

// Our processor implementation should also work even if we're not overriding
// the 'allow-whitespace?' parameter to `nextToken` and instead we're leaping
// from one non-whitespace token to the next.
test(function with_stanzas_no_intermediate_whitespace_checks() {
  let text =
         "foo" +
  '\n' +
  '\n' + "bar" +
  '\n' +
  '\n' +
  '\n' + "baz" +
  '\n';

  let source = new SourceProcessorForTesting(text);

  source.nextTokenIsIdentifier("foo");
  source.nextTokenIsIdentifier("bar");
  test.is(source.isAtNewStanza(), true);
  source.nextTokenIsIdentifier("baz");
  test.is(source.isAtNewStanza(), true);

  let eof = source.nextToken();
  test.is(eof, null);
})

test(function eof_is_not_false_stanza() {
  let text =
         "foo" +
  '\n' +
  '\n' + "bar" +
  '\n' +
  '\n' +
  '\n';

  let source = new SourceProcessorForTesting(text);

  source.nextTokenIsIdentifier("foo");
  source.nextTokenIsIdentifier("bar");
  test.is(source.isAtNewStanza(), true);

  let eof = source.nextToken();
  test.is(source.isAtNewStanza(), false);
  test.is(eof, null);
})

test(function module_can_start_with_a_blank_line() {
  let text =
         "" +
  '\n' + "export" +
  '\n' + "class Cstdio {" +
  '\n' + "  lol() {" +
  '\n' + "    console.log(\"this isn't Cstdio\");" +
  '\n' + "  }" +
  '\n' + "}" +
  '\n';

  let source = new SourceProcessorForTesting(text);
  test.is(source.nextToken(), Token.EXPORT);
})
