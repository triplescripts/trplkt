import { test, read } from "$TEST_HELPER";

import { SourceProcessor } from "$PROJECT/src/SourceProcessor.src";

test(function column_0_import() {
  const RUNAWAY_TOKEN_COUNT_CUTOFF = 10;
  return read("$PROJECT/tests/line-violations/GPL-3.0.src").then((text) => {
    let found = 0;
    let source = new SourceProcessor(text);
    try {
      while (found < RUNAWAY_TOKEN_COUNT_CUTOFF &&
             source.nextToken(false) != null) {
        ++found;
      }
    } catch (ex) {
      var exception = ex;
    }

    test.ok(found < RUNAWAY_TOKEN_COUNT_CUTOFF);

    test.ok(exception != undefined);
    test.ok(exception instanceof SourceProcessor.LineViolation);
  })
})
