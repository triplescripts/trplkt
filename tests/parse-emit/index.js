import { test, read } from "$TEST_HELPER";

import { Emitter } from "$PROJECT/src/Emitter.src";
import { InputIR } from "$PROJECT/src/InputIR.src";
import { RealParser } from "$PROJECT/src/RealParser.src";

function check($fn) {
  try {
    $fn();
  } catch (ex) {
    return ex;
  }
  return null;
}

test(function () {
  let text =
         "/// <"+"script>" +
  '\n' + "/// export" +
  '\n' + "let foo = 'bar';" +
  '\n' +
  '\n' + "/// import { Baz } from \"./Baz.src\"" +
  '\n' +
  '\n' + "/// $path$: \"extra/foo.src\"" +
  '\n' + "/// <"+"/script>" +
  '\n';

  let parser = new RealParser();
  let [ module ] = parser.ingestCompilationText(text);

  test.is(parser.modules.length, 1);
  test.is(parser.modules[0], module);

  let emitter = Emitter.from(parser.modules);
  test.is(emitter.textify(InputIR.FORM_TRIPLE), text);
  test.is(emitter.textify(InputIR.FORM_NORMAL),
           "export" +
    '\n' + "let foo = 'bar';" +
    '\n' +
    '\n' + "import { Baz } from \"./Baz.src\"" +
    '\n'
  );
})

test(function block_style() {
  let parser, text =
         "/// <"+"script>" +
  '\n' + "/// export" +
  '\n' + "let foo = 'bar';" +
  '\n' +
  '\n' + "/// import { Baz } from \"./Baz.src\"" +
  '\n' +
  '\n' + "/// $path$: \"extra/foo.src\"" +
  '\n' + "/// <"+"/script>" +
  '\n';

  parser = new RealParser();
  test.ok(check(() => (
    parser.ingestCompilationText(text, InputIR.GBlock)
  )));

  text =
         "/// <"+"script>" +
  '\n' + "/// export" +
  '\n' + "let foo = 'bar';" +
  '\n' +
  '\n' + "/// import { Baz } from \"./Baz.src\"" +
  '\n' +
  '\n' + "/// $path$: \"extra/foo.src\"" +
  '\n' + "/// <"+"/script>" +
  '\n';

  parser = new RealParser();
  test.ok(!check(() => (
    parser.ingestCompilationText(text, InputIR.TBlock)
  )));

  text =
         "//? <"+"script>" +
  '\n' + "/// export" +
  '\n' + "let foo = 'bar';" +
  '\n' +
  '\n' + "/// import { Baz } from \"./Baz.src\"" +
  '\n' +
  '\n' + "/// $path$: \"extra/foo.src\"" +
  '\n' + "//? <"+"/script>" +
  '\n';

  parser = new RealParser();
  test.ok(check(() => (
    parser.ingestCompilationText(text, InputIR.TBlock)
  )));

  text =
         "//? <"+"script>" +
  '\n' + "/// export" +
  '\n' + "let foo = 'bar';" +
  '\n' +
  '\n' + "/// import { Baz } from \"./Baz.src\"" +
  '\n' +
  '\n' + "/// $path$: \"extra/foo.src\"" +
  '\n' + "//? <"+"/script>" +
  '\n';

  parser = new RealParser();
  test.ok(!check(() => (
    parser.ingestCompilationText(text, InputIR.GBlock)
  )));
})

test(function pathed_and_pathless() {
  let text =
         "export" +
  '\n' + "let foo = 'bar';" +
  '\n' +
  '\n' + "import { Baz } from \"./Baz.src\"" +
  '\n';

  let parser = new RealParser();
  let module = parser.ingestNormalText(text);

  test.is(parser.modules.length, 1);
  test.is(parser.modules[0], module);
  test.is(module.path, null);

  let emitter = new Emitter();
  let emittee = emitter.prepareForOutput(module);
  test.is(emitter.textify(InputIR.FORM_NORMAL), text);
  test.is(emitter.textify(InputIR.FORM_TRIPLE),
           "/// <"+"script>" +
    '\n' + "/// export" +
    '\n' + "let foo = 'bar';" +
    '\n' +
    '\n' + "/// import { Baz } from \"./Baz.src\"" +
    '\n' + "/// <"+"/script>" +
    '\n'
  );

  emittee.addAttribute("path", "extra/foo.src");
  test.is(emitter.textify(InputIR.FORM_TRIPLE),
           "/// <"+"script>" +
    '\n' + "/// export" +
    '\n' + "let foo = 'bar';" +
    '\n' +
    '\n' + "/// import { Baz } from \"./Baz.src\"" +
    '\n' +
    '\n' + "/// $path$: \"extra/foo.src\"" +
    '\n' + "/// <"+"/script>" +
    '\n'
  );
  // ... and even with the synthetic attribute, normal form should still end
  // up looking the same.
  test.is(emitter.textify(InputIR.FORM_NORMAL), text);
})

test(function () {
  const srcFile = "$PROJECT/tests/parse-emit/emitter/Emitter.src";
  const outFile = "$PROJECT/tests/parse-emit/emitter/Emitter.out";

  let src, out, emitter, parser = new RealParser();
  return read(srcFile).then((contents) => {
    src = contents;
    return read(outFile).then((contents) => {
      out = contents;

      parser.ingestNormalText(src);
      emitter = Emitter.from(parser.modules);

      test.is(emitter.textify(InputIR.FORM_TRIPLE), out);

      parser = new RealParser();
      parser.ingestCompilationText(out);
      emitter = Emitter.from(parser.modules);

      test.is(emitter.textify(InputIR.FORM_NORMAL), src);
    });
  });
})

test(function multiple_blocks() {
  let text, parser, exception = null;

  text =
         "/// <"+"script>" +
  '\n' + "/// export" +
  '\n' + "let foo = 'bar';" +
  '\n' + "/// <"+"/script>" +
  '\n' + "/// <"+"script>" +
  '\n' + "void function main() { } ()" +
  '\n' + "/// <"+"/script>" +
  '\n';

  parser = new RealParser();

  exception = check(() => {
    parser.ingestCompilationText(text);
  });

  test.ok(!exception);
  test.is(parser.modules.length, 2);

  text =
         "//? <"+"script>" +
  '\n' + "/// export" +
  '\n' + "let foo = 'bar';" +
  '\n' + "//? <"+"/script>" +
  '\n' + "//? <"+"script>" +
  '\n' + "void function main() { } ()" +
  '\n' + "//? <"+"/script>" +
  '\n';

  parser = new RealParser();

  exception = check(() => {
    parser.ingestCompilationText(text);
  });

  test.ok(!exception);
  test.is(parser.modules.length, 2);
})

test(function block_style() {
  let text =
         "import { Baz } from \"./Baz.src\"" +
  '\n' +
  '\n' + "export" +
  '\n' + "let foo = 'bar';" +
  '\n';

  let parser = new RealParser();
  let module = parser.ingestNormalText(text);

  test.is(parser.modules.length, 1);
  test.is(parser.modules[0], module);
  test.is(module.path, null);

  let emitter = new Emitter();
  let emittee = emitter.prepareForOutput(module);
  test.is(emitter.textify(InputIR.FORM_TRIPLE),
           "/// <"+"script>" +
    '\n' + "/// import { Baz } from \"./Baz.src\"" +
    '\n' +
    '\n' + "/// export" +
    '\n' + "let foo = 'bar';" +
    '\n' + "/// <"+"/script>" +
    '\n'
  );

  emitter = new Emitter();
  emittee = emitter.prepareForOutput(module);
  emitter.blockStyle = InputIR.GBlock;
  test.is(emitter.textify(InputIR.FORM_TRIPLE),
           "//? <"+"script>" +
    '\n' + "/// import { Baz } from \"./Baz.src\"" +
    '\n' +
    '\n' + "/// export" +
    '\n' + "let foo = 'bar';" +
    '\n' + "//? <"+"/script>" +
    '\n'
  );

  emitter = new Emitter(InputIR.GBlock);
  emittee = emitter.prepareForOutput(module);
  test.is(emitter.textify(InputIR.FORM_TRIPLE),
           "//? <"+"script>" +
    '\n' + "/// import { Baz } from \"./Baz.src\"" +
    '\n' +
    '\n' + "/// export" +
    '\n' + "let foo = 'bar';" +
    '\n' + "//? <"+"/script>" +
    '\n'
  );
})

test(function no_surprise_attributes() {
  let text, parser, module, exception = null;

  text =
         "import { SystemA } from \"./SystemA.src\"" +
  '\n' +
  '\n' + "void function main() {" +
  '\n' + "  var $hey$ = 'this is not allowed';" +
  '\n' + "} ()" +
  '\n';

  parser = new RealParser();
  exception = check(() => {
    module = parser.ingestNormalText(text);
  });

  test.ok(!!exception);

  exception = null;
  text =
         "export" +
  '\n' + "function foo() {" +
  '\n' + "  var $bar$ = 'and that\'s not a legal identifier name';" +
  '\n' + "}" +
  '\n';

  parser = new RealParser();
  exception = check(() => {
    module = parser.ingestNormalText(text);
  });

  test.ok(!!exception);

  exception = null;
  text =
         "export" +
  '\n' + "function lol($wut$ = null) {" +
  '\n' + "  console.log('we can't do that, either')" +
  '\n' + "}" +
  '\n';

  parser = new RealParser();
  exception = check(() => {
    module = parser.ingestNormalText(text);
  });

  test.ok(!!exception);

  exception = null;
  text =
         "export" +
  '\n' + "class Seriously {" +
  '\n' + "  constructor() {" +
  '\n' + "    $notALoopLabel$:" +
  '\n' + "    for (let i = 0, n = 3; i < 3; ++i) {" +
  '\n' + "      console.log('no way, no how');" +
  '\n' + "    }" +
  '\n' + "  }" +
  '\n' + "}" +
  '\n';

  parser = new RealParser();
  exception = check(() => {
    module = parser.ingestNormalText(text);
  });

  test.ok(!!exception);
})
