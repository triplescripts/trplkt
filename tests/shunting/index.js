import { test } from "$TEST_HELPER";

import { RealParser } from "$PROJECT/src/RealParser.src";

function check($fn) {
  try {
    $fn();
  } catch (ex) {
    return ex;
  }
  return null;
}

test(function no_compilation_text_trailer() {
  let exception, parser, text =
         `//? <`+`script>` +
  '\n' + `/// export` +
  '\n' + `class Foo { }` +
  '\n' + `//? <`+`/script>` +
  '\n' + `//? <`+`script>` +
  '\n' + `/// export` +
  '\n' + `class Bar { }` +
  '\n' + `//? <`+`/script>` +
  '\n' + `//? <`+`script>` +
  '\n' + `/// import { Foo } from "./Foo.src";` +
  '\n' + `/// import { Bar } from "./Bar.src";` +
  '\n' +
  '\n' + `void function main() { } ()` +
  '\n' + `//? <`+`/script>` +
  '\n' + `/`+`* teehee! *`+`/` +
  '\n';

  parser = new RealParser();

  exception = check(() => {
    parser.ingestCompilationText(text);
  });

  test.ok(exception != null);
  test.ok(
    String(exception).includes("Encountered trailing text") // XXX
  );
})

test(function one_shunting_block() {
  // aka: no_static_initializers_for_standard_modules, where by "standard" we
  // mean any module that isn't a shunting block
  let exception, parser, text =
         `//? <`+`script>` +
  '\n' + `/// export` +
  '\n' + `class Foo { }` +
  '\n' +
  '\n' + `void function() {` +
  '\n' + `  Foo._x = makeMap();` +
  '\n' + `  function makeMap() {` +
  '\n' + `    let map = new Map();` +
  '\n' + `    map.set("foo", "bar");` +
  '\n' + `    map.set("fum", "nom");` +
  '\n' + `    return map;` +
  '\n' + `  }` +
  '\n' +
  '\n' + `  let others = [ "lorem", "ipsum" ];` +
  '\n' + `  for (let i = 0; i < others.length; ++i) {` +
  '\n' + `    Foo._x.set(others[i], null)` +
  '\n' + `  }` +
  '\n' + `} ()` +
  '\n' + `//? <`+`/script>` +
  '\n' + `//? <`+`script>` +
  '\n' + `/// import { Foo } from "./Foo.src";` +
  '\n' +
  '\n' + `void function main() { } ()` +
  '\n' + `//? <`+`/script>` +
  '\n';

  parser = new RealParser();

  exception = check(() => {
    parser.ingestCompilationText(text);
  });

  test.ok(exception != null);
  test.ok(
    //~ String(exception).includes("Program initialization must be confined")
    String(exception).includes("Unexpected token: \")\"")
  );
})

test(function standard_module_with_same_name_IIFE() {
  let exception, parser, text =
         `//? <`+`script>` +
  '\n' + `/// export` +
  '\n' + `class Foo { }` +
  '\n' +
  '\n' + `void function Foo() {` +
  '\n' + `  Foo._x = makeMap();` +
  '\n' + `  // ... use your imagination ...` +
  '\n' + `  // (see test 'one_shunting_block' above)` +
  '\n' + `} ()` +
  '\n' + `//? <`+`/script>` +
  '\n' + `//? <`+`script>` +
  '\n' + `/// import { Foo } from "./Foo.src";` +
  '\n' +
  '\n' + `void function main() { } ()` +
  '\n' + `//? <`+`/script>` +
  '\n';

  parser = new RealParser();

  exception = check(() => {
    parser.ingestCompilationText(text);
  });

  test.ok(exception != null);
  test.ok(
    //~ String(exception).includes("Program initialization must be confined")
    String(exception).includes("Unexpected token: identifier")
  );
})

test(function standard_module_with_other_IIFE() {
  let exception, parser, text =
         `//? <`+`script>` +
  '\n' + `/// export` +
  '\n' + `class Foo { }` +
  '\n' +
  '\n' + `void function main() {` +
  '\n' + `  Foo._x = makeMap();` +
  '\n' + `  // ... use your imagination ...` +
  '\n' + `  // (see test 'one_shunting_block' above)` +
  '\n' + `} ()` +
  '\n' + `//? <`+`/script>` +
  '\n' + `//? <`+`script>` +
  '\n' + `/// import { Foo } from "./Foo.src";` +
  '\n' +
  '\n' + `void function main() { } ()` +
  '\n' + `//? <`+`/script>` +
  '\n';

  parser = new RealParser();

  exception = check(() => {
    parser.ingestCompilationText(text);
  });

  test.ok(exception != null);
  test.ok(
    //~ String(exception).includes("Program initialization must be confined")
    String(exception).includes("Unexpected token: identifier")
  );
})
