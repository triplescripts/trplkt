import { test } from "$TEST_HELPER";

import { RealParser } from "$PROJECT/src/RealParser.src";

function check(description, $fn) {
  test.ok(
    (function() {
      try {
        $fn.apply(null);
      } catch (ex) {
        return String(ex.message).startsWith(description);
      }
      return false;
    })()
  );
}

test(function no_formal_alias() {
  let text =
         "export" +
  '\n' + "const foo = function() { };" +
  '\n' +
  '\n' + "void function() {" +
  '\n' + "  // the formal 'parameter' must be the alias declaration" +
  '\n' + "} ()" +
  '\n';

  let parser = new RealParser();
  check("Unexpected token: \")\"", (() => {
    parser.ingestNormalText(text);
  }));
})

test(function alias_binding_mismatch() {
  let text =
         "export" +
  '\n' + "const foo = function() { };" +
  '\n' +
  '\n' + "void function($proto) {" +
  '\n' + "  $other = foo.prototype;" +
  '\n' + "} ()" +
  '\n';

  let parser = new RealParser();
  check("Prototype alias binding mismatch", (() => {
    parser.ingestNormalText(text);
  }));
})

test(function only_prototype_modification_allowed_is_to_that_of_the_export() {
  let text =
         "export" +
  '\n' + "const foo = function() { };" +
  '\n' +
  '\n' + "void function($proto) {" +
  '\n' + "  $proto = bar.prototype;" +
  '\n' + "} ()" +
  '\n';

  let parser = new RealParser();
  check("Expected prototype alias for foo", (() => {
    parser.ingestNormalText(text);
  }));
})

test(function only_simple_definitions_of_prototype_members_are_allowed() {
  let text =
         "export" +
  '\n' + "const foo = function() { };" +
  '\n' +
  '\n' + "void function($proto) {" +
  '\n' + "  $proto = foo.prototype;" +
  '\n' +
  '\n' + "  if (typeof(module) != 'undefined') {" +
  '\n' + "    // you can't do this" +
  '\n' + "  }" +
  '\n' + "} ()" +
  '\n';

  let parser = new RealParser();
  check("Unexpected token: \"if\"", (() => {
    parser.ingestNormalText(text);
  }));
})

test(function seriously_only_simple_definitions_for_real() {
  let text =
         "export" +
  '\n' + "const foo = function() { };" +
  '\n' +
  '\n' + "void function($proto) {" +
  '\n' + "  $proto = foo.prototype;" +
  '\n' +
  '\n' + "  for (let k of Object.keys($proto)) {" +
  '\n' + "    // you can't do this, either" +
  '\n' + "  }" +
  '\n' + "} ()" +
  '\n';

  let parser = new RealParser();
  check("Unexpected token: \"for\"", (() => {
    parser.ingestNormalText(text);
  }));
})

test(function shunting_block_wannabe() {
  let text =
         "export" +
  '\n' + "const foo = function() { };" +
  '\n' +
  '\n' + "void function main() {" +
  '\n' + "  // shunting blocks can't have exports" +
  '\n' + "} ()" +
  '\n';

  let parser = new RealParser();
  check("Unexpected token: identifier", (() => {
    parser.ingestNormalText(text);
  }));
})
