import { test, read } from "$TEST_HELPER";

import { Token } from "$PROJECT/src/Token.src";
import { TokenCollector } from "$PROJECT/src/TokenCollector.src";
import { TokenScanner } from "$PROJECT/src/TokenScanner.src";

test(function unrecognized_tokens() {
  let text =
         `void#{}` +
  '\n';

  let error, kind, scanner = new TokenScanner(text);
  scanner.onerror = (which, where) => { error = which };

  test.is(error, undefined);

  kind = scanner.readNextToken();
  test.is(kind, Token.VOID);

  kind = scanner.readNextToken();
  test.is(kind, TokenScanner.ERROR);
  test.is(error, TokenScanner.ERROR_UNRECOGNIZED_TOKEN);
  error = null;

  kind = scanner.readNextToken();
  test.is(kind, Token.OPEN_CURLY);
  kind = scanner.readNextToken();
  test.is(kind, Token.CLOSE_CURLY);
  kind = scanner.readNextToken();
  test.is(kind, null);

  test.is(error, null);
})

test(function all_reserved_words() {
  let text =
         `async await break case catch class const continue debugger` +
  '\n' + `default delete do else enum export extends false finally for from` +
  '\n' + `function if implements import in instanceof interface let new` +
  '\n' + `null package private protected public return super switch this` +
  '\n' + `throw true try typeof undefined var void while with yield` +
  '\n';

  let kind, scanner = new TokenScanner(text);

  kind = scanner.readNextToken();
  test.is(kind, Token.ASYNC);
  kind = scanner.readNextToken();
  test.is(kind, Token.AWAIT);
  kind = scanner.readNextToken();
  test.is(kind, Token.BREAK);
  kind = scanner.readNextToken();
  test.is(kind, Token.CASE);
  kind = scanner.readNextToken();
  test.is(kind, Token.CATCH);
  kind = scanner.readNextToken();
  test.is(kind, Token.CLASS);
  kind = scanner.readNextToken();
  test.is(kind, Token.CONST);
  kind = scanner.readNextToken();
  test.is(kind, Token.CONTINUE);
  kind = scanner.readNextToken();
  test.is(kind, Token.DEBUGGER);
  kind = scanner.readNextToken();
  test.is(kind, Token.DEFAULT);
  kind = scanner.readNextToken();
  test.is(kind, Token.DELETE);
  kind = scanner.readNextToken();
  test.is(kind, Token.DO);
  kind = scanner.readNextToken();
  test.is(kind, Token.ELSE);
  kind = scanner.readNextToken();
  test.is(kind, Token.ENUM);
  kind = scanner.readNextToken();
  test.is(kind, Token.EXPORT);
  kind = scanner.readNextToken();
  test.is(kind, Token.EXTENDS);
  kind = scanner.readNextToken();
  test.is(kind, Token.FALSE);
  kind = scanner.readNextToken();
  test.is(kind, Token.FINALLY);
  kind = scanner.readNextToken();
  test.is(kind, Token.FOR);
  kind = scanner.readNextToken();
  test.is(kind, Token.FROM);
  kind = scanner.readNextToken();
  test.is(kind, Token.FUNCTION);
  kind = scanner.readNextToken();
  test.is(kind, Token.IF);
  kind = scanner.readNextToken();
  test.is(kind, Token.IMPLEMENTS);
  kind = scanner.readNextToken();
  test.is(kind, Token.IMPORT);
  kind = scanner.readNextToken();
  test.is(kind, Token.IN);
  kind = scanner.readNextToken();
  test.is(kind, Token.INSTANCEOF);
  kind = scanner.readNextToken();
  test.is(kind, Token.INTERFACE);
  kind = scanner.readNextToken();
  test.is(kind, Token.LET);
  kind = scanner.readNextToken();
  test.is(kind, Token.NEW);
  kind = scanner.readNextToken();
  test.is(kind, Token.NULL);
  kind = scanner.readNextToken();
  test.is(kind, Token.PACKAGE);
  kind = scanner.readNextToken();
  test.is(kind, Token.PRIVATE);
  kind = scanner.readNextToken();
  test.is(kind, Token.PROTECTED);
  kind = scanner.readNextToken();
  test.is(kind, Token.PUBLIC);
  kind = scanner.readNextToken();
  test.is(kind, Token.RETURN);
  kind = scanner.readNextToken();
  test.is(kind, Token.SUPER);
  kind = scanner.readNextToken();
  test.is(kind, Token.SWITCH);
  kind = scanner.readNextToken();
  test.is(kind, Token.THIS);
  kind = scanner.readNextToken();
  test.is(kind, Token.THROW);
  kind = scanner.readNextToken();
  test.is(kind, Token.TRUE);
  kind = scanner.readNextToken();
  test.is(kind, Token.TRY);
  kind = scanner.readNextToken();
  test.is(kind, Token.TYPEOF);
  kind = scanner.readNextToken();
  test.is(kind, Token.UNDEFINED);
  kind = scanner.readNextToken();
  test.is(kind, Token.VAR);
  kind = scanner.readNextToken();
  test.is(kind, Token.VOID);
  kind = scanner.readNextToken();
  test.is(kind, Token.WHILE);
  kind = scanner.readNextToken();
  test.is(kind, Token.WITH);
  kind = scanner.readNextToken();
  test.is(kind, Token.YIELD);

  kind = scanner.readNextToken();
  test.is(kind, null);
})

test(function simple_function() {
  let text =
         `void function main() { }` +
  '\n';

  let scanner = new TokenScanner(text);
  let collector = TokenCollector.attach(scanner);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.VOID);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.FUNCTION);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.IDENTIFIER_NAME);
  test.is(collector.last.content, "main");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.OPEN_PAREN);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.CLOSE_PAREN);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.OPEN_CURLY);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.CLOSE_CURLY);

  let eof = scanner.readNextToken();
  test.is(eof, null);
})

test(function comments() {
  let text =
         `// Foo the bar` +
  '\n' + `function baz() {` +
  '\n' + '  /* ... unimplemented ... *'+'/' +
  '\n' + `}` +
  '\n';

  let scanner = new TokenScanner(text);
  let collector = TokenCollector.attach(scanner);

  scanner.keepComments = true;

  scanner.readNextToken();
  test.is(collector.last.kind, Token.COMMENT);
  test.is(collector.last.content, "// Foo the bar");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.FUNCTION);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.IDENTIFIER_NAME);
  test.is(collector.last.content, "baz");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.OPEN_PAREN);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.CLOSE_PAREN);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.OPEN_CURLY);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.COMMENT);
  test.is(collector.last.content, "/* ... unimplemented ... *"+"/");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.CLOSE_CURLY);

  let eof = scanner.readNextToken();
  test.is(eof, null);
})

test(function whitespace() {
  let text =
         `foo\tbar\t baz` + '\r' +
  '\n' + `fum` +
  '\n' + `nom` +
  '\n';

  let scanner = new TokenScanner(text);
  let collector = TokenCollector.attach(scanner);
  scanner.keepWhitespace = true;

  scanner.readNextToken();
  test.is(collector.last.content, "foo");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.WHITESPACE);
  test.is(collector.last.content, "\t");
  scanner.readNextToken();
  test.is(collector.last.content, "bar");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.WHITESPACE);
  test.is(collector.last.content, "\t ");
  scanner.readNextToken();
  test.is(collector.last.content, "baz");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.WHITESPACE);
  test.is(collector.last.content, "\r\n");
  scanner.readNextToken();
  test.is(collector.last.content, "fum");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.WHITESPACE);
  test.is(collector.last.content, "\n");
  scanner.readNextToken();
  test.is(collector.last.content, "nom");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.WHITESPACE);
  test.is(collector.last.content, "\n");

  let eof = scanner.readNextToken();
  test.is(eof, null);
})

test(function whitespace_coalescing() {
  var scanner, collector;

  scanner = new TokenScanner("\n\n");
  collector = TokenCollector.attach(scanner);

  scanner.readNextToken(true);
  test.is(collector.last.kind, Token.WHITESPACE);
  test.is(collector.last.content, "\n");
  scanner.readNextToken(true);
  test.is(collector.last.kind, Token.WHITESPACE);
  test.is(collector.last.content, "\n");

  test.is(scanner.readNextToken(true), null);

  scanner = new TokenScanner("\r\n\n");
  collector = TokenCollector.attach(scanner);

  scanner.readNextToken(true);
  test.is(collector.last.kind, Token.WHITESPACE);
  test.is(collector.last.content, "\r\n");
  scanner.readNextToken(true);
  test.is(collector.last.kind, Token.WHITESPACE);
  test.is(collector.last.content, "\n");

  test.is(scanner.readNextToken(true), null);

  scanner = new TokenScanner("\n\r\n");
  collector = TokenCollector.attach(scanner);

  scanner.readNextToken(true);
  test.is(collector.last.kind, Token.WHITESPACE);
  test.is(collector.last.content, "\n");
  scanner.readNextToken(true);
  test.is(collector.last.kind, Token.WHITESPACE);
  test.is(collector.last.content, "\r\n");

  test.is(scanner.readNextToken(true), null);

  scanner = new TokenScanner("\r\n\r\n");
  collector = TokenCollector.attach(scanner);

  scanner.readNextToken(true);
  test.is(collector.last.kind, Token.WHITESPACE);
  test.is(collector.last.content, "\r\n");
  scanner.readNextToken(true);
  test.is(collector.last.kind, Token.WHITESPACE);
  test.is(collector.last.content, "\r\n");

  test.is(scanner.readNextToken(true), null);

  scanner = new TokenScanner("\n ");
  collector = TokenCollector.attach(scanner);

  scanner.readNextToken(true);
  test.is(collector.last.kind, Token.WHITESPACE);
  test.is(collector.last.content, "\n");
  scanner.readNextToken(true);
  test.is(collector.last.kind, Token.WHITESPACE);
  test.is(collector.last.content, " ");

  test.is(scanner.readNextToken(true), null);

  scanner = new TokenScanner("\n  ");
  collector = TokenCollector.attach(scanner);

  scanner.readNextToken(true);
  test.is(collector.last.kind, Token.WHITESPACE);
  test.is(collector.last.content, "\n");
  scanner.readNextToken(true);
  test.is(collector.last.kind, Token.WHITESPACE);
  test.is(collector.last.content, "  ");

  test.is(scanner.readNextToken(true), null);

  scanner = new TokenScanner("\n\t");
  collector = TokenCollector.attach(scanner);

  scanner.readNextToken(true);
  test.is(collector.last.kind, Token.WHITESPACE);
  test.is(collector.last.content, "\n");
  scanner.readNextToken(true);
  test.is(collector.last.kind, Token.WHITESPACE);
  test.is(collector.last.content, "\t");

  test.is(scanner.readNextToken(true), null);

  scanner = new TokenScanner("\n\t\t");
  collector = TokenCollector.attach(scanner);

  scanner.readNextToken(true);
  test.is(collector.last.kind, Token.WHITESPACE);
  test.is(collector.last.content, "\n");
  scanner.readNextToken(true);
  test.is(collector.last.kind, Token.WHITESPACE);
  test.is(collector.last.content, "\t\t");

  test.is(scanner.readNextToken(true), null);

  scanner = new TokenScanner("\n\t ");
  collector = TokenCollector.attach(scanner);

  scanner.readNextToken(true);
  test.is(collector.last.kind, Token.WHITESPACE);
  test.is(collector.last.content, "\n");
  scanner.readNextToken(true);
  test.is(collector.last.kind, Token.WHITESPACE);
  test.is(collector.last.content, "\t ");

  test.is(scanner.readNextToken(true), null);

  scanner = new TokenScanner("\n \t");
  collector = TokenCollector.attach(scanner);

  scanner.readNextToken(true);
  test.is(collector.last.kind, Token.WHITESPACE);
  test.is(collector.last.content, "\n");
  scanner.readNextToken(true);
  test.is(collector.last.kind, Token.WHITESPACE);
  test.is(collector.last.content, " \t");

  test.is(scanner.readNextToken(true), null);
})

test(function triple_slash() {
  let text =
         `/// <script>` +
  '\n' + `/// import { Foo } from __FOO_URI__` +
  '\n' +
  '\n' + `/// export` +
  '\n' + `void function main() {` +
  '\n' + `} ()` +
  '\n' + `/// $foo$: true` +
  '\n' + `/// </script>` +
  '\n';

  let scanner = new TokenScanner(text);
  let collector = TokenCollector.attach(scanner);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.OPEN_DELIMITER);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.IMPORT);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.OPEN_CURLY);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.IDENTIFIER_NAME);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.CLOSE_CURLY);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.FROM);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.IDENTIFIER_NAME);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.EXPORT);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.VOID);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.FUNCTION);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.IDENTIFIER_NAME);
  test.is(collector.last.content, "main");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.OPEN_PAREN);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.CLOSE_PAREN);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.OPEN_CURLY);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.CLOSE_CURLY);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.OPEN_PAREN);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.CLOSE_PAREN);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.ATTRIBUTE_NAME);
  test.is(collector.last.content, "/// $foo$");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.COLON);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.TRUE);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.CLOSE_DELIMITER);

  let eof = scanner.readNextToken();
  test.is(eof, null);
})

test(function bad_delimiters() {

  let error, scanner, kind, text =
         `///<script>` +
  '\n' + "/* ... unimplemented ... *"+"/" +
  '\n' + `///  </script>` +
  '\n';

  error = null;
  scanner = new TokenScanner(text);
  scanner.onerror = (which, where) => { error = which };
  scanner.keepComments = true;

  kind = scanner.readNextToken();
  test.is(kind, TokenScanner.ERROR);
  test.is(error, TokenScanner.ERROR_INVALID_TRIPLE_SLASH);

  scanner.reseat(text.indexOf("/*"));

  kind = scanner.readNextToken();
  test.is(kind, Token.COMMENT);

  error = null;
  kind = scanner.readNextToken();
  test.is(kind, TokenScanner.ERROR);
  test.is(error, TokenScanner.ERROR_INVALID_TRIPLE_SLASH);
})

test(function plus_etc() {
  let text =
         `+ ++ +=` +
  '\n';

  let scanner = new TokenScanner(text);
  let collector = TokenCollector.attach(scanner);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.PLUS);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.PLUS_PLUS);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.PLUS_EQUAL);

  let eof = scanner.readNextToken();
  test.is(eof, null);
})

test(function minus_etc() {
  let text =
         `- -- -=` +
  '\n';

  let scanner = new TokenScanner(text);
  let collector = TokenCollector.attach(scanner);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.MINUS);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.MINUS_MINUS);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.MINUS_EQUAL);

  let eof = scanner.readNextToken();
  test.is(eof, null);
})

test(function modulus_etc() {
  let text =
         `% %=` +
  '\n';

  let scanner = new TokenScanner(text);
  let collector = TokenCollector.attach(scanner);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.PERCENT);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.PERCENT_EQUAL);

  let eof = scanner.readNextToken();
  test.is(eof, null);
})

test(function less_and_left_shift_etc() {
  let text =
         `< <= << <<=` +
  '\n';

  let scanner = new TokenScanner(text);
  let collector = TokenCollector.attach(scanner);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.LESS_THAN);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.LESS_OR_EQUAL);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.LEFT_SHIFT);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.LEFT_SHIFT_EQUAL);

  let eof = scanner.readNextToken();
  test.is(eof, null);
})

test(function greater_and_right_shift_etc() {
  let text =
         `> >= >> >>= >>> >>>=` +
  '\n';

  let scanner = new TokenScanner(text);
  let collector = TokenCollector.attach(scanner);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.GREATER_THAN);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.GREATER_OR_EQUAL);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.RIGHT_SHIFT);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.RIGHT_SHIFT_EQUAL);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.UNSIGNED_RIGHT_SHIFT);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.UNSIGNED_RIGHT_SHIFT_EQUAL);

  let eof = scanner.readNextToken();
  test.is(eof, null);
})

test(function equal_and_arrow_etc() {
  let text =
         `= == === =>` +
  '\n';

  let scanner = new TokenScanner(text);
  let collector = TokenCollector.attach(scanner);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.EQUAL);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.DOUBLE_EQUALS);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.TRIPLE_EQUALS);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.ARROW);

  let eof = scanner.readNextToken();
  test.is(eof, null);
})

test(function caret_etc() {
  let text =
         `^ ^=` +
  '\n';

  let scanner = new TokenScanner(text);
  let collector = TokenCollector.attach(scanner);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.CARET);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.CARET_EQUAL);

  let eof = scanner.readNextToken();
  test.is(eof, null);
})

test(function logical_and_bitwise_or_etc() {
  let text =
         `| |= ||` +
  '\n';

  let scanner = new TokenScanner(text);
  let collector = TokenCollector.attach(scanner);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.BAR);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.BAR_EQUAL);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.OR);

  let eof = scanner.readNextToken();
  test.is(eof, null);
})

test(function bang_etc() {
  let text =
         `! != !==` +
  '\n';

  let scanner = new TokenScanner(text);
  let collector = TokenCollector.attach(scanner);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.BANG);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.NOT_EQUAL);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.NOT_STRICTLY_EQUAL);

  let eof = scanner.readNextToken();
  test.is(eof, null);
})

test(function asterisk_etc() {
  let text =
         `* *=` +
  '\n';

  let scanner = new TokenScanner(text);
  let collector = TokenCollector.attach(scanner);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.ASTERISK);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.ASTERISK_EQUAL);

  let eof = scanner.readNextToken();
  test.is(eof, null);
})

test(function logical_and_bitwise_and_etc() {
  let text =
         `& &= &&` +
  '\n';

  let scanner = new TokenScanner(text);
  let collector = TokenCollector.attach(scanner);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.AMPERSAND);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.AMPERSAND_EQUAL);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.AND);

  let eof = scanner.readNextToken();
  test.is(eof, null);
})

test(function dot_etc() {
  // NB: Also ensure that `..` (which should eventually lead to a parser
  // error, but not here) is treated correctly.

  let text =
         `. ...` +
  '\n' + `..` +
  '\n';

  let scanner = new TokenScanner(text);
  let collector = TokenCollector.attach(scanner);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.PERIOD);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.DOT_DOT_DOT);

  const keepWhitespace = true;
  scanner.readNextToken(keepWhitespace);
  test.is(collector.last.kind, Token.WHITESPACE);
  scanner.readNextToken(keepWhitespace);
  test.is(collector.last.kind, Token.PERIOD);
  scanner.readNextToken(keepWhitespace);
  test.is(collector.last.kind, Token.PERIOD);
  scanner.readNextToken(keepWhitespace);
  test.is(collector.last.kind, Token.WHITESPACE);

  let eof = scanner.readNextToken();
  test.is(eof, null);
})

test(function simple_strings() {
  var text, scanner, collector, eof;

  text =
         `import { Foo } from "./src/Foo.js"` +
  '\n';

  scanner = new TokenScanner(text);
  collector = TokenCollector.attach(scanner);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.IMPORT);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.OPEN_CURLY);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.IDENTIFIER_NAME);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.CLOSE_CURLY);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.FROM);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.STRING_LITERAL);

  eof = scanner.readNextToken();
  test.is(eof, null);

  text =
         `$path$: "main.js"` +
  '\n';

  scanner = new TokenScanner(text);
  collector = TokenCollector.attach(scanner);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.ATTRIBUTE_NAME);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.COLON);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.STRING_LITERAL);

  text =
         `print('foo', "bar")` +
  '\n';

  scanner = new TokenScanner(text);
  collector = TokenCollector.attach(scanner);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.IDENTIFIER_NAME);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.OPEN_PAREN);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.STRING_LITERAL);
  test.is(collector.last.content, "'foo'");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.COMMA);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.STRING_LITERAL);
  test.is(collector.last.content, "\"bar\"");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.CLOSE_PAREN);

  eof = scanner.readNextToken();
  test.is(eof, null);
})

test(function string_escapes() {
  var text, scanner, collector, kind, eof;

  text =
         `"foo` + '\\' + '\\' + `bar"` +
  '\n' + `"foo` + '\\' + '\'' + `bar"` +
  '\n' + `"foo` + '\\' + '"'  + `bar"` +
  '\n';

  scanner = new TokenScanner(text);
  collector = TokenCollector.attach(scanner);
  scanner.keepWhitespace = true;

  scanner.readNextToken();
  test.is(collector.last.kind, Token.STRING_LITERAL);
  test.is(collector.last.content, "\"foo\\\\bar\"");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.WHITESPACE);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.STRING_LITERAL);
  test.is(collector.last.content, "\"foo\\'bar\"");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.WHITESPACE);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.STRING_LITERAL);
  test.is(collector.last.content, "\"foo\\\"bar\"");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.WHITESPACE);

  eof = scanner.readNextToken();
  test.is(eof, null);

  text =
         `"foo` + '\\'  + '\n'  + `bar"` +
  '\n' + `"foo` + '\\'  + '\r'  + `bar"` +
  '\n' + `"foo` + '\\r'  + '\\n'+ `bar"` +
  '\n' + `"foo` + '\\'  + '0'   + `bar"` +
  '\n' + `"foo` + '\\'  + 'n'   + `bar"` +
  '\n' + `"foo` + '\\'  + 'r'   + `bar"` +
  '\n' + `"foo` + '\\'  + 't'   + `bar"` +
  '\n';

  scanner = new TokenScanner(text);
  collector = TokenCollector.attach(scanner);
  scanner.keepWhitespace = true;

  scanner.readNextToken();
  test.is(collector.last.kind, Token.STRING_LITERAL);
  test.is(collector.last.content, "\"foo\\\nbar\"");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.WHITESPACE);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.STRING_LITERAL);
  test.is(collector.last.content, "\"foo\\\rbar\"");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.WHITESPACE);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.STRING_LITERAL);
  test.is(collector.last.content, "\"foo\\r\\nbar\"");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.WHITESPACE);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.STRING_LITERAL);
  test.is(collector.last.content, "\"foo\\0bar\"");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.WHITESPACE);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.STRING_LITERAL);
  test.is(collector.last.content, "\"foo\\nbar\"");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.WHITESPACE);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.STRING_LITERAL);
  test.is(collector.last.content, "\"foo\\rbar\"");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.WHITESPACE);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.STRING_LITERAL);
  test.is(collector.last.content, "\"foo\\tbar\"");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.WHITESPACE);

  eof = scanner.readNextToken();
  test.is(eof, null);
})

test(function template_escapes() {
  var text, scanner, collector, eof;

  text =
         "`foo" + '\\' + '\n' + "bar`" +
  '\n' + "`foo" + '\r' + '\n' + "bar`" +
  '\n' + "`foo" + '\r' +        "bar`" +
  '\n' + "`foo" + '\n' +        "bar`" +
  '\n';

  scanner = new TokenScanner(text);
  collector = TokenCollector.attach(scanner);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.SEMISTATIC_FORM);
  test.is(collector.last.content, "`foo\\\nbar`");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.SEMISTATIC_FORM);
  test.is(collector.last.content, "`foo\r\nbar`");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.SEMISTATIC_FORM);
  test.is(collector.last.content, "`foo\rbar`");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.SEMISTATIC_FORM);
  test.is(collector.last.content, "`foo\nbar`");

  eof = scanner.readNextToken();
  test.is(eof, null);

  text =
         "`foo" + "${" + "'" + "bar" + "'" + "}" + "baz`" +
  '\n' + "`foo" + "${" + '"' + "bar" + '"' + "}" + "baz`" +
  '\n';

  scanner = new TokenScanner(text);
  collector = TokenCollector.attach(scanner);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.SEMISTATIC_FORM);
  test.is(collector.last.content, "`foo${'bar'}baz`");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.SEMISTATIC_FORM);
  test.is(collector.last.content, "`foo${\"bar\"}baz`");

  eof = scanner.readNextToken();
  test.is(eof, null);

  text =
         "`" + "foo" + "$\\{x}" + "baz" + "`" +
  '\n';

  scanner = new TokenScanner(text);
  collector = TokenCollector.attach(scanner);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.SEMISTATIC_FORM);
  test.is(collector.last.content, "`foo$" + "\\" + "{x}baz`");

  eof = scanner.readNextToken();
  test.is(eof, null);
})

test(function numeric_literals() {
  var text, scanner, collector, eof;

  text =
         "0" +
  '\n' + "1" +
  '\n' + "2" +
  '\n' + "3" +
  '\n' + "4" +
  '\n' + "5" +
  '\n' + "6" +
  '\n' + "7" +
  '\n' + "8" +
  '\n' + "9" +
  '\n';

  scanner = new TokenScanner(text);
  collector = TokenCollector.attach(scanner);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.NUMBER_LITERAL);
  test.is(collector.last.content, "0");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.NUMBER_LITERAL);
  test.is(collector.last.content, "1");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.NUMBER_LITERAL);
  test.is(collector.last.content, "2");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.NUMBER_LITERAL);
  test.is(collector.last.content, "3");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.NUMBER_LITERAL);
  test.is(collector.last.content, "4");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.NUMBER_LITERAL);
  test.is(collector.last.content, "5");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.NUMBER_LITERAL);
  test.is(collector.last.content, "6");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.NUMBER_LITERAL);
  test.is(collector.last.content, "7");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.NUMBER_LITERAL);
  test.is(collector.last.content, "8");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.NUMBER_LITERAL);
  test.is(collector.last.content, "9");

  eof = scanner.readNextToken();
  test.is(eof, null);

  text =
         "4294967296" +
  '\n' + "9007199254740992" +
  '\n' + "9007199254740993" +
  '\n' + "1e309" +
  '\n' + "1e-325" +
  '\n' + "6.02214076e+23" +
  '\n' + "0xFF" +
  '\n' + "0x1000" +
  '\n' + "0x8000FFFF" +
  '\n';

  scanner = new TokenScanner(text);
  collector = TokenCollector.attach(scanner);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.NUMBER_LITERAL);
  test.is(collector.last.content, "4294967296");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.NUMBER_LITERAL);
  test.is(collector.last.content, "9007199254740992");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.NUMBER_LITERAL);
  test.is(collector.last.content, "9007199254740993");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.NUMBER_LITERAL);
  test.is(collector.last.content, "1e309");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.NUMBER_LITERAL);
  test.is(collector.last.content, "1e-325");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.NUMBER_LITERAL);
  test.is(collector.last.content, "6.02214076e+23");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.NUMBER_LITERAL);
  test.is(collector.last.content, "0xFF");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.NUMBER_LITERAL);
  test.is(collector.last.content, "0x1000");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.NUMBER_LITERAL);
  test.is(collector.last.content, "0x8000FFFF");

  eof = scanner.readNextToken();
  test.is(eof, null);
})

// TODO test bad numeric literals

test(function character_escapes() {
  var text, scanner, collector, eof;

  text =
         "'\\u{3c}\\x2Fscript\\u003E'" +
  '\n';

  scanner = new TokenScanner(text);
  collector = TokenCollector.attach(scanner);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.STRING_LITERAL);
  test.is(collector.last.content, "'\\u{3c}\\x2Fscript\\u003E'");

  eof = scanner.readNextToken();
  test.is(eof, null);
})

// TODO test bad escapes

test(function non_comment_slashes() {
  var text, scanner, collector, eof;

  text =
         "a = b / c;" +
  '\n' + "x = y/z;" +
  '\n';

  scanner = new TokenScanner(text);
  collector = TokenCollector.attach(scanner);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.IDENTIFIER_NAME);
  test.is(collector.last.content, "a");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.EQUAL);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.IDENTIFIER_NAME);
  test.is(collector.last.content, "b");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.SLASH);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.IDENTIFIER_NAME);
  test.is(collector.last.content, "c");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.SEMICOLON);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.IDENTIFIER_NAME);
  test.is(collector.last.content, "x");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.EQUAL);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.IDENTIFIER_NAME);
  test.is(collector.last.content, "y");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.SLASH);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.IDENTIFIER_NAME);
  test.is(collector.last.content, "z");
  scanner.readNextToken();
  test.is(collector.last.kind, Token.SEMICOLON);

  eof = scanner.readNextToken();
  test.is(eof, null);
})

test(function pattern_literals() {
  var text, scanner, collector, eof;

  text =
         "(/bar/).test(input)" +
  '\n';

  scanner = new TokenScanner(text);
  collector = TokenCollector.attach(scanner);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.OPEN_PAREN);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.PATTERN_LITERAL);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.CLOSE_PAREN);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.PERIOD);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.IDENTIFIER_NAME);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.OPEN_PAREN);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.IDENTIFIER_NAME);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.CLOSE_PAREN);

  eof = scanner.readNextToken();
  test.is(eof, null);


  text =
         "foo.match(/bar|baz/)" +
  '\n';

  scanner = new TokenScanner(text);
  collector = TokenCollector.attach(scanner);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.IDENTIFIER_NAME);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.PERIOD);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.IDENTIFIER_NAME);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.OPEN_PAREN);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.PATTERN_LITERAL);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.CLOSE_PAREN);

  eof = scanner.readNextToken();
  test.is(eof, null);

  text =
         "text.replace(/bar|baz/g, 'foo')" +
  '\n';

  scanner = new TokenScanner(text);
  collector = TokenCollector.attach(scanner);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.IDENTIFIER_NAME);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.PERIOD);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.IDENTIFIER_NAME);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.OPEN_PAREN);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.PATTERN_LITERAL);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.COMMA);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.STRING_LITERAL);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.CLOSE_PAREN);

  eof = scanner.readNextToken();
  test.is(eof, null);
})

test(function bad_template() {
  // ES6-like template substitutions are not supported (for now)
  var text, scanner, collector, kind, error;

  text =
         "`foo ${bar} baz`" +
  '\n';

  scanner = new TokenScanner(text);
  collector = TokenCollector.attach(scanner);
  scanner.onerror = (x) => void(error = x);

  kind = scanner.readNextToken();
  test.is(kind, TokenScanner.ERROR);
  test.is(collector.tokens.length, 0);
  test.is(error, TokenScanner.ERROR_MALFORMED_SEMISTATIC_CONTENT);

  // End when they're escaped, they have to be escaped in a particular way.
  // (Backslash _separates_ `$` and `{`; it cannot precede the `$`.)
  text =
         "`" + "foo" + "\\${x}" + "baz" + "`" +
  '\n';

  scanner = new TokenScanner(text);
  collector = TokenCollector.attach(scanner);
  scanner.onerror = ((x) => void(error = x));

  kind = scanner.readNextToken();
  test.is(kind, TokenScanner.ERROR);
  test.is(collector.tokens.length, 0);
  test.is(error, TokenScanner.ERROR_INVALID_STRING_ESCAPE);
})

test(function bad_comment() {
  var text, scanner, collector, error;

  text =
         "/* foo bar baz" +
  '\n';

  scanner = new TokenScanner(text);
  collector = TokenCollector.attach(scanner);
  scanner.onerror = (x) => void(error = x);
  scanner.keepComments = true;

  let kind = scanner.readNextToken();
  test.is(kind, TokenScanner.ERROR);
  test.is(collector.tokens.length, 0);
  test.is(error, TokenScanner.ERROR_COMMENT_ABRUPT_END);
})

test(function scan_realistic_multiline_literal() {
  const credits =
  ` ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' '
  The primary work is trplkt (0.9.8).
  Available under MIT License, unless otherwise noted.

  NB: The tools used to build the primary work are distributed, alongside the
  source code for the primary work, in the project directory under license
  compatible with this use.  Nonetheless, these build tools are not considered a
  part of the primary work, but instead separate works which remain subject to
  the terms of their respective licenses.

  (The preceding notes are informational, and reproducing them is not itself
  intended to be a prerequisite for redistribution, although the inclusion of
  some of the content below may be required in order to redistribute the primary
  work in whole or in part.)

  Authorship and contributions
  ============================

  * Andrey Tupitsin
  * Colby Russell
  * Jean-loup Gailly
  * Mark Adler
  * Vitaly Puzrin

  Additional works involved
  =========================

  CRC-32 routines (derived from zlib / pako) available under zlib license

      Copyright (C) 1995-2013 Jean-loup Gailly and Mark Adler
      Copyright (C) 2014-2017 Vitaly Puzrin and Andrey Tupitsin

  <https://zlib.net>
  <https://github.com/nodeca/pako>

  Licenses compatible for use in the primary work
  ===============================================

  MIT License

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  * * *

  The zlib license

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
  ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' `

  let text = '`' + credits + '`';
  let scanner = new TokenScanner(text);
  let collector = TokenCollector.attach(scanner);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.SEMISTATIC_FORM);
  test.is(collector.last.content, text);
})

test(function gblocks_etc() {
  var text, scanner, collector, error = null;

  text =
         "//? <script>" +
  '\n' + "//? export foo" +
  '\n' + "/// import bar" +
  '\n' + "//? import baz" +
  '\n' + "//? </script>" +
  '\n';

  scanner = new TokenScanner(text);
  collector = TokenCollector.attach(scanner);
  scanner.onerror = ((x) => { error = x });

  scanner.readNextToken();
  test.is(collector.last.kind, Token.OPEN_DELIMITER);
  test.is(error, null);

  scanner.readNextToken();
  test.is(error, TokenScanner.ERROR_INVALID_TRIPLE_SLASH);

  error = null;
  scanner.position = text.indexOf("foo");

  scanner.readNextToken();
  test.is(collector.last.kind, Token.IDENTIFIER_NAME);
  test.is(collector.last.content, "foo");
  test.is(error, null);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.IMPORT);
  test.is(error, null);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.IDENTIFIER_NAME);
  test.is(collector.last.content, "bar");
  test.is(error, null);

  scanner.readNextToken();
  test.is(error, TokenScanner.ERROR_INVALID_TRIPLE_SLASH);

  error = null;
  scanner.position = text.indexOf("baz");

  scanner.readNextToken();
  test.is(collector.last.kind, Token.IDENTIFIER_NAME);
  test.is(collector.last.content, "baz");
  test.is(error, null);

  scanner.readNextToken();
  test.is(collector.last.kind, Token.CLOSE_DELIMITER);
  test.is(error, null);

  let eof = scanner.readNextToken();
  test.is(eof, null);
})

test(function attributes_vs_ids() {
  var text, scanner, collector, error = null;

  text =
         "foo$ $bar $baz$" +
  '\n';

  scanner = new TokenScanner(text);
  collector = TokenCollector.attach(scanner);
  scanner.onerror = ((x) => { error = x });

  scanner.readNextToken();
  test.is(collector.last.kind, Token.IDENTIFIER_NAME);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.IDENTIFIER_NAME);
  scanner.readNextToken();
  test.is(collector.last.kind, Token.ATTRIBUTE_NAME);
  scanner.readNextToken();
  test.is(error, null);

  let eof = scanner.readNextToken();
  test.is(eof, null);
})

test(function scan_trplkt() {
  return read("$PROJECT/tests/scanner/trplkt.app.htm").then((text) => {
    let scanner = new TokenScanner(text);
    let collector = TokenCollector.attach(scanner);

    scanner.keepWhitespace = true;
    scanner.keepComments = true;

    let error = null;
    scanner.onerror = (which, where) => {
      error = new ScannerError(which, where);
      function ScannerError(which, where) {
        this.reason = which;
        this.position = where;
      }
    }

    const expectedTokenCount = 27321;
    for (let i = 0; i < expectedTokenCount; ++i) {
      let before = scanner.position;
      let kind = scanner.readNextToken();
      if (kind == null) throw new Error("reached eof unexpectedly");
      let delta = scanner.position - before;
      if (delta < 1) throw new Error("scanner didn't advance");
    }

    test.is(collector.tokens.length, expectedTokenCount);
    test.is(collector.tokens.map((x) => (x.content)).join(""), text);
  });
})
