import { test, read } from "$TEST_HELPER"

import { TripleKit } from "$PROJECT/src/TripleKit.src"

// We are testing for the grammatical validity of simple static assignments
// <https://wiki.triplescripts.org/wiki/Simple_static_assignment>

function MockSystem(files) {
  this.pathWritten = null;
  this.dataWritten = null;

  const $res = (x) => (Promise.resolve(x));
  const $err = (x) => (Promise.reject(x));

  this.read = (path) => {
    let result = files.get(path);
    if (result) {
      return $res(files.get(path));
    }
    return $err(Error("invalid read: " + path));
  }

  this.write = (path, contents) => {
    if (this.pathWritten != null) {
      return $err(
        Error("multiple writes:\n - " + this.pathWritten + " + " + path)
      );
    }
    this.dataWritten = contents;
    this.pathWritten = path;
    return $res(void(0));
  };
}

test(function string_literal() {
  let files = new Map();
  files.set(
    "Hello.src",
           `export class Hello {` +
    '\n' + `  static greet(system) {` +
    '\n' + `    system.printLine(Hello.MESSAGE);` +
    '\n' + `  }` +
    '\n' + `}` +
    '\n' +
    '\n' + `Hello.MESSAGE = "Hello, world.";` +
    '\n'
  );
  files.set(
    "main.src",
           `import { Hello } from "./Hello.src";` +
    '\n' +
    '\n' + `void function main() {` +
    '\n' + `  if (!system) var system = {` +
    '\n' + `    printLine: ((x) => { console.log(x) })` +
    '\n' + `  }` +
    '\n' +
    '\n' + `  Hello.greet(system);` +
    '\n' + `} ()` +
    '\n'
  );

  let sys = new MockSystem(files);
  let kit = new TripleKit(sys);

  return kit.build([ "Hello.src" ], "main.src").then(() => {
    test.ok(!!sys.pathWritten);
  });
})

test(function boolean_literals() {
  let files = new Map();
  files.set(
    "Hello.src",
           `export class Hello {` +
    '\n' + `  static greet(system) {` +
    '\n' + `    system.printLine("Hello, world.");` +
    '\n' + `  }` +
    '\n' + `}` +
    '\n' +
    '\n' + `Hello.FOO = true;` +
    '\n' + `Hello.BAR = false;` +
    '\n'
  );
  files.set(
    "main.src",
           `import { Hello } from "./Hello.src";` +
    '\n' +
    '\n' + `void function main() {` +
    '\n' + `  if (!system) var system = {` +
    '\n' + `    printLine: ((x) => { console.log(x) })` +
    '\n' + `  }` +
    '\n' +
    '\n' + `  Hello.greet(system);` +
    '\n' + `} ()` +
    '\n'
  );

  let sys = new MockSystem(files);
  let kit = new TripleKit(sys);

  return kit.build([ "Hello.src" ], "main.src").then(() => {
    test.ok(!!sys.pathWritten);
  });
})

test(function null_value() {
  let files = new Map();
  files.set(
    "Hello.src",
           `export class Hello {` +
    '\n' + `  static greet(system) {` +
    '\n' + `    system.printLine("Hello, world.");` +
    '\n' + `  }` +
    '\n' + `}` +
    '\n' +
    '\n' + `Hello.FOO = null;` +
    '\n'
  );
  files.set(
    "main.src",
           `import { Hello } from "./Hello.src";` +
    '\n' +
    '\n' + `void function main() {` +
    '\n' + `  if (!system) var system = {` +
    '\n' + `    printLine: ((x) => { console.log(x) })` +
    '\n' + `  }` +
    '\n' +
    '\n' + `  Hello.greet(system);` +
    '\n' + `} ()` +
    '\n'
  );

  let sys = new MockSystem(files);
  let kit = new TripleKit(sys);

  return kit.build([ "Hello.src" ], "main.src").then(() => {
    test.ok(!!sys.pathWritten);
  });
})

test(function number_literals() {
  let files = new Map();
  files.set(
    "Hello.src",
           `export class Hello {` +
    '\n' + `  static greet(system) {` +
    '\n' + `    system.printLine("Hello, world.");` +
    '\n' + `  }` +
    '\n' + `}` +
    '\n' +
    '\n' + `Hello.FOO = 0x0100;` +
    '\n' + `Hello.BAR = 128;` +
    '\n' + `Hello.BAZ = -64;` +
    '\n'
  );
  files.set(
    "main.src",
           `import { Hello } from "./Hello.src";` +
    '\n' +
    '\n' + `void function main() {` +
    '\n' + `  if (!system) var system = {` +
    '\n' + `    printLine: ((x) => { console.log(x) })` +
    '\n' + `  }` +
    '\n' +
    '\n' + `  Hello.greet(system);` +
    '\n' + `} ()` +
    '\n'
  );

  let sys = new MockSystem(files);
  let kit = new TripleKit(sys);

  return kit.build([ "Hello.src" ], "main.src").then(() => {
    test.ok(!!sys.pathWritten);
  });
})

test(function helper_class_unnamed() {
  let files = new Map();
  files.set(
    "Hello.src",
           `export class Hello {` +
    '\n' + `  static greet(system) {` +
    '\n' + `    system.printLine(Hello.Message.getContent);` +
    '\n' + `  }` +
    '\n' + `}` +
    '\n' +
    '\n' + `Hello.Message = class {` +
    '\n' + `  static getContent() {` +
    '\n' + `    return "Hello, world.";` +
    '\n' + `  }` +
    '\n' + `}` +
    '\n'
  );
  files.set(
    "main.src",
           `import { Hello } from "./Hello.src";` +
    '\n' +
    '\n' + `void function main() {` +
    '\n' + `  if (!system) var system = {` +
    '\n' + `    printLine: ((x) => { console.log(x) })` +
    '\n' + `  }` +
    '\n' +
    '\n' + `  Hello.greet(system);` +
    '\n' + `} ()` +
    '\n'
  );

  let sys = new MockSystem(files);
  let kit = new TripleKit(sys);

  return kit.build([ "Hello.src" ], "main.src").then(() => {
    test.ok(!!sys.pathWritten);
  });
})

test(function helper_class_named() {
  let files = new Map();
  files.set(
    "Hello.src",
           `export class Hello {` +
    '\n' + `  static greet(system) {` +
    '\n' + `    system.printLine(Hello.Message.getContent);` +
    '\n' + `  }` +
    '\n' + `}` +
    '\n' +
    '\n' + `Hello.Message = class HelloMessage {` +
    '\n' + `  static getContent() {` +
    '\n' + `    return "Hello, world.";` +
    '\n' + `  }` +
    '\n' + `}` +
    '\n'
  );
  files.set(
    "main.src",
           `import { Hello } from "./Hello.src";` +
    '\n' +
    '\n' + `void function main() {` +
    '\n' + `  if (!system) var system = {` +
    '\n' + `    printLine: ((x) => { console.log(x) })` +
    '\n' + `  }` +
    '\n' +
    '\n' + `  Hello.greet(system);` +
    '\n' + `} ()` +
    '\n'
  );

  let sys = new MockSystem(files);
  let kit = new TripleKit(sys);

  return kit.build([ "Hello.src" ], "main.src").then(() => {
    test.ok(!!sys.pathWritten);
  });
})

test(function helper_function_unnamed() {
  let files = new Map();
  files.set(
    "Hello.src",
           `export class Hello {` +
    '\n' + `  static greet(system) {` +
    '\n' + `    system.printLine((new Hello.Message).content);` +
    '\n' + `  }` +
    '\n' + `}` +
    '\n' +
    '\n' + `Hello.Message = function() {` +
    '\n' + `  this.content = "Hello, world.";` +
    '\n' + `}` +
    '\n'
  );
  files.set(
    "main.src",
           `import { Hello } from "./Hello.src";` +
    '\n' +
    '\n' + `void function main() {` +
    '\n' + `  if (!system) var system = {` +
    '\n' + `    printLine: ((x) => { console.log(x) })` +
    '\n' + `  }` +
    '\n' +
    '\n' + `  Hello.greet(system);` +
    '\n' + `} ()` +
    '\n'
  );

  let sys = new MockSystem(files);
  let kit = new TripleKit(sys);

  return kit.build([ "Hello.src" ], "main.src").then(() => {
    test.ok(!!sys.pathWritten);
  });
})

test(function helper_function_named() {
  let files = new Map();
  files.set(
    "Hello.src",
           `export class Hello {` +
    '\n' + `  static greet(system) {` +
    '\n' + `    system.printLine((new Hello.Message).content);` +
    '\n' + `  }` +
    '\n' + `}` +
    '\n' +
    '\n' + `Hello.Message = function HelloMessage() {` +
    '\n' + `  this.content = "Hello, world.";` +
    '\n' + `}` +
    '\n'
  );
  files.set(
    "main.src",
           `import { Hello } from "./Hello.src";` +
    '\n' +
    '\n' + `void function main() {` +
    '\n' + `  if (!system) var system = {` +
    '\n' + `    printLine: ((x) => { console.log(x) })` +
    '\n' + `  }` +
    '\n' +
    '\n' + `  Hello.greet(system);` +
    '\n' + `} ()` +
    '\n'
  );

  let sys = new MockSystem(files);
  let kit = new TripleKit(sys);

  return kit.build([ "Hello.src" ], "main.src").then(() => {
    test.ok(!!sys.pathWritten);
  });
})

test(function quasi_namespace() {
  let files = new Map();
  files.set(
    "Hello.src",
           `export const Hello = { };` +
    '\n' +
    '\n' + `Hello.Message = class {` +
    '\n' + `  greet(system) {` +
    '\n' + `    system.printLine("Hello, world.");` +
    '\n' + `  }` +
    '\n' + `}` +
    '\n'
  );
  files.set(
    "main.src",
           `import { Hello } from "./Hello.src";` +
    '\n' +
    '\n' + `void function main() {` +
    '\n' + `  if (!system) var system = {` +
    '\n' + `    printLine: ((x) => { console.log(x) })` +
    '\n' + `  }` +
    '\n' +
    '\n' + `  (new Hello.Message).greet(system);` +
    '\n' + `} ()` +
    '\n'
  );

  let sys = new MockSystem(files);
  let kit = new TripleKit(sys);

  return kit.build([ "Hello.src" ], "main.src").then(() => {
    test.ok(!!sys.pathWritten);
  });
})

test(function backtick_literal() {
  let files = new Map();
  files.set(
    "Hello.src",
           `export class Hello {` +
    '\n' + `  static greet(system) {` +
    '\n' + `    system.printLine(Hello.MESSAGE);` +
    '\n' + `  }` +
    '\n' + `}` +
    '\n' +
    '\n' + "Hello.MESSAGE = `Hello, world.`;" +
    '\n'
  );
  files.set(
    "main.src",
           `import { Hello } from "./Hello.src";` +
    '\n' +
    '\n' + `void function main() {` +
    '\n' + `  if (!system) var system = {` +
    '\n' + `    printLine: ((x) => { console.log(x) })` +
    '\n' + `  }` +
    '\n' +
    '\n' + `  Hello.greet(system);` +
    '\n' + `} ()` +
    '\n'
  );

  let sys = new MockSystem(files);
  let kit = new TripleKit(sys);

  return kit.build([ "Hello.src" ], "main.src").then(() => {
    test.ok(!!sys.pathWritten);
  });
})

test(function parenthesized() {
  let files = new Map();
  files.set(
    "Hello.src",
           `export class Hello {` +
    '\n' + `  static exec(system) {` +
    '\n' + `    system.write(Hello.HEADING_MARKUP);` +
    '\n' + `    system.write("<p>" + Hello.EXTRA + "</p>");` +
    '\n' + `  }` +
    '\n' + `}` +
    '\n' +
    '\n' + "Hello.HEADING_MARKUP = (`" +
    '\n' + `  <h1 class="message">Hello, world.</h1>` +
    '\n' + "`);" +
    '\n' +
    '\n' + `Hello.EXTRA = (` +
    '\n' + `  "How are you?"` +
    '\n' + `);` +
    '\n'
  );
  files.set(
    "main.src",
           `import { Hello } from "./Hello.src";` +
    '\n' +
    '\n' + `void function main() {` +
    '\n' + `  if (!system) var system = {` +
    '\n' + `    write: ((x) => { console.log(x) }) // XXX` +
    '\n' + `  }` +
    '\n' +
    '\n' + `  Hello.exec(system);` +
    '\n' + `} ()` +
    '\n'
  );

  let sys = new MockSystem(files);
  let kit = new TripleKit(sys);

  return kit.build([ "Hello.src" ], "main.src").then(() => {
    test.ok(!!sys.pathWritten);
  });
})

test(function constant_folding_for_string_literals() {
  let files = new Map();
  files.set(
    "Hello.src",
           `export class Hello {` +
    '\n' + `  static greet(system) {` +
    '\n' + `    system.printLine(Hello.MESSAGE);` +
    '\n' + `  }` +
    '\n' + `}` +
    '\n' +
    '\n' + `Hello.MESSAGE =` +
    '\n' + `  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, " +` +
    '\n' + `  "sed do eiusmod tempor incididunt ut labore et dolore " +` +
    '\n' + `  "magna aliqua. Ut enim ad minim veniam, quis nostrud " +` +
    '\n' + `  "exercitation ullamco laboris nisi ut aliquip ex ea commodo " +` +
    '\n' + `  "consequat. Duis aute irure dolor in reprehenderit in " +` +
    '\n' + `  "voluptate velit esse cillum dolore eu fugiat nulla " +` +
    '\n' + `  "pariatur. Excepteur sint occaecat cupidatat non proident, " +` +
    '\n' + `  "sunt in culpa qui officia deserunt mollit anim id est " +` +
    '\n' + `  "laborum.";` +
    '\n' +
    '\n' + `Hello.IMAGE_DATA = (` +
    '\n' + `  "iVBORw0KGgoAAAANSUhEUgAAAD8AAAA/CAYAAABXXxDfAAAABHNCSVQIC" +` +
    '\n' + `  "AAAAlwSFlzAAAAkgAAAJIB+bxJdAAAABl0RVh0U29mdHdhcmUAd3d3Lml" +` +
    '\n' + `  "Lm9yZ5vuPBoAAAH4SURBVGje7Ze/alRBFIePKFhYCpZq9AEEW8tFQfyDh" +` +
    '\n' + `  "FPYBC0sRMRLXyKPIM2BufMOb+zpEibgFHBOBZu4iYYcnPv7O7MveeDae8" +` +
    '\n' + `  "cRzHcRzHcZwSCCGcjGYjVr3DzGeGIz4eX2TVz6xIk7URxO72XlxEzrFCp" +` +
    '\n' + `  "CI4v0O0EC8nwH+3nEbNxDfDRCB+9WLfzW7cETxnfVTRC4N4aj/fwGvKt5" +` +
    '\n' + `  "twMVR2LVe8MUB95Vd8ejQjLs+MeVlXQi63DRbMTAcxY8WwWWcj57FVjKs" +` +
    '\n' + `  "zbYCmlYyxY3veiTQauZxRHmeKqLw/6Lg1itzqdphjPFnnUDxHfDRCj3Ry" +` +
    '\n' + `  "EUXFiyMO8j2IXGvybFU9X+QdJyIKMr7dcqAtEbtarTgRUVC86TDYgQEm4" +` +
    '\n' + `  "3uOOCPCNzYf8dZEYu843vkza6wYrvjoFvRbFTNju8JIPaEFb87DvyNVR9" +` +
    '\n' + `  "DAFS8Ue93AALEl98gAWLLy5AIeLzD1CY+PwCFCo++wCFi88uQCXi+QNUJ" +` +
    '\n' + `  "+EnrYJEATv5/qTtZwTUPmOtw/QM/HmAXoqfniAnotPBXjEis2J+DYLlnv" +` +
    '\n' + `  "pwJw+cva2mlyHMdxHMdxHMeZAX8A5NvWwU6E2pUAAAAASUVORK5CYII="` +
    '\n' + `);` +
    '\n'
  );
  files.set(
    "main.src",
           `import { Hello } from "./Hello.src";` +
    '\n' +
    '\n' + `void function main() {` +
    '\n' + `  if (!system) var system = {` +
    '\n' + `    printLine: ((x) => { console.log(x) })` +
    '\n' + `  }` +
    '\n' +
    '\n' + `  Hello.greet(system);` +
    '\n' + `} ()` +
    '\n'
  );

  let sys = new MockSystem(files);
  let kit = new TripleKit(sys);

  return kit.build([ "Hello.src" ], "main.src").then(() => {
    test.ok(!!sys.pathWritten);
  });
})
