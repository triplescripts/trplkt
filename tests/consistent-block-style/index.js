import { test } from "$TEST_HELPER";

import { RealParser } from "$PROJECT/src/RealParser.src";

function check($fn) {
  try {
    $fn();
  } catch (ex) {
    return ex;
  }
  return null;
}

test(function gblock_tblock_mixed() {
  let exception, parser, text =
         "/// <"+"script>" +
  '\n' + "/// export" +
  '\n' + "let foo = 'bar';" +
  '\n' + "/// <"+"/script>" +
  '\n' + "//? <"+"script>" +
  '\n' + "void function main() { } ()" +
  '\n' + "//? <"+"/script>" +
  '\n';

  parser = new RealParser();

  exception = check(() => {
    parser.ingestCompilationText(text);
  });

  test.ok(exception != null);
  test.ok(
    String(exception).includes("Incompatible or mixed block style") // XXX
  );

  text =
         "//? <"+"script>" +
  '\n' + "/// export" +
  '\n' + "let foo = 'bar';" +
  '\n' + "//? <"+"/script>" +
  '\n' + "/// <"+"script>" +
  '\n' + "void function main() { } ()" +
  '\n' + "/// <"+"/script>" +
  '\n';

  parser = new RealParser();

  exception = check(() => {
    parser.ingestCompilationText(text);
  });

  test.ok(exception != null);
  test.ok(
    String(exception).includes("Incompatible or mixed block style") // XXX
  );
})

test(function close_delimiter_matches() {
  let exception, parser, text =
         "/// <"+"script>" +
  '\n' + "void function main() { } ()" +
  '\n' + "//? <"+"/script>" +
  '\n';

  parser = new RealParser();

  exception = check(() => {
    parser.ingestCompilationText(text);
  });

  test.ok(exception != null);
  test.ok(
    String(exception).includes("Incompatible or mixed block style") // XXX
  );
})
