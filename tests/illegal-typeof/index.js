import { test, read } from "$TEST_HELPER"

import { ErrorCommons } from "$PROJECT/src/ErrorCommons.src"
import { Token } from "$PROJECT/src/Token.src"
import { TripleKit } from "$PROJECT/src/TripleKit.src"

function MockSystem(files) {
  this.pathWritten = null;
  this.dataWritten = null;

  const $res = (x) => (Promise.resolve(x));
  const $err = (x) => (Promise.reject(new MockSystem.Error(x)));

  this.read = (path) => {
    let result = files.get(path);
    if (result) {
      return $res(files.get(path));
    }
    return $err("invalid read: " + path);
  }

  this.write = (path, contents) => {
    if (this.pathWritten != null) {
      return $err(
        "multiple writes:\n - " + this.pathWritten + " + " + path
      );
    }
    this.dataWritten = contents;
    this.pathWritten = path;
    return $res(void(0));
  };
}

MockSystem.Error = class extends Error { };

// If this test is failing, it indicates one of two things:
// 1. `ErrorCommons.createBadTokenMessage` has been changed to produce output
//     that differs from the way it looked at the time this test was written.
// 2. This type of parsing error is no longer using `createBadTokenMessage`.
//
// This is important to test for, because all the remaining tests in this file
// depend upon these basic invariants.  If all the tests are failing, then the
// tests probably need to be rewritten to account for the new way that
// feedback for parsing errors are dealt with.  If this test is passing but
// the rest are failing, then it's something else.
test(function error_feedback_still_looks_the_same() {
  let message = ErrorCommons.createBadTokenMessage(
    Token.NULL, Token.OPEN_CURLY
  );

  test.ok(message.indexOf("token: \"null\"") >= 0);
  test.ok(message.indexOf("wanted \"{\"") >= 0);
})

test(function typeof_must_have_parens() {
  let files = new Map();
  files.set(
    "Hello.src",
           `export class Hello {` +
    '\n' + `  static greet(system) {` +
    '\n' + `    system.printLine(Hello.MESSAGE);` +
    '\n' + `  }` +
    '\n' + `}` +
    '\n' +
    '\n' + `Hello.MESSAGE = "Hello, world.";` +
    '\n'
  );
  files.set(
    "main.src",
           `import { Hello } from "./Hello.src";` +
    '\n' +
    '\n' + `void function main() {` +
    '\n' + `  if (typeof __FOO__ == "undefined") return;` +
    '\n' + `  if (!system) var system = {` +
    '\n' + `    printLine: ((x) => { console.log(x) })` +
    '\n' + `  }` +
    '\n' +
    '\n' + `  Hello.greet(system);` +
    '\n' + `} ()` +
    '\n'
  );

  let sys = new MockSystem(files);
  let kit = new TripleKit(sys);

  let problem = null;
  return kit.build([ "Hello.src" ], "main.src").catch((err) => {
    problem = err;
  }).finally(() => {
    test.ok(!!problem);
    test.ok(!(problem instanceof MockSystem.Error));

    if ("inner" in problem) {
      problem = problem.inner;
    }

    let [ what ] = String(problem).split("\n", 1);
    test.ok(what.indexOf("token: identifier") >= 0);
    test.ok(what.indexOf("wanted \"(\"") >= 0);
  });
})

test(function typeof_not_for_general_programming() {
  let files = new Map();
  files.set(
    "Hello.src",
           `export class Hello {` +
    '\n' + `  static greet(system) {` +
    '\n' + `    system.printLine(Hello.MESSAGE);` +
    '\n' + `  }` +
    '\n' + `}` +
    '\n' +
    '\n' + `Hello.MESSAGE = "Hello, world.";` +
    '\n'
  );
  files.set(
    "main.src",
           `import { Hello } from "./Hello.src";` +
    '\n' +
    '\n' + `void function main() {` +
    '\n' + `  let isFoo = typeof(__FOO__) == "undefined";` +
    '\n' + `  if (isFoo) return;` +
    '\n' + `  if (!system) var system = {` +
    '\n' + `    printLine: ((x) => { console.log(x) })` +
    '\n' + `  }` +
    '\n' +
    '\n' + `  Hello.greet(system);` +
    '\n' + `} ()` +
    '\n'
  );

  let sys = new MockSystem(files);
  let kit = new TripleKit(sys);

  let problem = null;
  return kit.build([ "Hello.src" ], "main.src").catch((err) => {
    problem = err;
  }).finally(() => {
    test.ok(!!problem);
    test.ok(!(problem instanceof MockSystem.Error));

    if ("inner" in problem) {
      problem = problem.inner;
    }

    let [ what ] = String(problem).split("\n", 1);
    test.ok(what.indexOf("Unexpected typeof") >= 0);
  });
})

test(function typeof_can_use_negated_equivalence_comparison() {
  let files = new Map();
  files.set(
    "Hello.src",
           `export class Hello {` +
    '\n' + `  static greet(system) {` +
    '\n' + `    system.printLine(Hello.MESSAGE);` +
    '\n' + `  }` +
    '\n' + `}` +
    '\n' +
    '\n' + `Hello.MESSAGE = "Hello, world.";` +
    '\n'
  );
  files.set(
    "main.src",
           `import { Hello } from "./Hello.src";` +
    '\n' +
    '\n' + `void function main() {` +
    '\n' + `  if (typeof(__FOO__) != "undefined") {` +
    '\n' + `    if (!system) var system = {` +
    '\n' + `      printLine: ((x) => { console.log(x) })` +
    '\n' + `    }` +
    '\n' + `  }` +
    '\n' +
    '\n' + `  Hello.greet(system);` +
    '\n' + `} ()` +
    '\n'
  );

  let sys = new MockSystem(files);
  let kit = new TripleKit(sys);

  let problem = null;
  return kit.build([ "Hello.src" ], "main.src").catch((err) => {
    problem = err;
  }).finally(() => {
    test.ok(!problem);
  });
})

test(function typeof_strict_equality_comparison_disallowed() {
  let files = new Map();
  files.set(
    "Hello.src",
           `export class Hello {` +
    '\n' + `  static greet(system) {` +
    '\n' + `    system.printLine(Hello.MESSAGE);` +
    '\n' + `  }` +
    '\n' + `}` +
    '\n' +
    '\n' + `Hello.MESSAGE = "Hello, world.";` +
    '\n'
  );
  files.set(
    "main.src",
           `import { Hello } from "./Hello.src";` +
    '\n' +
    '\n' + `void function main() {` +
    '\n' + `  if (typeof(__FOO__) === "undefined") {` +
    '\n' + `    if (!system) var system = {` +
    '\n' + `      printLine: ((x) => { console.log(x) })` +
    '\n' + `    }` +
    '\n' + `  }` +
    '\n' +
    '\n' + `  Hello.greet(system);` +
    '\n' + `} ()` +
    '\n'
  );

  let sys = new MockSystem(files);
  let kit = new TripleKit(sys);

  let problem = null;
  return kit.build([ "Hello.src" ], "main.src").catch((err) => {
    problem = err;
  }).finally(() => {
    test.ok(!!problem);
    test.ok(!(problem instanceof MockSystem.Error));

    if ("inner" in problem) {
      problem = problem.inner;
    }

    let [ what ] = String(problem).split("\n", 1);
    test.ok(what.indexOf("token: \"===\"") >= 0);
    test.ok(what.indexOf("wanted \"==\"") >= 0);
  });
})
