import { LineChecker } from "./LineChecker.js";

export
class NodeJSSystem {
  constructor() {
    if (typeof(process) == "undefined") {
      throw new Error("Not a NodeJS environment");
    }

    this._nodeFS = process.mainModule.require("fs");

    let [ engine, script, ...args ] = process.argv;
    this.args = args;
  }

  run() {
    if (this.args.length == 1) {
      return void(LineChecker.analyze(this, this.args[0]));
    }
    throw new Error("Expected file path");
  }

  read(path) {
    return new Promise((resolve, reject) => {
      this._nodeFS.readFile(path, { encoding: "utf8" }, (err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      });
    });
  }

  print(message) {
    console.log(message);
  }
}
