import { LineChecker } from "./LineChecker.js";

export
class BrowserSystem {
  constructor() {
    if (typeof(window) == "undefined") {
      throw new Error("Not a browser environment");
    }

    this._page = window.document.body;

    this._file = null;
    this._fileInput = null;
  }

  run() {
    this._page.onload = () => {
      this._page.innerHTML = "";

      this._page.style.fontFamily = "monospace";
      this._page.style.whiteSpace = "pre-wrap";

      let doc = this._page.ownerDocument;
      let $$ = doc.createElement.bind(doc);

      let button = $$("input");
      button.type = "button";
      button.value = "Load\u2026";
      button.style.float = "right";
      button.onclick = () => void(this._fileInput.click());
      this._page.appendChild(button);

      let fileInput = $$("input");
      fileInput.type = "file";
      fileInput.style.display = "none";
      fileInput.onchange = this._onFileSelected.bind(this);
      this._fileInput = this._page.appendChild(fileInput);
    }
  }

  _onFileSelected(event) {
    this._file = event.target.files[0];
    let path = this._file.webkitRelativePath;
    LineChecker.analyze(this, path);
  }

  read(path) {
    return new Promise((resolve, reject) => {
      if (path == this._file.webkitRelativePath) {
        if (typeof(FileReader) != "undefined") {
          let reader = new FileReader();

          reader.addEventListener("loadend", (event) => {
            resolve(event.target.result);
          });

          return void(reader.readAsText(this._file));
        }
        return reject(Error("FileReader API not implemented"));
      }
      return reject(Error("Cannot read file with path: " + path));
    });
  }

  print(message) {
    this._page.textContent += "\n" + message;
  }
}
