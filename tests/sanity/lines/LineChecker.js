export
function LineChecker(text) {
  this.text = text;
  this.position = 0;
}

LineChecker.analyze = function(system, path) {
  return system.read(path).then((contents) => {
    let checker = new LineChecker(contents);
    let stats = checker.getStats();

    system.print("  CR: " + stats[LineChecker.TYPE_CR]);
    system.print("  LF: " + stats[LineChecker.TYPE_LF]);
    system.print("CRLF: " + stats[LineChecker.TYPE_CRLF]);

    if (stats[LineChecker.TYPE_NONE]) {
      system.print("\nThis file doesn't end with a line terminator.");
    }
  });
}

LineChecker.prototype.getStats = function() {
  let stats = [];

  stats[LineChecker.TYPE_NONE] = 0;
  stats[LineChecker.TYPE_CR] = 0;
  stats[LineChecker.TYPE_LF] = 0;
  stats[LineChecker.TYPE_CRLF] = 0;

  while (this.position < this.text.length) {
    let end = LineChecker.findLineEnd(this.text, this.position);
    let kind = LineChecker.getEOLType(this.text, end);
    ++stats[kind];
    if (kind == LineChecker.TYPE_CR || kind == LineChecker.TYPE_LF) {
      this.position = end + 1;
    } else if (kind == LineChecker.TYPE_CRLF) {
      this.position = end + 2;
    } else {
      this.position = end;
    }
  }

  return stats;
}

LineChecker.findLineEnd = function(contents, position) {
  const LF_CODE = 0x0A;
  const CR_CODE = 0x0D;
  while (position < contents.length) {
    let unit = contents.codePointAt(position);
    if (unit != CR_CODE && unit != LF_CODE) {
      ++position;
      continue;
    }
    break;
  }
  return position;
}

LineChecker.getEOLType = function(contents, position) {
  const LF_CODE = 0x0A;
  const CR_CODE = 0x0D;
  if (position < contents.length) {
    let unit = contents.codePointAt(position);
    if (unit == CR_CODE) {
      if (LineChecker.getEOLType(contents, position + 1) ==
          LineChecker.TYPE_LF) {
        return LineChecker.TYPE_CRLF;
      }
      return LineChecker.TYPE_CR;
    } else if (unit == LF_CODE) {
      return LineChecker.TYPE_LF;
    }
  }
  return LineChecker.TYPE_NONE;
}

// NB: not ASCII codes--just enum-likes doubling as indexes into stats.
LineChecker.TYPE_NONE = 0;
LineChecker.TYPE_CR = 1;
LineChecker.TYPE_LF = 2;
LineChecker.TYPE_CRLF = 3;
