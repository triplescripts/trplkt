import { BrowserSystem } from "./BrowserSystem.js";
import { NodeJSSystem } from "./NodeJSSystem.js";

void function main() {
  function init($kind) {
    try {
      return new $kind;
    } catch (ex) {
      return null;
    }
  }

  if (!system) var system = init(BrowserSystem);
  if (!system) var system = init(NodeJSSystem);

  if (!system) {
    // Might be running in an Inaft test context
    if (typeof(module) != "undefined") {
      module.exports.LineChecker = LineChecker;
    } else {
      throw new Error("Unknown environment");
    }
  }

  system && system.run();
} ();
