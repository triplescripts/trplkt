import { SimpleScanner } from "./SimpleScanner.js";

export
class LineScanner extends SimpleScanner {
  constructor(text) {
    super(text);
  }

  static from(ref) {
    let result = new LineScanner(ref.text);
    result.position = ref.position;
    return result;
  }

  scanWholeLine() {
    while (this.tip) {
      if (this.readCodePoint() == "\n") {
        break;
      }
    }
    return this.position;
  }
}
