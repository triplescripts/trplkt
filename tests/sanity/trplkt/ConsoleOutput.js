export
class ConsoleOutput {
  constructor(printer) {
    this._printer = printer;
    this._contents = [];
  }

  push(text) {
    this._contents.push(text);
  }

  flush(sequence) {
    if (sequence != "\n") throw new Error("newline expected");
    let line = this._contents.splice(0, Infinity).join("");
    this._printer.printLine(line);
  }
}
