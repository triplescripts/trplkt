export
class BuildUnit {
  constructor(id) {
    this.id = id;

    this.text = null;
    this.priority = null;

    this.dependencies = [];
    this.finished = false;
  }

  static normalizePath(child, parent) {
    if (child.startsWith("./")) {
      child = child.substr(("./").length);
    }
    return parent.substr(0, parent.lastIndexOf("/")) + "/" + child;
  }
}
