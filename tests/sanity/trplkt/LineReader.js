import { ConsoleBuffer } from "./ConsoleBuffer.js";

export
class LineReader {
  constructor(vtty, writer) {
    this._writer = writer;

    this._prompt = "";
    this._text = "";
    this._cursor = 0;

    this.oncommand = null;

    this._update();
  }

  static formatPrompt(text) {
    if (text) {
      return text.trim() + " ";
    }
    return "";
  }

  setPrompt(text) {
    this._prompt = LineReader.formatPrompt(text);
    this._update();
  }

  _reset() {
    try {
      return this._text;
    } finally {
      this._text = "";
      this._cursor = 0;
      this._writer.commit();
      this._update();
    }
  }

  _update() {
    this._writer.render(this._text, this._cursor, this._prompt);
  }

  process(input) {
    let $ = String.fromCharCode;
    switch (input) {
      case $(LineReader.Backspace):
        this._doBackspace();
      break;
      case $(LineReader.Enter):
        this._doEnter();
      break;
      case LineReader.LeftSequence:
        this._doLeft();
      break;
      case LineReader.RightSequence:
        this._doRight();
      break;
      default:
        if (ConsoleBuffer.isPrintable(input)) {
          this._doInsertCharacter(input);
        } else {
          console.warn("dropping unrecognized input", input);
        }
      break;
    }
  }

  _doBackspace() {
    if (this._cursor > 0) {
      this._text = this._removeText(this._cursor);
      --this._cursor;
      this._update();
    }
  }

  _doEnter() {
    let command = this._reset();
    this._dispatch(this.oncommand, command);
  }

  _dispatch(handler, ...args) {
    if (handler) {
      handler.apply(null, args);
    }
  }

  _removeText(position, span = -1, original = this._text) {
    if (span < 0) {
      return original.substr(0, position + span) + original.substr(position);
    }
    return original.substr(0, position) + original.substr(position + span);
  }

  _doInsertCharacter(input) {
    this._text = this._insertText(input);
    ++this._cursor;
    this._update();
  }

  _insertText(payload, position = this._cursor, original = this._text) {
    return original.substr(0, position) + payload + original.substr(position);
  }

  _doLeft() {
    if (this._cursor > 0) {
      --this._cursor;
      this._update();
    }
  }

  _doRight() {
    if (this._cursor < this._text.length) {
      ++this._cursor;
      this._update();
    }
  }
}

LineReader.Backspace = 0x08;
LineReader.Enter = 0x0D;

LineReader.LeftSequence = "\x1B[D";
LineReader.RightSequence = "\x1B[C";
