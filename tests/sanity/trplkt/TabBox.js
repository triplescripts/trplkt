export
class TabBox {
  constructor(container) {
    this._tabToPanelMap = new Map();

    let doc = container.ownerDocument;
    let $$ = doc.createElement.bind(doc);

    let styleElement = container.appendChild($$("style"));
    TabBox.applySkinDefaults(styleElement.sheet);

    this._nav = container.appendChild($$("nav"));
    this._nav.className = "tab-strip";

    this.ontabswitch = null;
    this._nav.onchange = (event) => {
      this.selectTab(event.target.labels[0]);
    }

    this._main = container.appendChild($$("section"));
  }

  static applySkinDefaults(styleSheet) {
    let $ = styleSheet.insertRule.bind(styleSheet);
    $("input[name=tabs] {" +
        "display: none;" +
    "}");

    $(".tab-strip {" +
        "padding: .75em;" +
    "}");

    $(".tab-strip label {" +
        "box-sizing: border-box;" +
        "padding: inherit;" +
        "font-family: sans;" +
    "}");

    $(".tab-panel {" +
        "display: none;" +
    "}");

    $(".tab-panel.foreground {" +
        "display: block;" +
    "}");
  }

  addTab(name) {
    let doc = this._nav.ownerDocument;
    let $$ = doc.createElement.bind(doc);

    let tab = this._nav.appendChild($$("label"));
    tab.innerHTML = name + this._buildMarkup(
      "input", "type='radio' name='tabs'"
    );

    let panel = this._main.appendChild($$("div"));
    panel.className = "tab-panel";
    this._tabToPanelMap.set(tab, panel);

    if (this._nav.childNodes.length == 1) {
      this.selectTab(tab);
    }

    return panel;
  }

  selectTab(tab) {
    let foreground = this._nav.querySelector(".selected");
    if (foreground) foreground.classList.toggle("selected", false);
    tab.classList.toggle("selected", true);

    foreground = this._main.querySelector(".foreground");
    if (foreground) foreground.classList.toggle("foreground", false);
    let panel = this._tabToPanelMap.get(tab);
    panel.classList.toggle("foreground", true);

    if (this.ontabswitch) {
      this.ontabswitch.call(null, panel);
    }
  }

  _buildMarkup(name, attributesText = "") {
    return "<" + name + " " + attributesText + ">" + "<" + "/" + name + ">";
  }
}
