import { LinkageSniffer } from "./LinkageSniffer.js";
import { SimpleStringParser } from "./SimpleStringParser.js";
import { Sourcerer } from "./Sourcerer.js";

export
class JSDocEmitter {
  static forward(text) {
    let $etransform = (snippet) => {
      return "/** @exports */" + snippet.substr("export".length);
    };
    let $itransform = (list, specifier) => {
      specifier = SimpleStringParser.eval(specifier);
      return "/** @requires " + specifier + " */";
    };
    let sourcerer = new Sourcerer(text);
    let sniffer = new LinkageSniffer(text, LinkageSniffer.NORMAL_INPUT);
    return sourcerer.rewrite(sniffer, $etransform, $itransform);
  }
}
