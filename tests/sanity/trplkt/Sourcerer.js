import { LineScanner } from "./LineScanner.js";
import { LinkageSniffer } from "./LinkageSniffer.js";
import { SimpleScanner } from "./SimpleScanner.js";
import { SimpleStringParser } from "./SimpleStringParser.js";
import { TextScanner } from "./TextScanner.js";
import { TextSplicer } from "./TextSplicer.js";
import { TripleScanner } from "./TripleScanner.js";

export
class Sourcerer {
  constructor(text) {
    this._text = text;
  }

  static createBlock(contents, dBlock = false) {
    if (!dBlock) {
      var openDelimiter = Sourcerer.TBLOCK_OPEN;
      var doneDelimiter = Sourcerer.TBLOCK_DONE;
    } else {
      var openDelimiter = Sourcerer.DBLOCK_OPEN;
      var doneDelimiter = Sourcerer.DBLOCK_DONE;
    }
    let result = openDelimiter + contents;
    if (!contents.endsWith("\n")) result += "\n";
    result += doneDelimiter;
    return result;
  }

  static getModules(text, emitter = Sourcerer) {
    let sourcerer = new Sourcerer(text);
    let blocks = sourcerer.extractBlocks();

    let modules = [];
    for (let i = 0, n = blocks.length; i < n; ++i) {
      sourcerer = new Sourcerer(blocks[i]);
      modules.push({
        name: sourcerer.inferName(blocks[i]),
        text: emitter.reverse(blocks[i])
      });
    }
    return modules;
  }

  extractBlocks() {
    let blocks = [];
    let scanner = new TripleScanner(this._text);
    while (scanner.tip) {
      this._check(scanner.getNextToken(), TripleScanner.DELIMITER_OPEN);
      let start = LineScanner.from(scanner).scanWholeLine();
      scanner.position = start;
      let token = this._guard(scanner);
      while (token.type != TripleScanner.DELIMITER_CLOSE) {
        if (token.type != TripleScanner.WHITESPACE ||
            !token.content.endsWith("\n")) {
          scanner.position = LineScanner.from(scanner).scanWholeLine();
        }
        token = this._guard(scanner);
      }
      scanner.position = LineScanner.from(scanner).scanWholeLine();
      blocks.push(this._text.substring(start, token.position));
    }
    return blocks;
  }

  _check(token, kind) {
    if (!token || token.type != kind) {
      throw new Error("Malformed text");
    }
  }

  _guard(scanner) {
    if (!scanner.tip) {
      throw new Error("Malformed text");
    }
    return scanner.getNextToken();
  }

  inferName() {
    let scanner = new TextScanner(this._text);
    scanner.position = this._skipComments(this._text);
    let { content } = this._guard(scanner);
    if (content == "void") {
      scanner.position = this._skipOptional("(", scanner);
      content = this._guard(scanner).content;
    }
    if (content == "class" || content == "function" || content == "const" ||
        content == "var" || content == "let") {
      let { content } = this._guard(scanner);
      let parenPosition = content.indexOf("(");
      if (parenPosition >= 0) {
        return content.substring(0, parenPosition);
      }
      return content;
    }
    // XXX Special case for upgrades from trplkt <= 0.9.3, which allowed IIFEs
    // (e.g., for the shunting block's `main`) to be wrapped in parentheses.
    if (content.startsWith("(")) {
      throw new Error(
        "Malformed text; encountered token '(' when expecting " +
        "'void', 'class', 'function', 'const', 'var', or 'let'"
      );
    }
    throw new Error("Malformed text");
  }

  _skipComments(text) {
    let scanner = new TextScanner(text);
    let token = scanner.getNextToken();
    do {
      if (!token) break;
      if (token.content.startsWith("/*")) {
        let possibleEnding = false;
        let blockScanner = new SimpleScanner(text);
        blockScanner.position = token.position + "/*".length;
        while (blockScanner.position < blockScanner.text.length) {
          let seent = blockScanner.readCodePoint();
          if (possibleEnding && seent == "/") {
            break;
          }
          possibleEnding = (seent == "*");
        }
        scanner.position = blockScanner.position;
        token = scanner.getNextToken();
        if (!token) break;
      }
      // NB: not `else` here.
      if (!token.content.startsWith("/*")) {
        if (token.content.startsWith("//")) {
          scanner.position = LineScanner.from(scanner).scanWholeLine();
          token = scanner.getNextToken();
        } else {
          return token.position;
        }
      }
    } while (scanner.tip);
    return text.length;
  }

  _skipOptional(character, scanner) {
    if (scanner.tip == character) {
      scanner.readCodePoint();
    }
    return scanner.position;
  }

  static sniffImports(text) {
    let sniffer = new LinkageSniffer(text);
    return sniffer.getImports();
  }

  static getSuperClass(text) {
    let sourcerer = new Sourcerer(text);
    return sourcerer.extractSuperClass();
  }

  extractSuperClass() {
    let scanner = new TextScanner(this._text);
    scanner.position = this._skipComments(this._text);
    let token = this._guard(scanner);
    if (token.content == "class") {
      token = this._guard(scanner); // identifier
      token = this._guard(scanner);
      if (token.content == "extends") {
        token = this._guard(scanner);
        return token.content;
      }
    }
    return null;
  }

  static reverse(text) {
    let $etransform = (snippet) => {
      if (snippet.endsWith("...\n")) {
        return "export ";
      }
      return "export\n";
    }
    let $itransform = (list, specifier) => {
      return "import { " + list.join(", ") + " } from " + specifier + ";";
    }
    let sourcerer = new Sourcerer(text);
    let sniffer = new LinkageSniffer(text, LinkageSniffer.TRIPLE_INPUT);
    return sourcerer.rewrite(sniffer, $etransform, $itransform);
  }

  static forward(text, matchDBlocks = false) {
    const $ = (text) => ((matchDBlocks) ? "// " : "/// ") + text;
    let $etransform = (snippet) => {
      if (snippet.endsWith(" ")) {
        return $("export ...\n");
      }
      return $("export\n");
    };
    let $itransform = (list, specifier, cap) => {
      return $("import { " + list.join(", ") + " } from " + specifier + cap);
    };
    let sourcerer = new Sourcerer(text);
    let sniffer = new LinkageSniffer(text, LinkageSniffer.NORMAL_INPUT);
    return sourcerer.rewrite(sniffer, $etransform, $itransform);
  }

  rewrite(sniffer, $etransform, $itransform) {
    let result = new TextSplicer(this._text);

    let exports = sniffer.getExports();
    for (let i = 0, n = exports.length; i < n; ++i) {
      let [ start ] = exports[i].span, mark = exports[i].mark;
      let snippet = this._text.substring(start, mark);
      result.splice(start, mark, $etransform(snippet));
    }

    let imports = sniffer.getImports();
    for (let i = 0, n = imports.length; i < n; ++i) {
      let { list, specifier } = imports[i];
      let [ start, end ] = imports[i].span;
      let tail = this._text.substring(imports[i].et, end);
      result.splice(start, end, $itransform(list, specifier, tail));
    }

    return String(result);
  }
}

Sourcerer.TBLOCK_OPEN = "/// \x3cscript\x3e\n";
Sourcerer.TBLOCK_DONE = "/// \x3c/script\x3e\n";
Sourcerer.DBLOCK_OPEN = "// \x3cscript\x3e\n";
Sourcerer.DBLOCK_DONE = "// \x3c/script\x3e\n";
