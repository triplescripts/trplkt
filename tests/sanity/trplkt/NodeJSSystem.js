import { CLIParser } from "./CLIParser.js";
import { CheckedInit } from "./CheckedInit.js";
import { ConsoleOutput } from "./ConsoleOutput.js";
import { FileNodeFSAdapter } from "./FileNodeFSAdapter.js";
import { TripleKit } from "./TripleKit.js";
import { TrplktCommands } from "./TrplktCommands.js";
import { credits } from "./credits.js";

export
class NodeJSSystem {
  constructor() {
    if (typeof(process) == "undefined" || typeof(module) == "undefined") {
      throw new CheckedInit.ShuntingError("Not a NodeJS environment");
    }

    if (process.argv.indexOf("--version") >= 0) {
      console.log(credits);
      process.exit(0);
    }

    const fs = module.require("fs");
    const path = module.require("path");

    this._process = process;

    let [ engine, script, ...eargs ] = process.argv;
    this._engine = engine;
    this._script = script;
    this._args = eargs;

    this.input = null;
    this.output = new ConsoleOutput({
      printLine: (text) => void(console.log(text))
    });

    this._files = new FileNodeFSAdapter(fs, path);

    this._cli = TrplktCommands.initDefinitions();

    this.identity = fs.readFileSync(module.filename, { encoding: "utf8" });
  }

  run() {
    this._processArguments(this._cli, this._args).then(() => {
      this._process.exit(NodeJSSystem.EXIT_SUCCESS);
    }).catch((err) => {
      if (err instanceof TrplktCommands.KnownFatalError) {
        err = err.original;
      }
      if (err instanceof CLIParser.ArgError) {
        console.log(err.message + "\n");
        let commandList = "";
        let commands = this._cli.listCommands();
        for (let i = 0, n = commands.length; i < n; ++i) {
          commandList += "    " + commands[i] + "\n";
        }
        let engine = this._engine.substr(this._engine.lastIndexOf("/") + 1);
        let script = this._script.substr(this._script.lastIndexOf("/") + 1);
        console.log(
          "Usage: " + engine + " " + script + " <command> FILE" + "\n\n" +
          "Available commands:" + "\n\n" + commandList
        );
        var status = NodeJSSystem.EXIT_BADARGS;
      } else if (err instanceof TripleKit.NoSuchFileError) {
        var status = NodeJSSystem.EXIT_BADFILE;
      } else if (err instanceof TripleKit.DialectError) {
        var status = NodeJSSystem.EXIT_BADTEXT;
      } else {
        console.error("Unrecognized error:", err);
        var status = NodeJSSystem.EXIT_EXECERR;
      }
      this._process.exit(status);
    });
  }

  _processArguments(parser, args) {
    let params = parser.handle(args);
    if (!params) return Promise.reject(parser.error);
    let commands = new TrplktCommands(this);
    return commands.executeStructuredCommand(parser.command, params);
  }

  read(path) {
    return this._files.read(path).catch((err) => {
      return this._wrapFSError(err, path);
    });
  }

  write(path, contents) {
    return this._files.write(path, contents).catch((err) => {
      return this._wrapFSError(err, path);
    });
  }

  _wrapFSError(nodeError, path) {
    switch (nodeError.code) {
      case "ENOENT":
        return Promise.reject(new TripleKit.NoSuchFileError(path));
      break;
    }
    return Promise.reject(nodeError);
  }
}

NodeJSSystem.EXIT_EXECERR = -1;
NodeJSSystem.EXIT_SUCCESS = 0;
NodeJSSystem.EXIT_BADARGS = 1;
NodeJSSystem.EXIT_BADFILE = 2;

NodeJSSystem.EXIT_BADTEXT = 10;
