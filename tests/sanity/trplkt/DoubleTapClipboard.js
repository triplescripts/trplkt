export
class DoubleTapClipboard {
  constructor(target, container) {
    this._injectNodes(container);

    this._target = target;

    this.onpaste = null;

    this._target.addEventListener("keydown", this);
    this._input.addEventListener("keydown", this);
    this._input.addEventListener("paste", this);
  }

  _injectNodes(container) {
    let $$ = container.ownerDocument.createElement.bind(
      container.ownerDocument
    );

    container.style.position = "relative";

    this._input = container.appendChild($$("textarea"));
    this._input.placeholder = "Double-Tap Clipboard (Paste again\u2026)";
    this._input.style.display = "none";
    this._input.style.position = "absolute";
    this._input.style.bottom = "0";
    this._input.style.right = "0";
    this._input.style.fontSize = "15px";
    this._input.style.height = "12em";
    this._input.style.width = "40ch";
    this._input.className = "term-clipboard";
  }

  handleEvent(event) {
    if (event.target == this._input) {
      if (event.type == "paste") {
        this._input.value = event.clipboardData.getData("text");
        this._onAcceptPaste();
      } else if (event.keyCode == DoubleTapClipboard.Enter && !event.shiftKey) {
        this._cancelEvent(event);
        this._onAcceptPaste();
      } else if (event.keyCode == DoubleTapClipboard.Escape) {
        this._input.value = "";
        this.dismiss();
      }
    }
    if (event.target == this._target) {
      if (event.ctrlKey && event.key == "v") {
        this._cancelEvent(event);
        this._onKeyCombo();
      }
    }
  }

  _cancelEvent(event) {
    event.stopPropagation();
    event.preventDefault();
  }

  _onKeyCombo(event) {
    this._input.style.display = "block";
    this._input.focus();
    this._input.select();
  }

  _onAcceptPaste(event) {
    this._dispatch(this.onpaste, this._input.value);
    this.dismiss();
  }

  dismiss() {
    this._input.style.display = "none";
    this._target.focus();
  }

  _dispatch(handler, ...args) {
    if (handler) {
      handler.apply(null, args);
    }
  }
}

DoubleTapClipboard.Enter = 0x0D;
DoubleTapClipboard.Escape = 0x1B;
