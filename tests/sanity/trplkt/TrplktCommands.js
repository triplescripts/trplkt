import { CLIParser } from "./CLIParser.js";
import { Creditizor } from "./Creditizor.js";
import { TextScanner } from "./TextScanner.js";
import { TripleKit } from "./TripleKit.js";

export
class TrplktCommands {
  constructor(system) {
    this._system = system;

    this.onbuild = null;
    this.oncreatecredits = null;
    this.ondecompile = null;

    TrplktCommands.initErrorHandlers(this, this._system);
  }

  static initDefinitions() {
    let parser = new CLIParser();
    parser.addCommand("build", (params) => {
      parser.constrain("mode", TrplktCommands.BUILD);
    });
    parser.addCommand("create-credits", (params) => {
      parser.constrain("mode", TrplktCommands.CREDITS);
    });
    parser.addCommand("decompile", (params) => {
      parser.constrain("mode", TrplktCommands.DECOMPILE);
    });
    parser.addCommand("init", (params) => {
      parser.constrain("mode", TrplktCommands.INIT);
    });

    parser.addDefaultCommand("build", (value) => {
      return value.includes(".") || value.includes("/");
    });

    parser.addOption("-d", "--emit-dblocks", (params) => {
      parser.check("mode", [ TrplktCommands.BUILD ]);
    });
    parser.addOption("-m", null, (params, value) => {
      parser.check("mode", [ TrplktCommands.BUILD ]);
      params.set("shunt", value);
    });
    parser.addOption("-o", null, (params, value) => {
      parser.check("mode", [ TrplktCommands.BUILD, TrplktCommands.CREDITS ]);
      params.set("outfile", value);
    });
    parser.addOption("-t", null, (params) => {
      parser.check("mode", [ TrplktCommands.BUILD ]);
      params.set("use-triplicate", true);
    });
    parser.addOption("-x", "--extract", (params, value) => {
      parser.check("mode", [ TrplktCommands.DECOMPILE ]);
      parser.addParamListItem("xlist", value);
    });

    parser.addOption("-f", null, (params, value) => {
      if (params.get("mode") != TrplktCommands.BUILD) {
        parser.constrain("flist", undefined, "Too many files specified");
      }
      parser.addParamListItem("flist", value);
    });
    parser.addDefaultOption("-f");
    parser.require("flist", (params) => {
      if (params.get("mode") == TrplktCommands.INIT) {
        return "File name required";
      }
      return "Input file name required";
    });

    return parser;
  }

  execute(commandAndArgsString) {
    let name, scanner = new TextScanner(commandAndArgsString);
    let parts = [ name = this._getNextText(scanner) ];
    if (!name) return Promise.resolve();
    while (scanner.tip) {
      let next = this._getNextText(scanner);
      if (next) parts.push(next);
    }

    let parser = TrplktCommands.initDefinitions();
    let params = parser.handle(parts);
    if (!params) {
      this._system.output.push(parser.error.message);
      this._system.output.flush("\n");
      return Promise.reject(
        new TrplktCommands.KnownFatalError(parser.error)
      );
    }
    return this.executeStructuredCommand(parser.command, params);
  }

  executeStructuredCommand(name, params) {
    switch (name) {
      case "build": return this._commandBuild(params);
      case "create-credits": return this._commandCreateCredits(params);
      case "decompile": return this._commandDecompile(params);
      case "init": return this._commandInit(params);
    }
    // XXX Shouldn't ever reach this point (at least not from the terminal or
    // in-browser UIs; but maybe we're being used as a lib?)
    throw new Error("invalid command: " + name);
  }

  _dispatch(handler, ...args) {
    if (handler) handler.apply(null, args);
  }

  _commandInit(params) {
    let fileName = params.get("flist")[0];
    let kit = new TripleKit(this._system);
    return kit.init(fileName).then(() => {
      this._dispatch(this.oninit, fileName);
    });
  }

  _commandBuild(params) {
    let $ = params.get.bind(params);
    let kit = new TripleKit(this._system);

    let fileName = $("outfile") || "out.app.htm";
    kit.useTriplicate = !!$("use-triplicate");
    kit.emitDBlocks = ($("emit-dblocks") == undefined) ?
      !kit.useTriplicate :
      true;

    return kit.build($("flist"), $("shunt"), fileName).then(() => {
      this._dispatch(this.onbuild, fileName);
    }).catch((err) => (
      this.$checkFileReadErrors(err) ||
      this.$checkDialectErrors(err) ||
      Promise.reject(err)
    ));
  }

  _commandCreateCredits(params) {
    let $ = params.get.bind(params);
    let [ inputPath ] = $("flist");
    let outputPath = $("outfile") || "credits.js";
    let kit = new TripleKit(this._system);
    return kit.createCredits(inputPath, outputPath).then(() => {
      this._dispatch(this.oncreatecredits, outputPath);
    }).catch((err) => (
      this.$checkFileReadErrors(err) ||
      this.$checkInvalidCreditsError(err) ||
      Promise.reject(err)
    ));
  }

  _commandDecompile(params) {
    let $ = params.get.bind(params);
    // assert(params.get("flist").length == 1)
    let [ fileName ] = $("flist");
    let extractions = $("xlist");
    let kit = new TripleKit(this._system);
    return kit.decompile(fileName, extractions).then((names) => {
      this._dispatch(this.ondecompile, names);
    }).catch((err) => (
      this.$checkFileReadErrors(err) ||
      Promise.reject(err)
    ));
  }

  _getNextText(scanner) {
    let token = scanner.getNextToken();
    return token && token.content;
  }

  static initErrorHandlers(commands, system) {
    const { check } = TrplktCommands.KnownFatalError;

    const { DialectError } = TripleKit;
    const { InputFormatError } = Creditizor;
    const { NoFileSourceError } = TripleKit;
    const { NoSuchFileError } = TripleKit;

    function $log(text) {
      if (system.output) {
        system.output.push(text);
        system.output.flush("\n");
      }
    }

    let $invalidCredits = (err) => $log(err.message + ": " + err.path);
    let $noFileSource = (err) => $log(err.message);
    let $noSuchFile = (err) => $log(err.message + ": " + err.path);
    let $syntaxIssue = (err) => {
      $log(err.message + " for " + err.path);
      if (err.inner) {
        $log(err.inner.message);
      }
    };

    commands.$checkFileReadErrors = (err) => {
      if (!wrap) var wrap = check(err, NoFileSourceError, $noFileSource);
      if (!wrap) var wrap = check(err, NoSuchFileError, $noSuchFile);
      return wrap && Promise.reject(wrap);
    }

    commands.$checkInvalidCreditsError = (err) => {
      var wrap = check(err, InputFormatError, $invalidCredits);
      return wrap && Promise.reject(wrap);
    }

    commands.$checkDialectErrors = (err) => {
      var wrap = check(err, DialectError, $syntaxIssue);
      return wrap && Promise.reject(wrap);
    }
  }
}

// Use this for errors that are recognized as fatal and known to have already
// been handled (with feedback to the user).  For other, unrecognized errors,
// do not wrap them--just let them escape, and they'll be treated as fatal,
// too, but with scarier, developer-oriented output.
TrplktCommands.KnownFatalError = class KnownFatalError extends Error {
  constructor(original) {
    if (original instanceof TrplktCommands.KnownFatalError) {
      return original;
    }
    super("Cannot proceed with operation");
    this.original = original;
  }

  static check(err, kind, $handler = null) {
    if (err instanceof TrplktCommands.KnownFatalError) {
      return err;
    }
    if (err instanceof kind) {
      if ($handler) $handler.call(null, err);
      return new TrplktCommands.KnownFatalError(err);
    }
    return null;
  }
}

TrplktCommands.DECOMPILE = -1;
TrplktCommands.INIT = 0;
TrplktCommands.BUILD = 1;

TrplktCommands.CREDITS = 10;
