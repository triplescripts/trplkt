import { BrowserQuirksHack } from "./BrowserQuirksHack.js";
import { BrowserUI } from "./BrowserUI.js";
import { CheckedInit } from "./CheckedInit.js";
import { ConsoleOutput } from "./ConsoleOutput.js";
import { FileCache } from "./FileCache.js";
import { TripleKit } from "./TripleKit.js";
import { TrplktCommands } from "./TrplktCommands.js";
import { WebkitFileIngester } from "./WebkitFileIngester.js";
import { ZodLib } from "./ZodLib.js";
import { credits } from "./credits.js";

export
class BrowserSystem {
  constructor() {
    if (typeof(window) == "undefined") {
      throw new CheckedInit.ShuntingError("Not a browser environment");
    }

    this.identity = window.document.body.innerHTML;

    this._document = window.document;
    this._quirksHack = new BrowserQuirksHack(window);

    this._directoryInput = null;
    this._fileSource = null;

    this._artifacts = new FileCache();

    this.output = new ConsoleOutput({
      printLine: (text) => void(this._ui.printFeedback(text))
    });
  }

  run() {
    this._document.defaultView.onload = () => {
      if (this._quirksHack.wrap()) return;

      this._ui = new BrowserUI(this, this._document);

      let $$ = this._document.createElement.bind(this._document);

      let fileInput = $$("input");
      fileInput.setAttribute("type", "file");
      fileInput.setAttribute("webkitdirectory", "true");
      fileInput.style.display = "none";
      this._directoryInput = this._document.head.appendChild(fileInput);

      this._directoryInput.onchange = this._onDirectoryChange.bind(this);

      let $exportFile = this._export.bind(this);
      this._commands = new TrplktCommands(this);
      this._commands.onbuild = $exportFile;
      this._commands.oncreatecredits = $exportFile;
      this._commands.oninit = $exportFile;
      this._commands.ondecompile = this._onScriptDecompiled.bind(this);
    }
  }

  read(path) {
    if (this._fileSource) {
      let file = this._fileSource.recall(path);
      return WebkitFileIngester.readBlob(file).then((contents) => {
        if (contents === null) {
          return Promise.reject(new TripleKit.NoSuchFileError(path));
        }
        return Promise.resolve(contents);
      });
    }
    return Promise.reject(new TripleKit.NoFileSourceError);
  }

  write(path, contents) {
    this._artifacts.store(new Blob([ contents ]), path);
    return Promise.resolve();
  }

  _export(fileName) {
    this._saveFile(fileName, this._artifacts.recall(fileName));
  }

  _saveFile(name, blob) {
    let anchor = this._document.createElement("a");
    anchor.href = URL.createObjectURL(blob);
    anchor.download = name;
    this._document.head.appendChild(anchor).click();
    anchor.parentNode.removeChild(anchor);
  }

  _onScriptDecompiled(names) {
    this._createZIP(this._artifacts, names).then((data) => {
      this._saveFile("src.zip", new Blob([ data ]));
    });
  }

  _createZIP(cache, names = cache.list()) {
    let archive = ZodLib.createArchive();

    let pending = names.map((path) =>
      WebkitFileIngester.readBlob(cache.recall(path), true).then((buffer) => {
        archive.addFile(path, buffer);
      })
    );

    return Promise.all(pending).then(() =>
      Promise.resolve(archive.zip())
    );
  }

  // NB: internal use only (for UI; not part of the abstract system interface)
  requestFileSource() {
    this._directoryInput.click();
  }

  _onDirectoryChange(event) {
    this._fileSource = WebkitFileIngester.cache(event.target.files);
    this._ui.acknowledgeFileSource();
  }

  // NB: internal use only (for UI; not part of the abstract system interface)
  executeCommand(escapedText) {
    let commandText = this._unescape(escapedText);
    let checkSyncAndAsyncErrors = (() => {
      try {
        return this._commands.execute(commandText).catch((err) => {
          // Errors wrapped like this are already dealt with.
          if (err instanceof TrplktCommands.KnownFatalError) return;
          // Anything else is unexpected/uncaught.
          return Promise.reject(err);
        });
      } catch (ex) {
        // ... and we want to handle non-async errors, too; they get converted
        // to async ones so they can be handled just the same as those above.
        return Promise.reject(ex);
      }
    });

    checkSyncAndAsyncErrors().catch((err) => {
      this._ui.printFeedback("Encountered internal error.");
      console.error(err);
    });
  }

  _unescape(commandLine) {
    let $ = (x, offset) => String.fromCodePoint(x.codePointAt(offset));
    let result = "";
    for (let i = 0, n = commandLine.length; i < n;) {
      let codePoint = $(commandLine, i);
      if (codePoint == "\\") {
        switch ($(commandLine, ++i)) {
          case "\\":
            result += "\\";
          break;
          case " ":
          case "\n":
            result += " ";
          break;
          default: throw new Error("Malformed escape sequence");
        }
      } else {
        result += codePoint;
        i += codePoint.length;
      }
    }
    return result;
  }
}
