export
class TextSplicer {
  constructor(text) {
    this._source = text;
    this._splices = [];
  }

  splice(start, end, contents) {
    this._splices.push(new TextSplicer.Splice(start, end, contents));
  }

  toString() {
    this._splices = this._splices.sort((a, b) => (a.start - b.start));

    let parts = [];
    // NB: `prefaceMark` and `end` may end up with value `undefined` in a few
    // different cases.  It's okay; `substring` will still work how we want.
    for (let i = 0, n = this._splices.length; i < n; ++i) {
      let prefaceMark = end;
      var { start, end } = this._splices[i];
      if (start < prefaceMark) throw new Error("Overlapping splices");
      parts.push(this._source.substring(prefaceMark, start));
      parts.push(this._splices[i].contents);
    }
    parts.push(this._source.substring(end));

    return parts.join("");
  }
}

TextSplicer.Splice = class Splice {
  constructor(start, end, contents) {
    this.start = start;
    this.end = end;
    this.contents = contents;
  }
}
