import { LineScanner } from "./LineScanner.js";
import { SimpleScanner } from "./SimpleScanner.js";
import { SimpleStringParser } from "./SimpleStringParser.js";

export class LinkageSniffer extends SimpleScanner {
  constructor(text, inputType) {
    super(text);
    this._findings = null;
    this._mode = inputType;
  }

  getImports() {
    if (!this._findings) this._findings = this._sniff();
    return this._findings.imports;
  }

  getExports() {
    if (!this._findings) this._findings = this._sniff();
    return this._findings.exports;
  }

  _sniff() {
    if (this.position != 0) throw new Error("Re-entrant parse disallowed");
    let result = { imports: [], exports: [] };
    let token = this.getToken(true);
    while (token) {
      let slashing;
      if (this._mode == LinkageSniffer.TRIPLE_INPUT &&
          token.type == LinkageSniffer.TRIPLE_START) {
        slashing = token;
        token = this.getToken();
      }

      if (token.type == LinkageSniffer.IMPORT) {
        result.imports.push(this._parseImportDeclaration(token, slashing));
        this.position = LineScanner.from(this).scanWholeLine();
      } else if (token.type == LinkageSniffer.EXPORT) {
        result.exports.push(this._parseExportDeclaration(token, slashing));
        this.position = LineScanner.from(this).scanWholeLine();
      } else if (token.type != SimpleScanner.WHITESPACE) {
        this.position = LineScanner.from(this).scanWholeLine();
      } else if (!token.content.endsWith("\n")) {
        this.position = LineScanner.from(this).scanWholeLine();
      }
      token = this.getToken(true);
    }
    return result;
  }

  getToken(includeGaps = false) {
    const { WHITESPACE, UNKNOWN } = SimpleScanner;
    do {
      var result = this.getGenericToken();
    } while (result && result.type == WHITESPACE && !includeGaps);
    if (result && result.type == UNKNOWN) {
      switch (this.tip) {
        case ";":
          result.type = LinkageSniffer.SEMICOLON;
          result.content = this.readCodePoint();
        break;
        case ",":
          result.type = LinkageSniffer.COMMA;
          result.content = this.readCodePoint();
        break;
        case "{":
          result.type = LinkageSniffer.OPENED_CURLY;
          result.content = this.readCodePoint();
        break;
        case "}":
          result.type = LinkageSniffer.CLOSED_CURLY;
          result.content = this.readCodePoint();
        break;
        case "*":
          result.type = LinkageSniffer.ASTERISK;
          result.content = this.readCodePoint();
        break;
        case "'":
        case '"':
          result.content = this._scanString();
          result.type = LinkageSniffer.STRING;
        break;
        default:
          if (this.tip == "$" || this.tip == "_" ||
              this._isLetter(this.tip)) {
            result.content = this._scanWord();
            switch (result.content) {
              case "export":
                result.type = LinkageSniffer.EXPORT;
              break;
              case "from":
                result.type = LinkageSniffer.FROM;
              break;
              case "import":
                result.type = LinkageSniffer.IMPORT;
              break;
              default:
                result.type = LinkageSniffer.WORD;
              break;
            }
          } else if (this.tip == "/") {
            const { TRIPLE_START } = LinkageSniffer;
            result.content = this._scanRepeated("/", 3);
            result.type = this._accept(result, "/// ", TRIPLE_START);
          } else if (this.tip == ".") {
            const { CONTINUATION } = LinkageSniffer;
            result.content = this._scanRepeated(".", 3);
            result.type = this._accept(result, "...\n", CONTINUATION);
          } else {
            // ... again UNKNOWN, but here we need to advance.
            result.content = this.readCodePoint();
          }
        break;
      }
    }
    return result;
  }

  _scanRepeated(character, max = Infinity) {
    let result = "";
    for (let i = 0; i < max; ++i) {
      if (this.tip != character) break;
      result += this.readCodePoint();
    }
    return result;
  }

  _accept(token, match, kind) {
    if (token.content + this.tip == match) {
      token.content += this.readCodePoint();
      token.type = kind;
    }
    return token.type;
  }

  _scanString() {
    const offset = this.position;
    // NB: This relies on the parser's implementation details.
    let parser = new SimpleStringParser({
      codePointAt: (index) => (!parser.literal) ?
        this.text.codePointAt(offset + index) :
        undefined
    });
    let { literal } = parser.run();
    this.position += literal.length;
    return literal;
  }

  _scanWord() {
    let result = this.readCodePoint();
    while (this._isLetter(this.tip) || this._isDigit(this.tip) ||
           this.tip == "$" || this.tip == "_") {
      result += this.readCodePoint();
    }
    return result;
  }

  _isLetter(x) {
    return ("A" <= x && x <= "Z") || ("a" <= x && x <= "z");
  }

  _isDigit(x) {
    return ("0" <= x && x <= "9");
  }

  _parseImportDeclaration(token, trigger = token) {
    let start = trigger.position;
    let specifier, list = [];
    token = this._expectToken(this.getToken());
    if (token.type != LinkageSniffer.OPENED_CURLY) {
      throw new Error("Only explicitly named imports supported");
    }
    token = this._expectToken(this.getToken());
    if (token.type == LinkageSniffer.ASTERISK) {
      throw new Error("Wildcard imports not supported");
    }
    list.push(this._expectToken(token, LinkageSniffer.WORD).content);
    token = this._expectToken(this.getToken());
    while (token.type != LinkageSniffer.CLOSED_CURLY) {
      if (token.type == LinkageSniffer.WORD && token.content == "as") {
        throw new Error("Namespace rebinding for imports not supported");
      }
      this._expectToken(token, LinkageSniffer.COMMA);
      let word = this._expectToken(this.getToken(), LinkageSniffer.WORD);
      list.push(word.content);
      token = this._expectToken(this.getToken());
    }
    this._expectToken(token, LinkageSniffer.CLOSED_CURLY);
    token = this._expectToken(this.getToken(), LinkageSniffer.FROM);
    token = this._expectToken(this.getToken(), LinkageSniffer.STRING);
    let et = token.position + token.content.length;
    specifier = token.content;
    if (this.tip && this.tip != "\n") {
      token = this._expectToken(this.getToken(), LinkageSniffer.SEMICOLON);
    }
    let end = token.position + token.content.length;
    return { list: list, specifier: specifier, span: [ start, end ], et: et };
  }

  _parseExportDeclaration(token, trigger = token) {
    let start = trigger.position;
    token = this._expectToken(this.getToken(true), SimpleScanner.WHITESPACE);
    if (token.content == " ") {
      if (this._mode == LinkageSniffer.TRIPLE_INPUT) {
        token = this._expectToken(this.getToken(), LinkageSniffer.CONTINUATION);
      }
    } else if (token.content != "\n") {
      throw new Error("Weird spacing"); // XXX
    }
    token = this._expectToken(this.getToken(), LinkageSniffer.WORD);
    let mark = token.position;
    let kind = token.content;
    if (kind != "class" && kind != "function" && kind != "const" &&
        kind != "var" && kind != "let") {
      throw new Error("Unsupported export: " + JSON.stringify(kind));
    }
    token = this._expectToken(this.getToken(), LinkageSniffer.WORD);
    let name = token.content;
    return { kind: kind, name: name, span: [ start, undefined ], mark: mark };
  }

  _expectToken(token, type) {
    if (token == null) throw new Error("Unexpected EOF");
    if (typeof(type) != "undefined" && token.type != type) {
      throw new Error("Unexpected token: " + JSON.stringify(token.content));
    }
    return token;
  }
}

LinkageSniffer.NORMAL_INPUT = 0;
LinkageSniffer.TRIPLE_INPUT = 1;

// NB: SimpleScanner.WHITESPACE is 0
LinkageSniffer.WORD = 1;
LinkageSniffer.SEMICOLON= 2;
LinkageSniffer.COMMA = 3;
LinkageSniffer.OPENED_CURLY = 4;
LinkageSniffer.CLOSED_CURLY = 5;
LinkageSniffer.ASTERISK = 6;
LinkageSniffer.STRING = 9;
LinkageSniffer.IMPORT = 11;
LinkageSniffer.FROM = 12;
LinkageSniffer.EXPORT = 21;
LinkageSniffer.CONTINUATION = 22;
LinkageSniffer.TRIPLE_START = 31;
