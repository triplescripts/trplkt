import { BrowserSystem } from "./BrowserSystem.js";
import { ConsoleBuffer } from "./ConsoleBuffer.js";
import { ConsoleOutput } from "./ConsoleOutput.js";
import { ConsoleWidget } from "./ConsoleWidget.js";
import { LineReader } from "./LineReader.js";
import { LineWriter } from "./LineWriter.js";
import { TabBox } from "./TabBox.js";
import { TrplktAppStyle } from "./TrplktAppStyle.js";

export
class BrowserUI {
  constructor(system, document) {
    this._system = system;
    this._document = document;

    this._vtty = new ConsoleBuffer();
    let writer = new LineWriter(this._vtty);
    this.input = new LineReader(this._vtty, writer);
    this.output = new ConsoleOutput(writer);

    this._document.body.innerHTML = "";
    this._injectUI(this._document.body);

    this.input.oncommand = this._onConsoleCommand.bind(this);
  }

  _injectUI(container) {
    TrplktAppStyle.applySkin(this._document.body.parentNode);

    let $$ = this._document.createElement.bind(this._document);

    let loadArea = container.appendChild($$("div"));
    loadArea.className = "load-area";

    let loadButton = $$("label");
    loadButton.className = "empty file-source-chooser";
    loadButton.textContent = "Open\u2026";
    loadButton.style.fontFamily = "sans";
    this._loadButton = loadArea.appendChild(loadButton);

    this._loadButton.onclick = () => void(this._system.requestFileSource());

    this._tabBox = new TabBox(container);
    // let mainPanel = this._tabBox.addTab("Main");
    // let buildsPanel = this._tabBox.addTab("Artifacts");
    let consolePanel = this._tabBox.addTab("Console");

    consolePanel.classList.add("console-panel");
    this.console = new ConsoleWidget(consolePanel, this._vtty, this.input);
    this.console.focus();

    this._tabBox.ontabswitch = (panel) => {
      if (panel == consolePanel) this.console.focus();
    }
  }

  _onConsoleCommand(text) {
    this._system.executeCommand(text);
  }

  acknowledgeFileSource() {
    this._loadButton.classList.remove("empty");
  }

  printFeedback(text) {
    this.output.push(text);
    this.output.flush("\n");
  }
}
