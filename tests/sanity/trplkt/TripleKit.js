import { BuildUnit } from "./BuildUnit.js";
import { Creditizor } from "./Creditizor.js";
import { JSDocEmitter } from "./JSDocEmitter.js";
import { Sourcerer } from "./Sourcerer.js";

export
class TripleKit {
  constructor(system) {
    this._system = system;

    // configuration defaults
    this.emitDBlocks = false;
    this.useTriplicate = true;
  }

  createCredits(inputPath, outputPath) {
    return this._system.read(inputPath).then((input) => {
      try {
        var output = Creditizor.process(input, inputPath);
      } catch (ex) {
        return Promise.reject(ex);
      }

      this._log("writing " + outputPath);
      return this._system.write(outputPath, output);
    });
  }

  init(path) {
    return this._system.write(path, this._system.identity);
  }

  decompile(path, extractions = null) {
    return this._system.read(path).then((contents) => {
      this._log("decompiling " + path);
      let names = [], blockers = [];

      let modules = Sourcerer.getModules(contents);

      for (let i = 0, n = modules.length; i < n; ++i) {
        let fileName = modules[i].name + ".js";
        if (!extractions || extractions.indexOf(modules[i].name) >= 0) {
          this._log("  module " + fileName + "...");
          blockers.push(this._system.write(fileName, modules[i].text));
          names.push(fileName);
        }
      }

      return new Promise((resolve) =>
        Promise.all(blockers).then(() => resolve(names))
      );
    });
  }

  build(paths, shunt, fileName = "out.app.htm") {
    let cache = new Map();
    let order = paths.slice();
    if (shunt) order.push(shunt);

    let pending = [];
    for (let i = 0, n = order.length; i < n; ++i) {
      if (!this.useTriplicate) var emitter = JSDocEmitter;
      pending.push(this._processBuildUnit(order[i], cache, emitter));
    }

    return Promise.all(pending).then(() => {
      let text = this._collateUnits(cache, order, shunt);
      this._log("writing " + fileName);
      return this._system.write(fileName, text);
    });
  }

  _processBuildUnit(id, cache, algo = Sourcerer) {
    return this._system.read(id).then((contents) => {
      let pending = [];
      let unit = this._getCachedBuildUnit(id, cache);

      if (unit.text == null) {
        if (this.emitDBlocks) var matchDBlocks = true;
        let partial = algo.forward(contents, matchDBlocks);
        pending.push(this._asyncNameGuard(id, partial));

        unit.text = Sourcerer.createBlock(partial, this.emitDBlocks);

        let superClass = Sourcerer.getSuperClass(partial);
        let imports = Sourcerer.sniffImports(contents);
        for (let i = 0, n = imports.length; i < n; ++i) {
          let { specifier, list } = imports[i];
          let relativePath = SimpleStringParser.eval(specifier);
          let importID = BuildUnit.normalizePath(relativePath, id);
          unit.dependencies.push(this._getCachedBuildUnit(importID, cache));

          if (list.indexOf(superClass) >= 0) {
            unit.priority = importID;
          }

          pending.push(this._processBuildUnit(importID, cache, algo));
        }
      }

      return Promise.all(pending);
    });
  }

  _getCachedBuildUnit(id, cache) {
    if (!cache.has(id)) {
      cache.set(id, new BuildUnit(id));
    }
    return cache.get(id);
  }

  _asyncNameGuard(id, text) {
    let sourcerer = new Sourcerer(text);
    let expectedName = this._stripPathDetails(id);
    try {
      let actualName = sourcerer.inferName();
      if (actualName != expectedName) {
        throw new Error(
          "Name mismatch; expected " + expectedName + ", got " + actualName
        );
      }
    } catch (ex) {
      return Promise.reject(new TripleKit.DialectError(id, ex));
    }
  }

  _stripPathDetails(id) {
    let fileName = id.substring(id.lastIndexOf("/") + 1);
    return fileName.substring(0, fileName.indexOf("."));
  }

  _collateUnits(cache, ids, shunt) {
    let result = "";

    while (ids.length) {
      let unit = cache.get(ids.shift());
      let prereq = cache.get(unit.priority);

      if (unit.finished) continue;
      if (prereq && !prereq.finished) {
        ids.unshift(unit.id);
        ids.unshift(prereq.id);
        continue;
      }

      if (unit.id == shunt) {
        var shuntingBlock = unit.text;
      } else {
        result += unit.text;
        unit.finished = true;
      }

      ids.unshift(...unit.dependencies.map((x) => x.id));
    }

    if (shuntingBlock) {
      result += shuntingBlock;
    }

    return result;
  }

  _log(text) {
    let { output } = this._system;
    if (output) {
      output.push(text);
      output.flush("\n");
    }
  }
}

TripleKit.NoSuchFileError = class NoSuchFileError extends Error {
  constructor(path) {
    super("No such file");
    this.path = path;
  }
}

TripleKit.NoFileSourceError = class NoFileSourceError extends Error {
  constructor() {
    super("No file source");
  }
}

TripleKit.DialectError = class DialectError extends Error {
  constructor(path, inner = null) {
    super("Malformed text");
    this.path = path;
    this.inner = inner;
  }
}
