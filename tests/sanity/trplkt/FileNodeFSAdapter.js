export
class FileNodeFSAdapter {
  constructor(fs, path, workDir = ".") {
    this._nodeFS = fs;
    this._nodePath = path;
    this._workDir = workDir;
  }

  read(path) {
    if (!path.startsWith("/")) {
      path = this._nodePath.join(this._workDir, path);
    }
    const options = { encoding: "utf8" };
    return new Promise((resolve, reject) => {
      void this._nodeFS.readFile(path, options, (err, contents) => {
        if (err) {
          reject(err);
        } else {
          resolve(contents);
        }
      });
    });
  }

  write(path, contents) {
    if (!path.startsWith("/")) {
      path = this._nodePath.join(this._workDir, path);
    }
    return new Promise((resolve, reject) => {
      this._nodeFS.writeFile(path, contents, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }
}
