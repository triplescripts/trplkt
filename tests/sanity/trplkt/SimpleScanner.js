export
class SimpleScanner {
  constructor(text) {
    this.text = text;
    this.position = 0;
  }

  get tip() {
    let unit = this.text.codePointAt(this.position);
    if (unit == undefined) return "";
    return String.fromCodePoint(unit);
  }

  getGenericToken() {
    if (this.tip == "") return null; // EOF
    let result = { position: this.position };
    if (this.isWhitespace(this.tip)) {
      result.content = "";
      do {
        result.content += this.readCodePoint();
      } while (this.isWhitespace(this.tip));
      result.type = SimpleScanner.WHITESPACE;
    } else {
      result.type = SimpleScanner.UNKNOWN;
    }
    return result;
  }

  readCodePoint() {
    let result = this.tip;
    this.position += result.length;
    return result;
  }

  isWhitespace(x) {
    return (x == " " || x == "\t" || x == "\r" || x == "\n");
  }
}

SimpleScanner.UNKNOWN = -1;
SimpleScanner.WHITESPACE = 0;
