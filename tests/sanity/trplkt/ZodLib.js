export
class ZodLib {
  constructor() {
    throw new Error("Not permitted; use ZodLib.createArchive");
  }

  static createArchive() {
    let result = Object.create(ZodLib.prototype);
    let $initializer = (function() {
      this._fileNames = [];
      this._fileBuffers = new Map();
      return this;
    });
    return $initializer.apply(result);
  }

  addFile(name, contents) {
    this._fileBuffers.set(name, contents);
  }

  zip() {
    // For info on the file format, refer to:
    // - <https://www.pkware.com/documents/APPNOTE/APPNOTE-6.2.0.txt>
    // - <https://www.pkware.com/documents/APPNOTE/APPNOTE-2.0.txt>

    const fileHeaderMinSize = 30;
    const directoryRecordMinSize = 46;
    const directoryTerminatorMinSize = 22;

    // Before we can allocate the output buffer, we'll have to compute some
    // things, including what size buffer is required.
    let filesSize = 0;
    let directorySize = 0;
    let fileNames = this._listFileNames();
    for (let i = 0, n = fileNames.length; i < n; ++i) {
      let name = fileNames[i];
      let size = this._fileBuffers.get(name).byteLength;
      filesSize += fileHeaderMinSize + name.length + size;
      directorySize += directoryRecordMinSize + name.length;
    }

    let totalSize = filesSize + directorySize + directoryTerminatorMinSize;
    let output = new ArrayBuffer(totalSize);

    const filesStartOffset = 0;
    const directoryStartOffset = 0 + filesSize;

    let filesWriter = new ZodLib.Writer(output, filesStartOffset);
    let directoryWriter = new ZodLib.Writer(output, directoryStartOffset);

    const fileHeaderSignature = 0x04034B50;
    const directoryHeaderSignature = 0x02014B50;
    const directoryTerminatorSignature = 0x06054B50;
    const zipVersion = 20;
    const flags = 0;

    for (let i = 0, n = fileNames.length; i < n; ++i) {
      let name = fileNames[i];
      let buffer = this._fileBuffers.get(name);
      let crc32 = ZodLib.computeCRC32(buffer);

      let time = 0; // TODO

      let headerOffset = filesWriter.offset;

      filesWriter.put4Bytes(fileHeaderSignature);
      filesWriter.put2Bytes(zipVersion);
      filesWriter.put2Bytes(flags);
      filesWriter.put2Bytes(ZodLib.STORE_METHOD);

      filesWriter.put4Bytes(time);

      filesWriter.put4Bytes(crc32);
      filesWriter.put4Bytes(buffer.byteLength);
      filesWriter.put4Bytes(buffer.byteLength);
      filesWriter.put2Bytes(name.length);

      filesWriter.put2Bytes(0); // extra size

      filesWriter.putStringBytes(name);

      filesWriter.putBufferBytes(buffer);

      directoryWriter.put4Bytes(directoryHeaderSignature);
      directoryWriter.put2Bytes(zipVersion);
      directoryWriter.put2Bytes(zipVersion);
      directoryWriter.put2Bytes(flags);
      directoryWriter.put2Bytes(ZodLib.STORE_METHOD);

      directoryWriter.put4Bytes(time);

      directoryWriter.put4Bytes(crc32);
      directoryWriter.put4Bytes(buffer.byteLength);
      directoryWriter.put4Bytes(buffer.byteLength);
      directoryWriter.put2Bytes(name.length);

      directoryWriter.put2Bytes(0); // extra size
      directoryWriter.put2Bytes(0); // comment size
      directoryWriter.put2Bytes(0); // disk number
      directoryWriter.put2Bytes(0); // internal attributes
      directoryWriter.put4Bytes(0); // external attributes

      directoryWriter.put4Bytes(headerOffset);

      directoryWriter.putStringBytes(name);
    }

    // assert(directoryWriter.offset - directoryStartOffset == directorySize)

    directoryWriter.put4Bytes(directoryTerminatorSignature);
    directoryWriter.put2Bytes(0); // current disk
    directoryWriter.put2Bytes(0); // directory disk
    directoryWriter.put2Bytes(fileNames.length);
    directoryWriter.put2Bytes(fileNames.length);
    directoryWriter.put4Bytes(directorySize);
    directoryWriter.put4Bytes(directoryStartOffset);

    directoryWriter.put2Bytes(0); // comment size

    return output;
  }

  _listFileNames() {
    return Array.from(this._fileBuffers.keys());
  }

  // CRC-32 routines (derived from zlib / pako; see zlib notices)

  static computeCRC32(buffer, crc = 0, table = ZodLib.createCRC32Table()) {
    crc ^= -1;

    let bytes = new Uint8Array(buffer);
    for (let i = 0, n = bytes.byteLength; i < n; i++) {
      crc = (crc >>> 8) ^ table[(crc ^ bytes[i]) & 0xFF];
    }

    return (crc ^ (-1)) >>> 0;
  }

  static createCRC32Table() {
    var c, table = [];

    for (var n = 0; n < 256; n++) {
      c = n;
      for (var k = 0; k < 8; k++) {
        c = ((c & 1) ? (0xEDB88320 ^ (c >>> 1)) : (c >>> 1));
      }
      table[n] = c;
    }

    return table;
  }
}

ZodLib.Writer = class Writer {
  constructor(buffer, offset) {
    this._data = new DataView(buffer);
    this.offset = offset;
  }

  put2Bytes(value) {
    this._data.setUint16(this.offset, value, true);
    this.offset += 2;
  }

  put4Bytes(value) {
    this._data.setUint32(this.offset, value, true);
    this.offset += 4;
  }

  putBufferBytes(buffer) {
    let srcBytes = new Uint8Array(buffer);
    let destBytes = new Uint8Array(this._data.buffer);
    destBytes.set(srcBytes, this.offset);
    this.offset += buffer.byteLength;
  }

  putStringBytes(string) {
    Writer.syncStringToBuffer(string, this._data.buffer, this.offset);
    this.offset += string.length;
  }

  static syncStringToBuffer(text, buffer, offset) {
    let bytes = new Uint8Array(buffer, offset);
    for (let i = 0, n = text.length; i < n; ++i) {
      let unit = text.codePointAt(i);
      if (unit != unit & 0x7F) throw new Error("not ASCII"); // TODO
      bytes[i] = unit;
    }
  }
}

ZodLib.STORE_METHOD = 0;
