export
class ConsoleBuffer {
  constructor(scrollback = []) {
    this.lines = [ "" ];
    this.demarcation = 0;
    this.cursorX = 0;
    this.cursorY = 0;

    this.onupdate = null;
  }

  static isPrintable(character) {
    return " " <= character && character < "\x7F";
  }
}
