export
class LineWriter {
  constructor(buffer) {
    this._buffer = buffer;
  }

  render(text, cursor, prompt = "") {
    let buffer = this._buffer;
    if (buffer.lines.length == buffer.demarcation) {
      buffer.lines.push("");
    }
    buffer.lines.splice(buffer.demarcation, 1, prompt + text);
    buffer.cursorX = prompt.length + cursor;
    this._dispatch(buffer.onupdate, buffer, [ buffer.demarcation ]);

    this._cachedText = text;
    this._cachedCursor = cursor;
    this._cachedPrompt = prompt;
  }

  _dispatch(handler, ...args) {
    if (handler) {
      handler.apply(null, args);
    }
  }

  commit(cursorAdjustment = 0) {
    this._buffer.demarcation = this._buffer.lines.length;
    this._buffer.cursorY = this._buffer.demarcation + cursorAdjustment;
    return this._buffer.demarcation;
  }

  printLine(text) {
    // add new text to non-active region
    let { lines, demarcation, cursorY } = this._buffer;
    lines.splice(demarcation, Infinity, ...text.split("\n"));
    let newDemarcation = this.commit(cursorY - demarcation);

    // refresh scrollback's new text lines
    let dirty = [];
    for (let i = 0, n = newDemarcation - demarcation; i < n; ++i) {
      dirty.push(demarcation + i);
    }
    this._dispatch(this._buffer.onupdate, this._buffer, dirty);

    // refresh active region
    this.render(this._cachedText, this._cachedCursor, this._cachedPrompt);
  }
}
