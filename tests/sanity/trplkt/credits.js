export var credits =
` ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' '
The primary work is trplkt (0.9.8).
Available under MIT License, unless otherwise noted.

NB: The tools used to build the primary work are distributed, alongside the
source code for the primary work, in the project directory under license
compatible with this use.  Nonetheless, these build tools are not considered a
part of the primary work, but instead separate works which remain subject to
the terms of their respective licenses.

(The preceding notes are informational, and reproducing them is not itself
intended to be a prerequisite for redistribution, although the inclusion of
some of the content below may be required in order to redistribute the primary
work in whole or in part.)

Authorship and contributions
============================

* Andrey Tupitsin
* Colby Russell
* Jean-loup Gailly
* Mark Adler
* Vitaly Puzrin

Additional works involved
=========================

CRC-32 routines (derived from zlib / pako) available under zlib license

    Copyright (C) 1995-2013 Jean-loup Gailly and Mark Adler
    Copyright (C) 2014-2017 Vitaly Puzrin and Andrey Tupitsin

<https://zlib.net>
<https://github.com/nodeca/pako>

Licenses compatible for use in the primary work
===============================================

MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

* * *

The zlib license

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' `
