export
class Creditizor {
  static process(input, path = null) {
    if (Creditizor.isValid(input)) {
      return (
        "export var credits =" + "\n" +
        "` ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' '" +
        " ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' '" + "\n" +
        input + ((!input.endsWith("\n") && "\n") || "") +
        "' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' '" +
        " ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' `" + "\n"
      );
    }
    throw new Creditizor.InputFormatError(inputPath);
  }

  static isValid(text) {
    return text.indexOf("`") < 0;
  }
}

Creditizor.InputFormatError = class InputFormatError extends Error {
  constructor(path) {
    super("Invalid input");
    this.path = path;
  }
}
