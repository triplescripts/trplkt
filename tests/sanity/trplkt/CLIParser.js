export
class CLIParser {
  constructor() {
    this.params = null;
    this.command = null;
    this.error = null;
    this._defaultCommand = null;
    this._defaultOption = null;
    this._commands = new Map();
    this._globalConstraints = [];
    this._longOptions = new Map();
    this._shortOptions = new Map();
  }

  addCommand(name, callback) {
    this._commands.set(name, { name: name, callback: callback });
  }

  addDefaultCommand(name, heuristic = null) {
    let info = this._commands.get(name);
    if (!info) throw new Error("Bad command");
    this._defaultCommand = info;
    if (heuristic) {
      info.heuristic = heuristic;
    }
  }

  listCommands() {
    let result = [];
    for (let [ name ] of this._commands.entries()) {
      result.push(name);
    }
    return result;
  }

  addOption(terse, verbose, callback) {
    let name = this._getNameForLongOption(verbose);
    if (verbose) {
      if (!name) throw new Error("Bad format");
      this._longOptions.set(verbose, { name: name, callback: callback });
    }
    if (terse) {
      if (!this._isShort(terse)) throw new Error("Bad format");
      this._shortOptions.set(terse, { name: name, callback: callback });
    }
  }

  _getNameForLongOption(arg) {
    if (arg && this._isLong(arg)) {
      return arg.substring(2);
    }
    return null;
  }

  _isShort(arg) {
    return arg.length == 2 && this._isOption(arg);
  }

  _isOption(arg) {
    return arg.substring(0, 1) == "-";
  }

  _isLong(arg) {
    return arg.length >= 4 && arg.substring(0, 2) == "--";
  }

  constrain(name, value, message = null) {
    if (this.params.has(name)) {
      this.check(name, [ value ], message);
    } else if (value !== undefined) {
      this.params.set(name, value);
    }
  }

  check(name, values, message = null) {
    if (values.indexOf(this.params.get(name)) < 0) {
      if (!message) message = "Argument constraint violated";
      throw new CLIParser.ArgError(message);
    }
  }

  addParamListItem(listName, value) {
    let list = this.params.get(listName) || [];
    if (!list.length) this.params.set(listName, list);
    list.push(value);
  }

  addDefaultOption(option, name = null) {
    if (!this._isShort(option)) {
      throw new Error("Bad format");
    }
    this._defaultOption = option;
    if (name) {
      this.require(name);
      this.addOption(option, this._prefix(name), (params, value) => {
        this.constrain(name, undefined);
        params.set(name, value);
      });
    }
  }

  require(param, message = null) {
    this._globalConstraints.push(() => {
      if (!this.params.has(param)) {
        if (typeof(message) == "function") {
          message = message.call(null, this.params);
        } else if (!message) {
          message = "Parameter " + JSON.stringify(param) + " is required";
        }
        throw new CLIParser.ArgError(message);
      }
    });
  }

  handle(args) {
    this.params = new Map();
    try {
      let optionsPosition = 0;
      if (this._commands.size) {
        let info = this._commands.get(args[0]);
        if (info) {
          ++optionsPosition;
        } else {
          if (this._defaultCommand) {
            let { heuristic } = this._defaultCommand;
            if (this._isOption(args[0]) ||
                (heuristic && heuristic.call(null, args[0]))) {
              info = this._defaultCommand;
            }
          } else if (args.length < 1 || this._isOption(args[0])) {
            throw this._argExpected(null);
          } else {
            throw this._argUnrecognized(args[0]);
          }
        }
        this.command = info.name;
        info.callback(this.params);
      }
      for (let i = optionsPosition, n = args.length; i < n; ++i) {
        if (this._isOption(args[i])) {
          i += this._parseArguments(this.params, args, i);
        } else if (this._defaultOption != null) {
          this._parseArguments(this.params, [ this._defaultOption, args[i] ]);
        } else {
          throw this._argUnrecognized(args[i]);
        }
      }
      for (let i = 0, n = this._globalConstraints.length; i < n; ++i) {
        (this._globalConstraints[i])();
      }
      return this.params;
    } catch (ex) {
      if (ex instanceof CLIParser.ArgError) {
        this.error = ex;
        return this.params = null;
      }
      throw (ex);
    }
  }

  _parseArguments(params, args, index = 0) {
    if (this._isLong(args[index])) {
      return this._parseLongOption(params, args, index);
    }
    return this._parseShortOptions(params, args, index);
  }

  _parseLongOption(params, args, index) {
    let name, value, spec = args[index].substring(2);
    [ name ] = spec.split("=", 1);
    let skipCount = 0;
    let info = this._longOptions.get(this._prefix(name));
    if (!info) throw this._argUnrecognized(this._prefix(name));
    if (info.callback.length > 1) {
      if (name.length == spec.length) {
        value = args[index + 1];
        ++skipCount;
      } else {
        value = spec.substring(spec.indexOf("=") + 1);
      }
      if (!value) {
        throw this._argExpected(name);
      }
      info.callback(params, value);
    } else {
      if (name.length != spec.length) {
        throw this._argExpected(name, false);
      }
      info.callback(params);
      this._setBoolArg(params, info.name);
    }
    return skipCount;
  }

  _setBoolArg(params, name) {
    if (name && !params.has(name)) {
      params.set(name, true);
    }
  }

  _parseShortOptions(params, args, index) {
    let skipCount = 0;
    // assert(args[index].charAt(0) == "-");
    let optionSequence = args[index].substring(1);
    for (let i = 0, n = optionSequence.length; i < n;) {
      let codePoint = String.fromCodePoint(optionSequence.codePointAt(i));
      let info = this._shortOptions.get("-" + codePoint);
      if (!info) throw this._argUnrecognized("-" + codePoint);
      if (info.callback.length > 1) {
        let shortOptionMnemonicIsInTailPosition = (i + codePoint.length) == n;
        if (!(index + 1 < args.length) || this._isOption(args[index + 1]) ||
            !shortOptionMnemonicIsInTailPosition) {
          throw this._argExpected(codePoint);
        }
        info.callback(params, args[index + 1]);
        ++skipCount;
      } else {
        info.callback(params);
        if (info.name != null) {
          this._setBoolArg(params, info.name);
        }
      }
      i += codePoint.length;
    }
    return skipCount;
  }

  _argUnrecognized(prefixed) {
    return new CLIParser.ArgError(
      "Unrecognized option \"" + prefixed + "\""
    );
  }

  _argExpected(name, needsArg = true) {
    let message, prefixed = this._prefix(name);
    if (name === null) {
      message = "Expected command name as an argument";
    } else if (needsArg) {
      message = "Option \"" + prefixed + "\" expects an argument"
    } else {
      message = "Option \"" + prefixed + "\" expects no argument"
    }
    return new CLIParser.ArgError(message);
  }

  _prefix(name) {
    if (name) {
      if (this._isOption(name)) {
        throw new Error("Internal error; there is a bug in CLIParser");
      }
      if (name.length > 1) {
        return "--" + name;
      }
      return "-" + name;
    }
    return null;
  }
}

CLIParser.ArgError = class ArgError extends Error { };
