export
class CheckedInit {
  static create(system) {
    let result = null;
    try {
      result = new system;
    } catch (ex) {
      if (!(ex instanceof CheckedInit.ShuntingError)) {
        throw ex;
      }
    }
    return result;
  }
}

CheckedInit.ShuntingError = class ShuntingError extends Error { };
