import { SimpleScanner } from "./SimpleScanner.js";

export
class TextScanner extends SimpleScanner {
  getNextToken() {
    do {
      var result = this.getGenericToken();
    } while (result && result.type == TextScanner.WHITESPACE);
    if (result && result.type == SimpleScanner.UNKNOWN) {
      result.content = this._scanText();
      result.type = TextScanner.TEXT;
    }
    return result;
  }

  _scanText() {
    let result = "";
    while (this.tip && !this.isWhitespace(this.tip)) {
      result += this.readCodePoint();
    }
    return result;
  }
}

TextScanner.WHITESPACE = SimpleScanner.WHITESPACE;
TextScanner.TEXT = 1;
