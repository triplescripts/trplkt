import { ConsoleBuffer } from "./ConsoleBuffer.js";
import { DoubleTapClipboard } from "./DoubleTapClipboard.js";
import { LineReader } from "./LineReader.js";

export
class ConsoleWidget {
  constructor(container, buffer, input = null, width, height) {
    this._cursorState = true;
    this._rowBoxes = [];

    this.input = input || new LineReader(buffer);

    this._buffer = buffer;
    this._buffer.onupdate = this._onUpdate.bind(this);

    this._injectNodes(container);
    this._setDimensions(width, height);

    this._contentBox.addEventListener("keypress", this, false);
    this._contentBox.addEventListener("keydown", this, false);
    this._contentBox.addEventListener("keyup", this, false);

    this.clipboard = new DoubleTapClipboard(
      this._contentBox, this._clipboardBox
    );
    this.clipboard.onpaste = this._onPaste.bind(this);

    this.$intercept = this._specialInterceptHandler.bind(this);

    this._doc = container.ownerDocument;
    this._doc.defaultView.setInterval(this._nextCursorPhase.bind(this), 500);
  }

  _nextCursorPhase() {
    this._cursorState = !this._cursorState;
    this._render(this._buffer, [ this._buffer.cursorY ]);
  }

  _injectNodes(container) {
    let $$ = container.ownerDocument.createElement.bind(
      container.ownerDocument
    );

    let styleElement = container.appendChild($$("style"));
    ConsoleWidget.applySkinDefaults(styleElement.sheet);

    this._contentBox = $$("div");
    this._contentBox.tabIndex = "1";
    this._contentBox.className = "term-content";
    container.appendChild(this._contentBox);

    this._clipboardBox = container.appendChild($$("div"));
  }

  _setDimensions(width = Infinity, height = Infinity) {
    if (width < Infinity) {
      this._contentBox.style.width = width + "ch";
    } else {
      this._contentBox.style.width = "100%";
    }

    if (height < Infinity) {
      this._contentBox.style.height = (height * 1.2) + "em";
    } else {
      this._contentBox.style.height = "100%";
    }
  }

  static applySkinDefaults(styleSheet) {
    let $ = styleSheet.insertRule.bind(styleSheet);
    $(".term-content {" +
        "outline: 0;" +
        "display: block;" +
        "overflow-x: hidden;" +
        "white-space: nowrap;" +
        "font-family: fixed, monospace;" +
        "font-size: 15px;" +
        "font-variant-ligatures: none;" +
        "color: #f0f0f0;" +
        "background: #000000;" +
    "}");

    $(".term-content:focus .term-cursor {" +
        "color: #000000;" +
        "background: #ffffff;" +
    "}");
  }

  _onUpdate(buffer, dirtyCoords) {
    this._render(buffer, dirtyCoords);
    this._contentBox.scrollTop = this._contentBox.scrollHeight;
  }

  _render(vtty, dirtyCoords) {
    if (!dirtyCoords.length) return;

    // In case the active region has been transferred into the scrollback
    // region since we last rendered, we need to erase any cursor artifact.
    let cursorCell = this._contentBox.querySelector(".term-cursor");
    if (cursorCell) {
      cursorCell.classList.remove("term-cursor");
    }

    dirtyCoords = dirtyCoords.sort((a, b) => (a - b));
    let farthest = dirtyCoords[dirtyCoords.length - 1];
    while (this._rowBoxes.length <= farthest) {
      this._rowBoxes.push(this._createRowBox());
    }

    for (let i = 0, n = dirtyCoords.length; i < n; ++i) {
      this._renderLine(vtty, dirtyCoords[i]);
    }
  }

  _renderLine(vtty, y) {
    let line = vtty.lines[y];
    let lineCap = line.length - 1;
    let lineHasCursor = y == vtty.cursorY;
    if (lineHasCursor) {
      lineCap = Math.max(lineCap, vtty.cursorX);
    }

    let lineMarkup = "";
    for (let x = 0; x <= lineCap; ++x) {
      let cellMarkup = null;
      if (x < line.length) {
        cellMarkup = line[x];
      }
      switch (cellMarkup) {
        case " ":
          cellMarkup = "&nbsp;";
        break;
        case "&":
          cellMarkup = "&amp;";
        break;
        case "\x3E":
          cellMarkup = "&gt;";
        break;
        case "\x3C":
          cellMarkup = "&lt;";
        break;
        case "\"":
          cellMarkup = "&quot;";
        break;
        case "'":
          cellMarkup = "&#x27;";
        break;
        default:
          if (!ConsoleBuffer.isPrintable(cellMarkup)) {
            cellMarkup = "&nbsp;";
          }
        break;
      }

      if (lineHasCursor && x == vtty.cursorX && this._cursorState) {
        cellMarkup = this._buildMarkup(
          "span", "class=\"term-cursor\"", [ cellMarkup ]
        );
      }

      lineMarkup += cellMarkup;
    }

    this._rowBoxes[y].innerHTML = lineMarkup || "&zwnj;";
  }

  _buildMarkup(name, attributesText, content = []) {
    if (attributesText == null) attributesText = "";
    let result = "<" + name + " " + attributesText + ">";
    if (content != null) {
      result += content.join("");
      result += "<" + "/" + name + ">";
    }
    return result;
  }

  _onPaste(value) {
    value = value.replace(/\n|\r/g, " "); // XXX
    for (let i = 0, n = value.length; i < n; ++i) {
      this.input.process(value[i]);
    }
  }

  handleEvent(event) {
    switch (event.type) {
      case "keydown":
        return void(this._onKeyDown(event));
      case "keypress":
        return void(this._onKeyPress(event));
      case "keyup":
        return void(this._onKeyUp(event));
      default:
        return void(console.error("Unexpected event:", event.type));
    }
  }

  _onKeyPress(event) {
    if (event.key.length == 1 && !this._hasModifier(event)) {
      this._cancelEvent(event);
      this.input.process(event.key);
    }
  }

  _hasModifier(event) {
    return event.altKey || event.ctrlKey || event.metaKey;
  }

  _cancelEvent(event) {
    event.stopPropagation();
    event.preventDefault();
  }

  _onKeyDown(event) {
    if (this._hasModifier(event)) {
      return void(this._setUpIntercept(this._doc.defaultView));
    }
    let input, $ = String.fromCharCode;
    switch (event.keyCode) {
      case LineReader.Backspace:
        input = $(LineReader.Backspace);
      break;
      case LineReader.Enter:
        input = $(LineReader.Enter);
      break;
      case ConsoleWidget.LeftKey:
        input = LineReader.LeftSequence;
      break;
      case ConsoleWidget.RightKey:
        input = LineReader.RightSequence;
      break;
    }
    if (input) {
      this._cancelEvent(event);
      this.input.process(input);
    }
  }

  _setUpIntercept(view) {
    view.addEventListener("beforeunload", this.$intercept, false);
  }

  _specialInterceptHandler(event) {
    this._cancelEvent(event);
    this._clearIntercept(this._doc.defaultView);
  }

  _clearIntercept(view) {
    view.removeEventListener("beforeunload", this.$intercept, false);
  }

  _onKeyUp(event) {
    this._clearIntercept(this._doc.defaultView);
  }

  focus() {
    this._contentBox.focus();
  }

  _createRowBox() {
    return this._contentBox.appendChild(this._doc.createElement("div"));
  }
}

ConsoleWidget.LeftKey = 0x25;
ConsoleWidget.RightKey = 0x27;
