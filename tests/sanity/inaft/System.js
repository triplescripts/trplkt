export
class System {
  static create(system) {
    let result = null;
    try {
      result = new system;
    } catch (ex) {
      if (!(ex instanceof System.ShuntingError)) {
        throw ex;
      }
    }
    return result;
  }
}

System.ShuntingError = class ShuntingError extends Error { };
