import { Require } from "./Require.js";

export
class Loader {
  constructor() {
    this._modules = new Map();
  }

  static createModule(id) {
    return { id: id, exports: {} };
  }

  load(id, text, resolver) {
    let module = this.getCachedModule(id);
    if (module) return module;

    module = this.cacheModule(Loader.createModule(id));
    let require = new Require(module, this, resolver);
    let $ = (new Function("require", "module", "exports", text));

    try {
      $(require, module, module.exports);
    } catch (ex) {
      this._modules.delete(id);
      throw ex;
    }

    return module;
  }

  cacheModule(module) {
    if (this.getCachedModule(module.id)) {
      throw new Error("Module already cached: " + module.id);
    }
    this._modules.set(module.id, module);
    return module;
  }

  getCachedModule(id) {
    return this._modules.has(id) ?
      this._modules.get(id) :
      null;
  }
}
