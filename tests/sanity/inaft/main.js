import { BrowserHandler } from "./BrowserHandler.js";
import { NodeJSHandler } from "./NodeJSHandler.js";
import { System } from "./System.js";

void function main() {
  if (!system) var system = System.create(BrowserHandler);
  if (!system) var system = System.create(NodeJSHandler);
  if (!system) throw new System.ShuntingError("unknown environment");

  system.run();
} ();
