import { DependencySniffer } from "./DependencySniffer.js";
import { InaftPathResolver } from "./InaftPathResolver.js";
import { SimpleStringParser } from "./SimpleStringParser.js";
import { Sourcerer } from "./Sourcerer.js";

export
class TestRunner {
  constructor(systemHelper) {
    this.config = null;
    this.preloads = new Map();
    this.system = systemHelper;

    this._pending = [];

    this.onbadconfig = null;
    this.onfinish = null;
    this.onpathsknown = null;
    this.ontestexecute = null;
    this.ontestskip = null;
  }

  runTests() {
    return this.system.read("tests/config.json").then((text) =>
      void this._initConfigFromText(text).then((testPaths) =>
      void this._runNextTest(testPaths)
    ));
  }

  _initConfigFromText(text) {
    let testPaths, pending = [];
    if (text) {
      try {
        this.config = this._validateConfig(text);
      } catch (ex) {
        if (ex instanceof TestRunner.ConfigError) {
          this._dispatch(this.onbadconfig, ex);
          return Promise.resolve([]);
        }
        throw ex;
      }

      let { preload, tests } = this.config;
      if (typeof(preload) != "undefined") {
        pending.push(this._promisePreloadText(preload));
      }
      if (typeof(tests) != "undefined") {
        testPaths = tests.map((x) => this._getTestFilePath(x));
      }
    }

    if (!testPaths) {
      pending.push(this.system.list("tests/").then((kids) => {
        let $ok = (x) => (!x.startsWith(".") && x.endsWith("/"));
        testPaths = kids.filter($ok).map((x) => this._getTestFilePath(x));
      }));
    }

    return Promise.all(pending).then(() => {
      let stop = testPaths.indexOf(null);
      if (stop >= 0) testPaths.splice(stop, Infinity);
      this._dispatch(this.onpathsknown, testPaths);
      return Promise.resolve(testPaths);
    }).catch((err) => {
      if (err instanceof TestRunner.ConfigError) {
        this._dispatch(this.onbadconfig, err);
        return Promise.resolve([]);
      } else {
        Promise.reject(err); // Unknown/unexpected
      }
    });
  }

  _dispatch(handler, ...args) {
    if (handler) handler.apply(null, args);
  }

  _getTestFilePath(directoryName) {
    if (!directoryName) return null;
    if (!directoryName.endsWith("/")) {
      directoryName += "/";
    }
    return "tests/" + directoryName + "index.js";
  }

  _validateConfig(text) {
    // TODO Actually validate this (e.g., de-dupe entries)
    try {
      var config = JSON.parse(text);
    } catch (ex) {
      const { PARSE_ISSUE } = TestRunner.ConfigError;
      throw new TestRunner.ConfigError(PARSE_ISSUE, ex);
    }
    return config;
  }

  _promisePreloadText(paths) {
    let blockers = [];
    for (let i = 0; i < paths.length; ++i) {
      blockers.push(
        this.system.read(paths[i]).then((contents) => {
          if (contents != null) {
            this.preloads.set(paths[i], { path: paths[i], text: contents });
            return Promise.resolve();
          } else {
            const { PRELOAD_ISSUE } = TestRunner.ConfigError;
            let inner = new Error("No such file: " + paths[i]);
            return Promise.reject(
              new TestRunner.ConfigError(PRELOAD_ISSUE, inner)
            );
          }
        })
      );
    }
    return Promise.all(blockers);
  }

  _runNextTest(testPaths, index = 0) {
    let path = testPaths[index];
    if (path) {
      this._pending.push(new Promise((end) => {
        this.system.createContext().then((context) =>
          void this._promiseEvalPreloads(context).then(() =>
          void this._evalTestScript(context, path).then(() =>
          void (end(), this._runNextTest(testPaths, index + 1))
        )));
      }));
    } else {
      Promise.all(this._pending).then(() => {
        this._dispatch(this.onfinish);
      });
    }
  }

  _promiseEvalPreloads(context) {
    if (!this.preloads.size) return Promise.resolve();
    let specs = this.config.preload;
    let blockers = [];
    for (let i = 0; i < specs.length; ++i) {
      let { path, text } = this.preloads.get(specs[i]);
      blockers.push(
        this._rewriteImports(context, text, path).then((phonyText) =>
          this.system.loadScript(context, path, phonyText)
        )
      );
    }
    return Promise.all(blockers);
  }

  _evalTestScript(rootContext, path) {
    const timeBudget = 2500;
    var end; // can (will) manage contention for `onfinish` dispatch
    this._pending.push(new Promise((resolve) => void(end = resolve)));
    return this.system.read(path).then((scriptText) => {
      if (scriptText) {
        this.system.createContext(rootContext, true).then((subContext) =>
          void this._rewriteImports(subContext, scriptText).then((phonyText) =>
          void this.system.startCountdown(subContext, timeBudget).then(() =>
          void this.system.loadScript(subContext, path, phonyText).then(() =>
          void (end(), this._dispatch(this.ontestexecute, rootContext, path))
        ))))
      } else {
        end(), this._dispatch(this.ontestskip, path);
      }
    });
  }

  _rewriteImports(context, text, basePath = null) {
    let sniffer = new DependencySniffer(text);
    let imports = sniffer.getImports();

    let blockers = [];
    let nextContext = context.parent || context;
    for (let i = 0, n = imports.length; i < n; ++i) {
      let relativePath = SimpleStringParser.eval(imports[i].specifier);
      let dep = context.pathResolver.handle(relativePath, basePath);
      if (dep != InaftPathResolver.HELPER_PATH) {
        blockers.push(
          this.system.read(dep).then((text) => {
            if (text != null) return (
              this._rewriteImports(nextContext, text, dep).then((fake) => {
                return this.system.loadScript(nextContext, dep, fake);
              })
            );
          }
        ));
      }
    }

    return Promise.all(blockers).then(() => {
      return Promise.resolve(
        Sourcerer.rewrite(text, imports, sniffer.getExports())
      );
    });
  }
}

TestRunner.ConfigError = class ConfigError extends Error {
  constructor(message, inner) {
    super(message);
    this.innerException = inner;
  }
}

TestRunner.ConfigError.PARSE_ISSUE = "Couldn't parse tests' config.json";
TestRunner.ConfigError.PRELOAD_ISSUE = "Couldn't preload file";
