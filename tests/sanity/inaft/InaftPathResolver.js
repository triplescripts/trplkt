export
class InaftPathResolver {
  constructor(withMagic = false) {
    this.isMagic = withMagic;
  }

  static normalizePath(path, reference = "") {
    if (!InaftPathResolver.isRelative(path)) return path;
    let normalized = [];
    let parts = reference.split("/");
    parts.pop();
    parts = parts.concat(path.split("/"));
    for (let i = 0; i < parts.length; ++i) {
      switch (parts[i]) {
        case ".":
          // no-op
        break;
        case "..":
          let popped = normalized.pop();
          if (popped == undefined) throw new Error(
            "Path forays outside local root: " + JSON.stringify(path) + " " +
            "(relative to " + JSON.stringify(reference) + ")"
          );
        break;
        default:
          normalized.push(parts[i]);
        break;
      }
    }
    return normalized.join("/");
  }

  static isRelative(path) {
    return path.substr(0, 2) == "./" || path.substr(0, 3) == "../";
  }

  static getRealNameForMagicPath(id) {
    let magic = "$";
    if (!id.startsWith(magic)) return null;
    for (let i = magic.length; i < id.length;) {
      let codePoint = String.fromCodePoint(id.codePointAt(i));
      if (codePoint == "/") break;
      magic += codePoint;
      if (codePoint != "_") {
        if (codePoint < "A") return null;
        if (codePoint > "Z") return null;
      }
      i += codePoint.length;
    }

    switch (magic) {
      case InaftPathResolver.HELPER_PATH:
        return id;
      case InaftPathResolver.PROJECT_DIR:
        return id.substr((InaftPathResolver.PROJECT_DIR + "/").length);
    }

    return null;
  }

  handle(path, relativeTo) {
    if (this.isMagic) {
      return InaftPathResolver.getRealNameForMagicPath(path);
    }
    return InaftPathResolver.normalizePath(path, relativeTo);
  }
}

InaftPathResolver.WITH_MAGIC = true;

InaftPathResolver.HELPER_PATH = "$TEST_HELPER";
InaftPathResolver.PROJECT_DIR = "$PROJECT";
