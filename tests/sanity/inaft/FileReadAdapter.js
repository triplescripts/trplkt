export
class FileReadAdapter {
  constructor(fs, path, workDir) {
    this._nodeFS = fs;
    this._nodePath = path;
    this._workDir = workDir;
  }

  read(path, raw) {
    path = this._nodePath.join(this._workDir, path);
    if (!raw) {
      var options = { encoding: "utf8" };
    }

    return new Promise((resolve) => {
      void this._nodeFS.readFile(path, options, (err, contents) => {
        if (raw) {
          resolve(Uint8Array.from(contents))
        } else {
          resolve(contents)
        }
      });
    });
  }
}
