export
class TestMediator {
  static forgeID(requests) {
    do {
      var id = (Math.random() * 0xFDB97531) >>> 0;
    } while (requests.has(id));
    return id;
  }
}

TestMediator.EVAL_SCRIPT = "eval-script";
TestMediator.READ_FILE = "read-file";
TestMediator.REQUEST_RESULTS = "request-results";
TestMediator.RESOLVE_EVAL = "resolve-eval";
TestMediator.RESOLVE_READ = "resolve-read";
TestMediator.REVIEW_RESULTS = "review-results";
TestMediator.START_COUNTDOWN = "start-countdown";

TestMediator.WorkerMessage = class WorkerMessage {
  toString() {
    return JSON.stringify(this);
  }
}

TestMediator.EvalScriptWorkerMessage =
class extends TestMediator.WorkerMessage {
  constructor(fileName, contents, wantMagic, workOrders) {
    super();
    this.type = TestMediator.EVAL_SCRIPT;
    this.value = contents;
    this.name = fileName;
    this.magic = wantMagic;
    this.id = TestMediator.forgeID(workOrders);
  }
}

TestMediator.ReadFileWorkerMessage =
class extends TestMediator.WorkerMessage {
  constructor(path, raw, workOrders) {
    super();
    this.type = TestMediator.READ_FILE;
    this.value = path;
    this.raw = raw;
    this.id = TestMediator.forgeID(workOrders);
  }
}

TestMediator.RequestResultsWorkerMessage =
class extends TestMediator.WorkerMessage {
  constructor(workOrders) {
    super();
    this.type = TestMediator.REQUEST_RESULTS;
    this.id = TestMediator.forgeID(workOrders);
  }
}

TestMediator.ResolveEvalWorkerMessage =
class extends TestMediator.WorkerMessage {
  constructor(evalRequestID, error = null, stack = null) {
    super();
    this.type = TestMediator.RESOLVE_EVAL;
    this.error = error;
    this.stack = stack;
    this.ref = evalRequestID;
  }
}

TestMediator.ResolveReadWorkerMessage =
class extends TestMediator.WorkerMessage {
  constructor(contents, readRequestID) {
    super();
    this.type = TestMediator.RESOLVE_READ;
    this.value = contents;
    this.ref = readRequestID;
  }
}

TestMediator.ReviewResultsWorkerMessage =
class extends TestMediator.WorkerMessage {
  constructor(results, resultsRequestID) {
    super();
    this.type = TestMediator.REVIEW_RESULTS;
    this.value = results;
    this.ref = resultsRequestID;
  }
}

TestMediator.StartCountdownWorkerMessage =
class extends TestMediator.WorkerMessage {
  constructor(millis) {
    super();
    this.type = TestMediator.START_COUNTDOWN;
    this.value = millis;
  }
}
