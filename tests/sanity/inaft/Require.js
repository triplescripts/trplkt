export
class Require {
  constructor(module, loader, resolver) {
    this._module = module;
    this._loader = loader;
    this._resolver = resolver;
    return this.$require.bind(this);
  }

  $require(originalID) {
    if (this._module) {
      var relativeTo = this._module.id;
    }
    let module, id = this._resolver.handle(originalID, relativeTo);
    if (id) {
      module = this._loader.getCachedModule(id);
      if (!module) {
        module = this._loader.getCachedModule(id + ".js");
      }
    }
    if (!module) {
      let error = new Error(
        "Cannot resolve module " + JSON.stringify(originalID)
      );
      if (this._module) {
        error.message += " required by " + JSON.stringify(this._module.id)
      }
      throw error;
    }
    return module.exports;
  }
}
