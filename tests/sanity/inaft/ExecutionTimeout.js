export
class ExecutionTimeout {
  constructor() {
    this.$cleanup = null;
    this._promise = new Promise((resolve) => {
      this._resolve = resolve;
    });
    this.contention = 0;
  }

  start($init) {
    this.$cleanup = $init(this.burn.bind(this));
  }

  burn() {
    if (this.$cleanup) {
      this.$cleanup.call();
    }
    this._resolve();
  }

  then(...args) {
    return this._promise.then.apply(this._promise, args);
  }
}
