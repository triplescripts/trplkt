import { SimpleScanner } from "./SimpleScanner.js";
import { SimpleStringParser } from "./SimpleStringParser.js";

export
class DependencySniffer extends SimpleScanner {
  constructor(text, preserveWhitespace) {
    super(text);
    this._findings = null;
    this._preserveWhitespace = !!preserveWhitespace;
  }

  getImports() {
    if (!this._findings) this._findings = this._sniff();
    return this._findings.imports;
  }

  getExports() {
    if (!this._findings) this._findings = this._sniff();
    return this._findings.exports;
  }

  getToken(includeGaps = this._preserveWhitespace) {
    const { WHITESPACE, UNKNOWN } = SimpleScanner;
    do {
      var result = this.getGenericToken();
    } while (result && result.type == WHITESPACE && !includeGaps);
    if (result && result.type == UNKNOWN) {
      switch (this.tip) {
        case ";":
          result.type = DependencySniffer.SEMICOLON;
          result.content = this.readCodePoint();
        break;
        case ",":
          result.type = DependencySniffer.COMMA;
          result.content = this.readCodePoint();
        break;
        case "{":
          result.type = DependencySniffer.OPENED_CURLY;
          result.content = this.readCodePoint();
        break;
        case "}":
          result.type = DependencySniffer.CLOSED_CURLY;
          result.content = this.readCodePoint();
        break;
        case "*":
          result.type = DependencySniffer.ASTERISK;
          result.content = this.readCodePoint();
        break;
        case "'":
        case '"':
          result.content = this._scanString();
          result.type = DependencySniffer.STRING;
        break;
        default:
          if (this.tip == "$" || this.tip == "_" ||
              this._isLetter(this.tip)) {
            result.content = this._scanWord();
            switch (result.content) {
              case "export":
                result.type = DependencySniffer.EXPORT;
              break;
              case "from":
                result.type = DependencySniffer.FROM;
              break;
              case "import":
                result.type = DependencySniffer.IMPORT;
              break;
              default:
                result.type = DependencySniffer.WORD;
              break;
            }
          } else {
            // Still UNKNOWN, but we need to advance.
            result.content = this.readCodePoint();
          }
        break;
      }
    }
    return result;
  }

  _scanString() {
    const offset = this.position;
    // NB: This relies on the parser's implementation details.
    let parser = new SimpleStringParser({
      codePointAt: (index) => (!parser.literal) ?
        this.text.codePointAt(offset + index) :
        undefined
    });
    let { literal } = parser.run();
    this.position += literal.length;
    return literal;
  }

  _scanWholeLine() {
    while (this.tip != "\n" && this.tip != "") {
      this.readCodePoint();
    }
  }

  _scanWord() {
    let result = this.readCodePoint();
    while (this._isLetter(this.tip) || this._isDigit(this.tip) ||
           this.tip == "$" || this.tip == "_") {
      result += this.readCodePoint();
    }
    return result;
  }

  _isLetter(x) {
    return ("A" <= x && x <= "Z") || ("a" <= x && x <= "z");
  }

  _isDigit(x) {
    return ("0" <= x && x <= "9");
  }

  _sniff() {
    if (this.position != 0) throw new Error("Re-entrant parse disallowed");
    let result = { imports: [], exports: [] };
    let token = this.getToken();
    if (token) {
      do {
        if (token.type == DependencySniffer.IMPORT) {
          result.imports.push(this._parseImportDeclaration(token));
        } else if (token.type == DependencySniffer.EXPORT) {
          result.exports.push(this._parseExportDeclaration(token));
        } else {
          this._scanWholeLine();
        }
        token = this.getToken();
      } while (token);
    }
    return result;
  }

  _parseImportDeclaration(token) {
    let start = token.position;
    let specifier, list = [];
    token = this._expectToken(this.getToken());
    if (token.type != DependencySniffer.OPENED_CURLY) {
      throw new Error("Only explicitly named imports supported");
    }
    token = this._expectToken(this.getToken());
    if (token.type == DependencySniffer.ASTERISK) {
      throw new Error("Wildcard imports not supported");
    }
    list.push(this._expectToken(token, DependencySniffer.WORD).content);
    token = this._expectToken(this.getToken());
    while (token.type != DependencySniffer.CLOSED_CURLY) {
      if (token.type == DependencySniffer.WORD && token.content == "as") {
        throw new Error("Namespace rebinding for imports not supported");
      }
      this._expectToken(token, DependencySniffer.COMMA);
      let word = this._expectToken(this.getToken(), DependencySniffer.WORD);
      list.push(word.content);
      token = this._expectToken(this.getToken());
    }
    this._expectToken(token, DependencySniffer.CLOSED_CURLY);
    token = this._expectToken(this.getToken(), DependencySniffer.FROM);
    token = this._expectToken(this.getToken(), DependencySniffer.STRING);
    specifier = token.content;
    if (this.tip && this.tip != "\n") {
      token = this._expectToken(this.getToken(), DependencySniffer.SEMICOLON);
    }
    let end = token.position + token.content.length;
    return { list: list, specifier: specifier, span: [ start, end ] };
  }

  _parseExportDeclaration(token) {
    let start = token.position;
    let mark = this.position;
    token = this._expectToken(this.getToken(), DependencySniffer.WORD);
    let kind = token.content;
    if (kind != "class" && kind != "function" && kind != "const" &&
        kind != "var" && kind != "let") {
      throw new Error("Unsupported export: " + JSON.stringify(kind));
    }
    token = this._expectToken(this.getToken(), DependencySniffer.WORD);
    let name = token.content;
    let end = token.position + token.content.length;
    return { kind: kind, name: name, span: [ start, end ], mark: mark };
  }

  _expectToken(token, type) {
    if (token == null) throw new Error("Unexpected EOF");
    if (typeof(type) != "undefined" && token.type != type) {
      throw new Error("Unexpected token: " + JSON.stringify(token.content));
    }
    return token;
  }
}

DependencySniffer.WORD = 1;
DependencySniffer.SEMICOLON= 2;
DependencySniffer.COMMA = 3;
DependencySniffer.OPENED_CURLY = 4;
DependencySniffer.CLOSED_CURLY = 5;
DependencySniffer.ASTERISK = 6;
DependencySniffer.STRING = 9;
DependencySniffer.IMPORT = 11;
DependencySniffer.FROM = 12;
DependencySniffer.EXPORT = 21;
