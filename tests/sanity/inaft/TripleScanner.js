import { SimpleScanner } from "./SimpleScanner.js";

export
class TripleScanner extends SimpleScanner {
  getNextToken() {
    let result = this.getGenericToken();
    if (result && result.type == SimpleScanner.UNKNOWN) {
      result.content = this._scanText();
      if (result.content == "/// \x3Cscript\x3E" ||
          result.content == "// \x3Cscript\x3E") {
        result.type = TripleScanner.DELIMITER_OPEN;
      } else if (result.content == "/// \x3C/script\x3E" ||
                 result.content == "// \x3C/script\x3E") {
        result.type = TripleScanner.DELIMITER_CLOSE;
      } else {
        result.type = TripleScanner.TEXT;
      }
    }
    return result;
  }

  _scanText() {
    let result = "";
    while (this.tip && !this.isWhitespace(this.tip)) {
      result += this.readCodePoint();
    }
    if (result.startsWith("//")) {
      while (this.tip && this.tip != "\n") {
        result += this.readCodePoint();
      }
    }
    return result;
  }
}

TripleScanner.WHITESPACE = SimpleScanner.WHITESPACE;
TripleScanner.TEXT = 1;
TripleScanner.DELIMITER_OPEN = 2;
TripleScanner.DELIMITER_CLOSE = 3;
