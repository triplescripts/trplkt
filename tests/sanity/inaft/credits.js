export var credits =
` ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' '
The primary work is inaft (0.9.8-not_really).
Available under MIT License, unless otherwise noted.

NB: Any auxiliary development tools, including but not limited to those
distributed alongside the project source code and used to build the primary
work, are separate works used under license compatible with this use and are
not considered a part of the primary work.

The primary work also incorporates elements from other works, which are
indentified below.

(The preceding notes are informational, and reproducing them is not itself
intended to be a prerequisite for redistribution, although the inclusion of
some of the content below may be required in order to redistribute the primary
work in whole or in part.)

Authorship and contributions
============================

* Colby Russell
* Ives van Hoorne

Works involved
==============

codesandbox.io test viewer visual design

(Inspired Inaft's in-browser UI.  Used with permission.)

    Copyright (C) 2018  Ives van Hoorne

Licenses compatible for use in the primary work
===============================================

MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' `
