export
class FixedWidthResultsFormatter {
  constructor() {
    this._columns = [];
  }

  addColumn(header, options = {}) {
    let width = header.length;
    if (options.entries) {
      for (let i = 0; i < options.entries.length; ++i) {
        let current = options.entries[i];
        if (current.length > width) {
          width = current.length;
        }
      }
    }
    this._columns.push({
      header: header,
      width: width,
      align: options.align || "left",
    });
  }

  formatHeader() {
    let values = [];
    for (let i = 0; i < this._columns.length; ++i) {
      values.push(this._columns[i].header);
    }
    return this.formatRowText(values);
  }

  formatRowText(cellValues) {
    let result = "";
    for (let i = 0; i < cellValues.length; ++i) {
      if (i) result += "  ";
      let value = String(cellValues[i]);
      let columnInfo = this._columns[i];
      if (columnInfo.align == "center") {
        let lefted = this._pad(value, columnInfo.width, true);
        let halfPad = ((lefted.length - value.length) / 2) | 0;
        result += lefted.substr(halfPad) + lefted.substr(0, halfPad);
      } else {
        result += this._pad(
          value, columnInfo.width, columnInfo.align == "right"
        );
      }
    }
    return result;
  }

  _pad(text, width, left) {
    let padding = "";
    while (width --> text.length) padding += " ";
    if (left) {
      return padding + text;
    }
    return text + padding;
  }
}
