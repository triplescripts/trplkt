import { BrowserWorkerHandler } from "./BrowserWorkerHandler.js";
import { BrowserQuirksHack } from "./BrowserQuirksHack.js";
import { BrowserUI } from "./BrowserUI.js";
import { ExecutionContext } from "./ExecutionContext.js";
import { FileCache } from "./FileCache.js";
import { FileUtils } from "./FileUtils.js";
import { Sourcerer } from "./Sourcerer.js";
import { System } from "./System.js";
import { TestMediator } from "./TestMediator.js";
import { TestRunner } from "./TestRunner.js";
import { WebkitFileIngester } from "./WebkitFileIngester.js";
import { credits } from "./credits.js";

export
function BrowserHandler() {
  if (typeof(window) == "undefined") {
    throw new System.ShuntingError("not a browser environment");
  }

  this._quirksHack = new BrowserQuirksHack(window);

  this._location = window.location;
  this._domRoot = window.document;
  this._workOrders = new Map();

  this._ui = null;
  this._uiMediator = null;

  this._directoryInput = null;
  this._fileSource = null;

  this._contextToWorkerMap = new WeakMap();

  let scriptSourceCode = this._blankPage();
  this._sourcerer = new Sourcerer(scriptSourceCode);
  this._workerURI = BrowserWorkerHandler.getWorkerURI(this._sourcerer);
}

void function($proto) {
  $proto = BrowserHandler.prototype;

  $proto._blankPage = function() {
    try {
      return this._domRoot.body.innerHTML;
    } finally {
      this._domRoot.body.textContent = "";
    }
  }

  $proto.run = function() {
    this._domRoot.defaultView.onload = () => {
      if (this._quirksHack.wrap()) return;

      this._ui = new BrowserUI(this, this._domRoot.body);

      this._domRoot.defaultView.addEventListener("message", this);

      this._directoryInput = this._domRoot.createElement("input");
      this._directoryInput.setAttribute("type", "file");
      this._directoryInput.setAttribute("webkitdirectory", "true");
      this._domRoot.head.appendChild(this._directoryInput);

      this._directoryInput.onchange = this._onDirectoryChange.bind(this);
    }
  }

  // NB: internal use only (for UI; not part of the abstract system interface)
  $proto.getFiles = function() {
    this._directoryInput.click();
  }

  $proto._onDirectoryChange = function(event) {
    this._ui.acknowledgeTests(null);
    this._fileSource = WebkitFileIngester.cache(event.target.files);
    let harness = new TestRunner(this);
    harness.onbadconfig = this._ui.showConfigProblem.bind(this._ui);
    harness.onpathsknown = this._ui.acknowledgeTests.bind(this._ui);
    harness.ontestskip = this._ui.markTestAsSkipped.bind(this._ui);
    harness.ontestexecute = this._getResults.bind(this);
    harness.runTests();
  }

  $proto.createContext = function(parent, wantMagic = false) {
    let context = new ExecutionContext(parent, wantMagic);
    if (!parent) {
      let worker = new Worker(this._workerURI);
      worker.addEventListener("message", this);
      this._contextToWorkerMap.set(context, worker);
    }
    return Promise.resolve(context);
  }

  $proto.startCountdown = function(context, millis) {
    let message = new TestMediator.StartCountdownWorkerMessage(millis);
    let rootContext = ExecutionContext.getRoot(context);
    let worker = this._contextToWorkerMap.get(rootContext);
    worker.postMessage(message.toString());
    return Promise.resolve(); // XXX fire-and-forget is okay here, right?
  }

  $proto.loadScript = function(context, fileName, contents) {
    return new Promise((resolve) => {
      let message = new TestMediator.EvalScriptWorkerMessage(
        fileName, contents, context.isMagic, this._workOrders
      );
      this._workOrders.set(message.id, {
        resolve: resolve,
        context: context,
        path: fileName
      });
      let rootContext = ExecutionContext.getRoot(context);
      let worker = this._contextToWorkerMap.get(rootContext);
      worker.postMessage(message.toString());
    });
  }

  $proto.handleEvent = function(event) {
    switch (event.type) {
      case "message": return void(this._onMessage(event));
      default: throw new Error("Unexpected event: " + event.type);
    }
  }

  $proto._onMessage = function(event) {
    try {
      var payload = JSON.parse(event.data);
      if (!("type" in payload)) return;
    } catch (ex) { }

    switch (payload.type) {
      case TestMediator.RESOLVE_EVAL:
        return void(this._onResolveEval(payload));
      case TestMediator.REVIEW_RESULTS:
        return void(this._onResultsReady(payload));
      case TestMediator.READ_FILE:
        return void(this._onFileReadRequest(payload, event.target));
    }
  }

  $proto._pullOrder = function(id) {
    let result = this._workOrders.get(id);
    this._workOrders.delete(id);
    return result;
  }

  $proto._onResolveEval = function(message) {
    let order = this._pullOrder(message.ref);
    if (message.error != null) {
      let { context, path } = order;
      let { error, stack } = message;
      let exception = new BrowserHandler.Exception(path, error, stack);
      let rootContext = ExecutionContext.getRoot(context);
      rootContext.recordException(path, exception);
    }
    order.resolve();
  }

  $proto._getResults = function(context, path) {
    const { RequestResultsWorkerMessage } = TestMediator;
    let message = new RequestResultsWorkerMessage(this._workOrders);
    let rootContext = ExecutionContext.getRoot(context);
    this._workOrders.set(message.id, { path: path, context: rootContext });
    let worker = this._contextToWorkerMap.get(rootContext);
    worker.postMessage(message.toString());
  }

  $proto._onResultsReady = function(message) {
    let order = this._pullOrder(message.ref);
    message.value.metafailures = this._getMetafailures(order.context);
    this._ui.showResults(order.path, message.value);
  }

  $proto._getMetafailures = function(context) {
    let lists = Array.from(context.exceptions.values());
    return lists.reduce((all, some) => all.concat(some), []);
  }

  $proto._onFileReadRequest = function(payload, worker) {
    const { ResolveReadWorkerMessage } = TestMediator;
    this.read(payload.value, payload.raw).then((contents) => {
      if (payload.raw) {
        contents = Array.from(new Uint8Array(contents));
      }
      let response = new ResolveReadWorkerMessage(contents, payload.id);
      worker.postMessage(String(response));
    });
  }

  $proto.read = function(path, raw) {
    let file = this._fileSource.recall(path);
    return WebkitFileIngester.readBlob(file, raw);
  }

  $proto.list = function(path) {
    let kids = FileUtils.list(this._fileSource, path);
    return Promise.resolve(kids);
  }
} ()

BrowserHandler.Exception = class Exception {
  constructor(path, message, stack) {
    // NB: Instances MUST remain stringify-able.
    this.path = path;
    this.message = message;
    this.stack = stack;
  }
}
