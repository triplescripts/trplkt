import { ExecutionContext } from "./ExecutionContext.js";
import { InaftPathResolver } from "./InaftPathResolver.js";
import { InaftTestHelper } from "./InaftTestHelper.js";
import { Loader } from "./Loader.js";
import { Require } from "./Require.js";
import { Sourcerer } from "./Sourcerer.js";

export
class BrowserWorkerHandler {
  constructor(worker) {
    this._worker = worker;
    this._workOrders = new Map();
    this._rootContext = ExecutionContext.createRoot(this);
  }

  static getWorkerURI(sourcerer) {
    return Sourcerer.makeDataURI(
      Sourcerer.makeIIFE(
        sourcerer.extractClassDefinition(ExecutionContext) + "\n" +
        sourcerer.extractClassDefinition(ExecutionTimeout) + "\n" +
        sourcerer.extractClassDefinition(InaftPathResolver) + "\n" +
        sourcerer.extractClassDefinition(InaftTestHelper) + "\n" +
        sourcerer.extractClassDefinition(Loader) + "\n" +
        sourcerer.extractClassDefinition(Require) + "\n" +
        sourcerer.extractClassDefinition(TestMediator) + "\n" +
        sourcerer.extractClassDefinition(BrowserWorkerHandler) + "\n" +
        "BrowserWorkerHandler.install(worker);",
        "worker", "this"
      )
    );
  }

  static install(worker) {
    let handler = new BrowserWorkerHandler(worker);
    worker.addEventListener("message", handler);
  }

  read(path, raw) {
    return new Promise((resolve) => {
      const { ReadFileWorkerMessage } = TestMediator;
      let message = new ReadFileWorkerMessage(path, raw, this._workOrders);
      this._workOrders.set(message.id, { resolve: resolve, raw: raw });
      this._worker.postMessage(String(message));
    });
  }

  handleEvent(event) {
    switch (event.type) {
      case "message": return void(this._onMessage(event));
      default: throw new Error("Unexpected event: " + event.type);
    }
  }

  _onMessage(event) {
    try {
      var payload = JSON.parse(event.data);
      if (!("type" in payload)) return;
    } catch (ex) { }

    switch (payload.type) {
      case TestMediator.EVAL_SCRIPT:
        return void(this._onEvalScriptRequest(payload, event.target));
      case TestMediator.REQUEST_RESULTS:
        return void(this._onTestResultsRequested(payload, event.target));
      case TestMediator.RESOLVE_READ:
        return void(this._onResolveRead(payload));
      case TestMediator.START_COUNTDOWN:
        return void(this._onStartCountdown(payload));
    }
  }

  _onEvalScriptRequest(message, target) {
    const { ResolveEvalWorkerMessage } = TestMediator;
    const { name, value, magic } = message;
    let context = new ExecutionContext(this._rootContext, magic);
    let exception = ExecutionContext.evaluate(context, name, value);
    if (exception) {
      var error = exception.name + ": " + exception.message;
      var stack = exception.stack;
    }
    let response = new ResolveEvalWorkerMessage(message.id, error, stack);
    target.postMessage(response.toString());
  }

  _onTestResultsRequested(message, target) {
    const { ReviewResultsWorkerMessage } = TestMediator;
    ExecutionContext.preemptTimerIfDone(this._rootContext);
    return this._rootContext.getTestResults().then((results) => {
      let response = new ReviewResultsWorkerMessage(results, message.id);
      target.postMessage(response.toString());
    });
  }

  _onResolveRead(message) {
    let order = this._workOrders.get(message.ref);
    if (!order) throw new Error("Unexpected response: " + message);
    this._workOrders.delete(message.ref);
    if (order.raw) {
      order.resolve(Uint8Array.from(message.value));
    } else {
      order.resolve(message.value);
    }
  }

  _onStartCountdown(message) {
    ExecutionContext.setUpTimer(this._rootContext, ($finish) => {
      let ref = this._worker.setTimeout($finish, message.value);
      return () => void(this._worker.clearTimeout(ref));
    });
  }
}
