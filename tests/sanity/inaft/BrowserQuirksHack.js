export
class BrowserQuirksHack {
  constructor(window) {
    this._document = window.document;
    this._location = window.location;

    this._frame = null;

    this._isOuter = window.parent == window;
  }

  get inner() {
    if (!this._isOuter) return null;
    return this._frame.contentWindow;
  }

  static buildMarkup(name, content = []) {
    let result = "<" + name + ">";
    if (content != null) {
      result += content.join("");
      result += "<" + "/" + name + ">";
    }
    return result;
  }

  wrap() {
    if (this._isOuter) {
      if (this._location.search != "") {
        throw new Error("Unexpected query string in URI");
      }
      let { body } = this._document;
      body.style.margin = "0";
      body.style.padding = "0";
      body.innerHTML = BrowserQuirksHack.buildMarkup("iframe");
      this._frame = body.firstChild;
      this._frame.style.width = "100vw";
      this._frame.style.height = "100vh";
      this._frame.style.border = "0";
      this._frame.style.margin = "0";
      this._frame.src = this._location + "?";
    } else {
      let $$ = BrowserQuirksHack.buildMarkup;
      let substitute =
        $$("!doctype html", null) + "\n" +
        $$("html", [
          $$("head") + $$("body")
        ]);
      this._document.write(substitute);
    }
    return this.inner;
  }
}
