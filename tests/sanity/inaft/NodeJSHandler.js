import { ExecutionContext } from "./ExecutionContext.js";
import { FileReadAdapter } from "./FileReadAdapter.js";
import { Sourcerer } from "./Sourcerer.js";
import { TestRunner } from "./TestRunner.js";
import { Loader } from "./Loader.js";
import { InaftPathResolver } from "./InaftPathResolver.js";
import { InaftTestHelper } from "./InaftTestHelper.js";
import { FixedWidthResultsFormatter } from "./FixedWidthResultsFormatter.js";
import { System } from "./System.js";
import { credits } from "./credits.js";

export
class NodeJSHandler {
  constructor() {
    if (typeof(process) == "undefined") {
      throw new System.ShuntingError("not a NodeJS environment");
    }

    if (process.argv.indexOf("--version") >= 0) {
      console.log(credits);
      process.exit(0);
    }

    this._nodeFS = process.mainModule.require("fs");
    this._nodePath = process.mainModule.require("path");
    this._nodeVM = process.mainModule.require("vm");
    this._process = process;

    this._fileName = process.mainModule.filename;

    let workDir = ".";
    if (process.argv.length > 2) {
      workDir = process.argv[2];
    }
    this._workDir = workDir;

    this._table = null;

    this._files = new FileReadAdapter(this._nodeFS, this._nodePath, workDir);
    this._sourcerer = new Sourcerer(
      this._nodeFS.readFileSync(this._fileName, { encoding: "utf8" })
    );
  }

  run() {
    let harness = new TestRunner(this);
    harness.ontestexecute = this._showResults.bind(this);
    harness.onpathsknown = (paths) => {
      this._table = new FixedWidthResultsFormatter();
      this._table.addColumn("status", { align: "center" });
      this._table.addColumn("test set", { entries: paths });
      this._table.addColumn("counts", { align: "right" });
      console.log(this._table.formatHeader());
    };
    harness.onbadconfig = (error) => {
      console.log(error.message + "\n\n" + error.innerException);
    };
    harness.runTests();
  }

  read(path, raw) {
    return this._files.read(path, raw);
  }

  list(path) {
    if (!path.startsWith("/")) {
      path = this._nodePath.join(this._workDir, path);
    }
    return new Promise((resolve) => {
      this._nodeFS.readdir(path, (err, fileNames) => {
        if (err) return void(resolve(null));
        resolve(fileNames.map((name) => {
          let stats = this._nodeFS.statSync(path + name);
          if (stats.isDirectory()) return name + "/";
          return name;
        }));
      });
    });
  }

  createContext(parent, wantMagic = false) {
    if (parent) {
      var context = new ExecutionContext(parent, wantMagic);
    } else {
      let sandbox = this._nodeVM.createContext({ console: console });
      let iife = Sourcerer.makeIIFE(
        this._sourcerer.extractClassDefinition(Loader) + "\n" +
        this._sourcerer.extractClassDefinition(Require) + "\n" +
        this._sourcerer.extractClassDefinition(InaftPathResolver) + "\n" +
        "return new Loader();"
      );

      let loader = this._nodeVM.runInContext(iife, sandbox);
      var context = ExecutionContext.createRoot(this, wantMagic, loader);
    }
    return Promise.resolve(context);
  }

  startCountdown(context, millis) {
    return ExecutionContext.setUpTimer(context, ($finish) => {
      let ref = setTimeout($finish, millis);
      return () => void(clearTimeout(ref));
    });
  }

  loadScript(context, fileName, fileText) {
    ExecutionContext.evaluate(context, fileName, fileText);
    return Promise.resolve();
  }

  _showResults(context, path) {
    ExecutionContext.preemptTimerIfDone(context);
    context.getTestResults().then((results) => {
      this._printResults(path, results, context.exceptions);
    });
  }

  _printResults(path, results, metafailures) {
    const details = results.details;
    const failed = details.filter((x) => (!!x.failure));
    let status, count, countExtra = "";
    if (results.passed && !metafailures.size) {
      status = "pass";
      if (details.length) {
        count = details.length - failed.length;
      } else {
        count = "--";
      }
    } else {
      status = "fail";
      if (!metafailures.size) {
        count = String(details.length - failed.length);
        countExtra = " (of " + details.length + ")";
      } else {
        count = "!!!";
      }
    }
    console.log(
      this._table.formatRowText([ status, path, count ]) + countExtra
    );
  }
}
