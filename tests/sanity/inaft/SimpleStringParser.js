import { SimpleScanner } from "./SimpleScanner.js";

export
class SimpleStringParser {
  constructor(input) {
    this._scanner = new SimpleScanner(input);
    this.literal = null;
    this.value = null;
  }

  static eval(input) {
    let parser = new SimpleStringParser(input);
    parser.run();
    return parser.value;
  }

  run() {
    let quote = this._scanner.readCodePoint();
    let isEscaped = false;
    let result = quote;
    let jsonSafe = "";
    while (!(this._scanner.tip == quote && !isEscaped)) {
      if (this._scanner.tip == "") throw new Error("Unexpected EOF");
      if (quote == "'") {
        if (this._scanner.tip == "\"" && !isEscaped) {
          jsonSafe += "\\";
        } else if (this._scanner.tip == "'" && isEscaped) {
          jsonSafe = jsonSafe.substring(0, jsonSafe.length - 1);
        }
      }
      if (!isEscaped) {
        if (this._scanner.tip == "\u2028" || this._scanner.tip == "\u2029") {
          throw new Error("Unterminated string literal");
        }
        if (this._scanner.tip == "\\") isEscaped = true;
      } else {
        isEscaped = false;
      }
      jsonSafe += this._scanner.tip;
      result += this._scanner.readCodePoint();
    }
    result += this._scanner.readCodePoint();
    this.value = JSON.parse("\"" + jsonSafe + "\"");
    this.literal = result;
    if (this._scanner.tip) throw new Error(
      "Unexpected token " + this._scanner.tip + " after string literal"
    );
    return this;
  }
}
