import { test, read } from "$TEST_HELPER"

import { TripleKit } from "$PROJECT/src/TripleKit.src"

// We're going to do some basic, high-level sanity checking the compilation
// and decompilation process works correctly and hasn't broken anything for a
// few obvious use cases.
//
// - A simple "Hello, world." triple script
// - lines.app.htm, from <https://triplescripts.org/example/>
// - Inaft's harness.app.htm
// - A copy of trplkt itself
//
// Specifically, we're going to test `TripleKit` methods `build` and
// `decompile` here.  For that, we need to mimic the system interface that
// `TripleKit` relies on.  Each needs be able to successfully "read" and
// "write" some "files".

function MockSystem(files) {
  this.pathWritten = null;
  this.dataWritten = null;

  const $res = (x) => (Promise.resolve(x));
  const $err = (x) => (Promise.reject(x));

  this.read = (path) => {
    let result = files.get(path);
    if (result) {
      return $res(files.get(path));
    }
    return $err(Error("invalid read: " + path));
  }

  this.write = (path, contents) => {
    if (this.pathWritten != null) {
      return $err(
        Error("multiple writes:\n - " + this.pathWritten + " + " + path)
      );
    }
    this.dataWritten = contents;
    this.pathWritten = path;
    return $res(void(0));
  };
}

test(() => {
  // First is first, a simple "hello, world" triple script.
  //
  // Wewe need the text of four modules in normal form:
  //
  // - Hello.js -- the heart of the app, just prints "Hello, world."
  // - InBrowserSystem.js -- implements `printLine` for the browser
  // - ConsoleSystem.js -- implements `printLine` for e.g. NodeJS
  // - main.js -- shunting block that handles startup and initialization

  let files = new Map();
  files.set(
    "Hello.js",
           `export class Hello {` +
    '\n' + `  static greet(system) {` +
    '\n' + `    system.printLine("Hello, world.");` +
    '\n' + `  }` +
    '\n' + `}` +
    '\n'
  );
  files.set(
    "InBrowserSystem.js",
           `export` +
    '\n' + `class InBrowserSystem {` +
    '\n' + `  static create() {` +
    '\n' + `    if (typeof(window) != "undefined") {` +
    '\n' + `      return new InBrowserSystem(window);` +
    '\n' + `    }` +
    '\n' + `    return null;` +
    '\n' + `  }` +
    '\n' +
    '\n' + `  run($App) {` +
    '\n' + `    window.onload = () => {` +
    '\n' + `      this._page = window.document.body;` +
    '\n' + `      this._page.innerHTML = "";` +
    '\n' +
    '\n' + `      $App.greet(this);` +
    '\n' + `    }` +
    '\n' + `  }` +
    '\n' +
    '\n' + `  printLine(message) {` +
    '\n' + `    this._page.textContent += message;` +
    '\n' + `  }` +
    '\n' + `}` +
    '\n'
  );
  files.set(
    "ConsoleSystem.js",
           `export` +
    '\n' + `class ConsoleSystem {` +
    '\n' + `  static create() {` +
    '\n' + `    if (typeof(console) != "undefined") {` +
    '\n' + `      return new ConsoleSystem;` +
    '\n' + `    }` +
    '\n' + `    return null;` +
    '\n' + `  }` +
    '\n' +
    '\n' + `  run($App) {` +
    '\n' + `    $App.greet(this);` +
    '\n' + `  }` +
    '\n' +
    '\n' + `  printLine(message) {` +
    '\n' + `    console.log(message);` +
    '\n' + `  }` +
    '\n' + `}` +
    '\n'
  );
  files.set(
    "main.src",
           `import { Hello } from "./Hello.js";` +
    '\n' + `import { InBrowserSystem } from "./InBrowserSystem.js";` +
    '\n' + `import { ConsoleSystem } from "./ConsoleSystem.js";` +
    '\n' +
    '\n' + `void function main() {` +
    '\n' + `  if (!system) var system = InBrowserSystem.create();` +
    '\n' + `  if (!system) var system = ConsoleSystem.create();` +
    '\n' + `  if (!system) throw new Error("I literally can't even");` +
    '\n' +
    '\n' + `  system.run(Hello);` +
    '\n' + `} ()` +
    '\n'
  );

  let sys = new MockSystem(files);
  let kit = new TripleKit(sys);

  // Now do the equivalent of the command
  //
  //     trplkit build -f Hello.js -m main.src
  //
  // ... and check that the result matches our reference output, already
  // pre-compiled and saved as plain text nearby.
  const referenceFile = "$PROJECT/tests/sanity/hello/hello.app.htm";

  // NB: both `build` and this `read` operation are async (Promise-based)
  return read(referenceFile).then((refText) =>
    kit.build([ "Hello.js" ], "main.src").then(() => {
      test.is(sys.pathWritten, "out.app.htm");
      test.is(sys.dataWritten, refText);
    }
  ));
})

// For the rest of our tests, we'll rely on _everything_, including the normal
// form module text, being stored in files beneath our test directory (like
// the way we chose to `read` the hello.app.htm above instead of using inline
// string literals).  Otherwise, this test file will become a monster to
// scroll through and get a good idea of what's going on.

function cacheFileText(mockFS, dirName, fileName) {
  if (!dirName.endsWith("/")) dirName += "/";
  return read(dirName + fileName).then((text) => {
    mockFS.set(fileName, text);
  });
}

// Next up: the lines demo app.
test(() => {
  let files = new Map();

  let prepWork = Promise.all([
    cacheFileText(files, "$PROJECT/tests/sanity/lines", "LineChecker.js"),
    cacheFileText(files, "$PROJECT/tests/sanity/lines", "BrowserSystem.js"),
    cacheFileText(files, "$PROJECT/tests/sanity/lines", "NodeJSSystem.js"),
    cacheFileText(files, "$PROJECT/tests/sanity/lines", "main.js")
  ]);

  const referenceFile = "$PROJECT/tests/sanity/lines/lines.app.htm";

  return prepWork.then(() =>
    read(referenceFile).then((refText) => {
      let sys = new MockSystem(files);
      let kit = new TripleKit(sys);

      return kit.build([ "LineChecker.js" ], "main.js").then(() => {
        test.is(sys.pathWritten, "out.app.htm");
        test.is(sys.dataWritten, refText);
      })
    })
  );
})

// Now Inaft's harness.app.htm
test(() => {
  let files = new Map();

  const prefix =  "$PROJECT/tests/sanity/inaft";
  let prepWork = Promise.all([
    cacheFileText(files, prefix, "BrowserHandler.js"),
    cacheFileText(files, prefix, "BrowserQuirksHack.js"),
    cacheFileText(files, prefix, "BrowserUI.js"),
    cacheFileText(files, prefix, "BrowserWorkerHandler.js"),
    cacheFileText(files, prefix, "DependencySniffer.js"),
    cacheFileText(files, prefix, "ExecutionContext.js"),
    cacheFileText(files, prefix, "ExecutionTimeout.js"),
    cacheFileText(files, prefix, "FileCache.js"),
    cacheFileText(files, prefix, "FileReadAdapter.js"),
    cacheFileText(files, prefix, "FileUtils.js"),
    cacheFileText(files, prefix, "FixedWidthResultsFormatter.js"),
    cacheFileText(files, prefix, "InaftPathResolver.js"),
    cacheFileText(files, prefix, "InaftTestHelper.js"),
    cacheFileText(files, prefix, "Loader.js"),
    cacheFileText(files, prefix, "NodeJSHandler.js"),
    cacheFileText(files, prefix, "Require.js"),
    cacheFileText(files, prefix, "SimpleScanner.js"),
    cacheFileText(files, prefix, "SimpleStringParser.js"),
    cacheFileText(files, prefix, "Sourcerer.js"),
    cacheFileText(files, prefix, "System.js"),
    cacheFileText(files, prefix, "TestMediator.js"),
    cacheFileText(files, prefix, "TestRunner.js"),
    cacheFileText(files, prefix, "TextSplicer.js"),
    cacheFileText(files, prefix, "TripleScanner.js"),
    cacheFileText(files, prefix, "WebkitFileIngester.js"),
    cacheFileText(files, prefix, "credits.js"),
    cacheFileText(files, prefix, "main.js")
  ]);

  const referenceFile = "$PROJECT/tests/sanity/inaft/harness.app.htm";

  return prepWork.then(() =>
    read(referenceFile).then((refText) => {
      let sys = new MockSystem(files);
      let kit = new TripleKit(sys);

      return kit.build([ "TestRunner.js" ], "main.js").then(() => {
        test.is(sys.pathWritten, "out.app.htm");
        test.is(sys.dataWritten, refText);
      })
    })
  );
})

// Finally, our very own trplkt
test(() => {
  let files = new Map();

  const prefix =  "$PROJECT/tests/sanity/trplkt";
  let prepWork = Promise.all([
    cacheFileText(files, prefix, "BrowserQuirksHack.js"),
    cacheFileText(files, prefix, "BrowserSystem.js"),
    cacheFileText(files, prefix, "BrowserUI.js"),
    cacheFileText(files, prefix, "BuildUnit.js"),
    cacheFileText(files, prefix, "CLIParser.js"),
    cacheFileText(files, prefix, "CheckedInit.js"),
    cacheFileText(files, prefix, "ConsoleBuffer.js"),
    cacheFileText(files, prefix, "ConsoleOutput.js"),
    cacheFileText(files, prefix, "ConsoleWidget.js"),
    cacheFileText(files, prefix, "Creditizor.js"),
    cacheFileText(files, prefix, "DoubleTapClipboard.js"),
    cacheFileText(files, prefix, "FileCache.js"),
    cacheFileText(files, prefix, "FileNodeFSAdapter.js"),
    cacheFileText(files, prefix, "JSDocEmitter.js"),
    cacheFileText(files, prefix, "LineReader.js"),
    cacheFileText(files, prefix, "LineScanner.js"),
    cacheFileText(files, prefix, "LineWriter.js"),
    cacheFileText(files, prefix, "LinkageSniffer.js"),
    cacheFileText(files, prefix, "NodeJSSystem.js"),
    cacheFileText(files, prefix, "SimpleScanner.js"),
    cacheFileText(files, prefix, "SimpleStringParser.js"),
    cacheFileText(files, prefix, "Sourcerer.js"),
    cacheFileText(files, prefix, "TabBox.js"),
    cacheFileText(files, prefix, "TextScanner.js"),
    cacheFileText(files, prefix, "TextSplicer.js"),
    cacheFileText(files, prefix, "TripleKit.js"),
    cacheFileText(files, prefix, "TripleScanner.js"),
    cacheFileText(files, prefix, "TrplktAppStyle.js"),
    cacheFileText(files, prefix, "TrplktCommands.js"),
    cacheFileText(files, prefix, "WebkitFileIngester.js"),
    cacheFileText(files, prefix, "ZodLib.js"),
    cacheFileText(files, prefix, "credits.js"),
    cacheFileText(files, prefix, "main.js")
  ]);

  const referenceFile = "$PROJECT/tests/sanity/trplkt/Build.app.htm";
  const outFile = "Build.app.htm";

  return prepWork.then(() =>
    read(referenceFile).then((refText) => {
      let sys = new MockSystem(files);
      let kit = new TripleKit(sys);

      return kit.build([ "TripleKit.js" ], "main.js", outFile).then(() => {
        test.is(sys.pathWritten, outFile);
        test.is(sys.dataWritten, refText);
      })
    })
  );
})

// Decompilation is pretty involved.  We'll just revisit lines.app.htm.
test(() => {
  const inputFile = "$PROJECT/tests/sanity/lines/lines.app.htm";

  let compilationText, refs = new Map();
  let prepWork = Promise.all([
    cacheFileText(refs, "$PROJECT/tests/sanity/lines", "LineChecker.js"),
    cacheFileText(refs, "$PROJECT/tests/sanity/lines", "BrowserSystem.js"),
    cacheFileText(refs, "$PROJECT/tests/sanity/lines", "NodeJSSystem.js"),
    cacheFileText(refs, "$PROJECT/tests/sanity/lines", "main.js"),
    read(inputFile).then((text) => void(compilationText = text))
  ]);

  let sys = new MockSystem({
    get: (path) => {
      if (path == "lines.app.htm") {
        return compilationText;
      }
      error = new Error("Unexpected read: " + path);
    }
  });
  sys.write = (path, text) => (Promise.resolve(
    void(outputFiles.set(path, text))
  ));

  var error = null;
  var outputFiles = new Map();

  return prepWork.then(() => {
    test.ok(refs.has("LineChecker.js"));
    test.ok(refs.has("BrowserSystem.js"));
    test.ok(refs.has("NodeJSSystem.js"));
    test.ok(refs.has("main.js"));
    test.is(refs.size, 4);

    let kit = new TripleKit(sys);
    return kit.decompile("lines.app.htm").then((names) => {
      test.is(error && String(error), null);

      test.ok(outputFiles.has("LineChecker.js"));
      test.ok(outputFiles.has("BrowserSystem.js"));
      test.ok(outputFiles.has("NodeJSSystem.js"));
      test.ok(outputFiles.has("main.js"));
      test.is(outputFiles.size, 4);

      let $filePair = (name) => ([ outputFiles.get(name), refs.get(name) ]);

      test.is(...$filePair("LineChecker.js"));
      test.is(...$filePair("BrowserSystem.js"));
      test.is(...$filePair("NodeJSSystem.js"));
      test.is(...$filePair("main.js"));
    })
  });
})
