import { test, read } from "$TEST_HELPER"

import { ErrorCommons } from "$PROJECT/src/ErrorCommons.src"
import { Token } from "$PROJECT/src/Token.src"
import { TripleKit } from "$PROJECT/src/TripleKit.src"

// We are checking that the error output for unbalanced brackets (parens,
// square brackets, or curly braces) is reasonable.

function MockSystem(files) {
  this.pathWritten = null;
  this.dataWritten = null;

  const $res = (x) => (Promise.resolve(x));
  const $err = (x) => (Promise.reject(new MockSystem.Error(x)));

  this.read = (path) => {
    let result = files.get(path);
    if (result) {
      return $res(files.get(path));
    }
    return $err("invalid read: " + path);
  }

  this.write = (path, contents) => {
    if (this.pathWritten != null) {
      return $err(
        "multiple writes:\n - " + this.pathWritten + " + " + path
      );
    }
    this.dataWritten = contents;
    this.pathWritten = path;
    return $res(void(0));
  };
}

MockSystem.Error = class extends Error { };

// If this test is failing, it indicates one of two things:
// 1. `ErrorCommons.createBadTokenMessage` has been changed to produce output
//     that differs from the way it looked at the time this test was written.
// 2. This type of parsing error is no longer using `createBadTokenMessage`.
//
// This is important to test for, because all the remaining tests in this file
// depend upon these basic invariants.  If all the tests are failing, then the
// tests probably need to be rewritten to account for the new way that
// feedback for parsing errors are dealt with.  If this test is passing but
// the rest are failing, then it's something else.
test(function error_feedback_still_looks_the_same() {
  let message = ErrorCommons.createBadTokenMessage(
    Token.NULL, Token.OPEN_CURLY
  );

  test.ok(message.indexOf("token: \"null\"") >= 0);
  test.ok(message.indexOf("wanted \"{\"") >= 0);
})

test(function expected_close_curly_brace() {
  let files = new Map();
  files.set(
    "Hello.src",
           `export class Hello {` +
    '\n' + `  static greet(system) {` +
    '\n' + `    system.printLine(Hello.MESSAGE);` +
    '\n' + `  }` +
    '\n' + `}` +
    '\n' +
    '\n' + `Hello.MESSAGE = "Hello, world.";` +
    '\n'
  );
  files.set(
    "main.src",
           `import { Hello } from "./Hello.src";` +
    '\n' +
    '\n' + `void function main() {` +
    '\n' + `  if (!system) var system = {` +
    '\n' + `    printLine: ((x) => { console.log(x) ))` +
    // The preceding line contains an error here ---^
    // (It should be a close curly bracket.)
    '\n' + `  }` +
    '\n' +
    '\n' + `  Hello.greet(system);` +
    '\n' + `} ()` +
    '\n'
  );

  let sys = new MockSystem(files);
  let kit = new TripleKit(sys);

  let problem = null;
  return kit.build([ "Hello.src" ], "main.src").catch((err) => {
    problem = err;
  }).finally(() => {
    test.ok(!!problem);
    test.ok(!(problem instanceof MockSystem.Error));

    if ("inner" in problem) {
      problem = problem.inner;
    }

    let [ what ] = String(problem).split("\n", 1);
    test.ok(what.indexOf("token: \")\"") >= 0);
    test.ok(what.indexOf("wanted \"}\"") >= 0);
  });
})

// TODO: test the others
