import { test } from "$TEST_HELPER";

import { CommonErrors } from "$PROJECT/src/CommonErrors.src";
import { TripleKit } from "$PROJECT/src/TripleKit.src";

function MockSystem(files) {
  const $err = (x) => (Promise.reject(x));
  const $res = (x) => (Promise.resolve(x));

  this.read = (path) => {
    let result = files.get(path);
    if (result) {
      return $res(files.get(path));
    }
    return $err(Error("invalid read: " + path));
  }

  this.write = (path, contents) => {
    if (this.fileWrites.indexOf(path) >= 0) {
      return $err(
        Error("multiple writes to path: " + path)
      );
    }
    this.fileWrites.push(path);
    return $res(void(0));
  };

  this.fileWrites = [];
}

test(function decompile_two_modules_same_name() {
  const DUMMY_FILE_NAME = "foobar.app.htm";
  const text =
         `//? <`+`script>` +
  '\n' + `/// import { Foo } from "./Foo.src";` +
  '\n' +
  '\n' + `/// export` +
  '\n' + `class Bar { }` +
  '\n' + `//? <`+`/script>` +
  '\n' + `//? <`+`script>` +
  '\n' + `/// export` +
  '\n' + `class Foo { }` +
  '\n' + `//? <`+`/script>` +
  '\n' + `//? <`+`script>` +
  '\n' + `/// import { Foo } from "./Foo.src";` +
  '\n' +
  '\n' + `/// export` +
  '\n' + `class Baz { }` +
  '\n' + `//? <`+`/script>` +
  '\n' + `//? <`+`script>` +
  '\n' + `/// export` +
  '\n' + `class Foo { }` +
  '\n' + `//? <`+`/script>` +
  '\n' + `//? <`+`script>` +
  '\n' + `/// import { Bar } from "./bar/Bar.src";` +
  '\n' + `/// import { Baz } from "./baz/Baz.src";` +
  '\n' +
  '\n' + `void function main() { } ()` +
  '\n' + `//? <`+`/script>` +
  '\n';

  let sys = new MockSystem({
    get: (path) => {
      if (path == DUMMY_FILE_NAME) {
        return text;
      }
      return null;
    }
  });

  // NB: this is a wrapper to deal with the fact that we _expect_
  // decompilation to fail.
  function doDecompile(kit, name = DUMMY_FILE_NAME) {
    return kit.decompile(name).catch((err) => {
      return Promise.resolve(err);
    });
  }

  return doDecompile(new TripleKit(sys)).then((error) => {
    try {
      test.ok(error instanceof CommonErrors.DialectError);
      test.ok(error.inner != null);
      test.ok(error.inner.message.startsWith("Name collision"));
      test.ok(error.inner.item.name == "Foo");
    } catch (ex) {
      return Promise.reject(ex);
    }
  });
})

test(function build_where_two_modules_have_same_name() {
  let sys = new MockSystem({
    get: (path) => {
      switch (path) {
        case "main.src":
          return (
                   "import { Bar } from \"./a/Bar.src\"" +
            "\n" + "import { Baz } from \"./a/Baz.src\"" +
            "\n" +
            "\n" + "void function main() {" +
            "\n" + "  if (!!Bar || !!Baz) throw Error(\"panicking!\");" +
            "\n" + "} ()" +
            "\n"
          )
        break;
        case "a/Bar.src":
          return (
                   "export" +
            "\n" + "const Bar = \"Bar\";" +
            "\n" +
            "\n" + "import { Foo } from \"../x/Foo.src\"" +
            "\n"
          )
        break;
        case "a/Baz.src":
          return (
                   "export" +
            "\n" + "const Baz = \"Baz\";" +
            "\n" +
            "\n" + "import { Foo } from \"../y/Foo.src\"" +
            "\n"
          )
        break;
        case "x/Foo.src":
          return (
                   "export" +
            "\n" + "const Foo = \"x/Foo\";" +
            "\n"
          )
        break;
        case "y/Foo.src":
          return (
                   "export" +
            "\n" + "const Foo = \"y/Foo\";" +
            "\n"
          )
        break;
      }
    }
  });

  function doBuild(kit) {
    return kit.build(null, "main.src").then((name) => {
      return Promise.resolve(null); // what shouldn't happen
    }).catch((err) => {
      return Promise.resolve(err); // what should happen
    });
  }

  return doBuild(new TripleKit(sys)).then((error) => {
    try {
      test.ok(error instanceof CommonErrors.DialectError);
      test.ok(error.inner != null);
      test.ok(error.inner.message.startsWith("Name collision"));
      test.ok(error.inner.item.name == "Foo");
    } catch (ex) {
      return Promise.reject(ex);
    }
  });
})
