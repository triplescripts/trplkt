/// <script>
/// export
const TripleKit = null;
/// </script>
/// <script>
/// export
const BrowserSystem = null;
/// </script>
/// <script>
/// export
const CheckedInit = null;
/// </script>
/// <script>
/// export
const TrplktCommands = null;
/// </script>
/// <script>
/// export
const NodeJSSystem = null;
/// </script>
/// <script>
/// import { BrowserSystem } from "./BrowserSystem.js";
/// import { CheckedInit } from "./CheckedInit.js";
/// import { NodeJSSystem } from "./NodeJSSystem.js";
/// import { TripleKit } from "./TripleKit.js";
/// import { TrplktCommands } from "./TrplktCommands.js";

void function main() {
  if (typeof(__FORCE_LIB__) != "undefined") {
    if (typeof(module) != "undefined" && "exports" in module) {
      module.exports.TripleKit = TripleKit;
      module.exports.TrplktCommands = TrplktCommands;
    } else {
      throw new CheckedInit.ShuntingError("No CommonJS-style `module` here");
    }
  } else {
    if (!system) var system = CheckedInit.create(BrowserSystem);
    if (!system) var system = CheckedInit.create(NodeJSSystem);
    if (!system) throw CheckedInit.ShuntingError("Unknown environment");

    system.run();
  }
} ();
/// </script>
