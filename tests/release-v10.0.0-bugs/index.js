import { test, read } from "$TEST_HELPER";

import { TripleKit } from "$PROJECT/src/TripleKit.src";

function MockSystem(files) {
  const $res = (x) => (Promise.resolve(x));
  const $err = (x) => (Promise.reject(x));

  this.read = (path) => {
    let result = files.get(path);
    if (result) {
      return $res(files.get(path));
    }
    return $err(Error("invalid read: " + path));
  }

  this.write = (path, contents) => {
    return $res(void(0));
  };
}

/*
 * A bug made it into release where root blocks (such as the shunting block)
 * that have no `$path$` attribute cause "decompile" to fail, since we rely on
 * a path being attached to the compiler's internal representation for that
 * module.
 *
 * This caused the unfortunate side effect that trplkt v0.9.8 cannot be
 * decompiled with v0.10.0.  Oops.
 */
test(function unattributed_unreachable_roots_decompile() {
  const PATH = "$PROJECT/tests/release-v10.0.0-bugs/trplkt-v0.9.8-abridged.x";

  return read(PATH).then((contents) => {
    let files = new Map();
    files.set("trplkt.app.htm", contents);

    let compiler = new TripleKit(new MockSystem(files));
    return compiler.decompile("trplkt.app.htm").then((names) => {
      try {
        test.is((names.sort()).length, 6);
        test.is(names.shift(), "BrowserSystem.js");
        test.is(names.shift(), "CheckedInit.js");
        test.is(names.shift(), "NodeJSSystem.js");
        test.is(names.shift(), "TripleKit.js");
        test.is(names.shift(), "TrplktCommands.js");
        test.is(names.shift(), "main.js");
      } catch (ex) {
        return Promise.reject(ex);
      }
    })
  })
})

/*
 * ... and we shouldn't need a `$path$` in most programs (including trplkt
 * itself), because we can infer the file name.  The existing tests are
 * adequate to test this fix, so nothing is included here.
 */
