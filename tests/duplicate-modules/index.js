import { test } from "$TEST_HELPER";

import { RealParser } from "$PROJECT/src/RealParser.src";

test(() => {
  let error, text = "" +
         "import { Foo } from \"./Foo.src\";" +
  '\n' + "import { Bar } from \"./Bar.src\";" +
  '\n' + "import { Foo } from \"./Foo.src\";" +
  '\n' +
  '\n' + "void function main() {" +
  '\n' + "  console.log('foo');" +
  '\n' + "} ()" +
  '\n';

  let parser = new RealParser();

  try {
    parser.ingestNormalText(text);
  } catch (ex) {
    error = ex;
  }

  test.ok(!!error);
  test.ok(
    String(error.message).includes("Multiple imports of module")
  );
})
