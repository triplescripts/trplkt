trplkt
======

trplkt is the triple script compiler.

(Triple scripts are fully self-contained, inspectable, and end-user hackable
programs that run anywhere and should require no additional setup.  Learn more
about triple scripts at <https://triplescripts.org>.)

The triple script file format produced by trplkt's build process is assembled
from one or more source code modules as inputs, and trplkt's compilation is
"non-destructive" so it (or another tool) can do the inverse—to decompile a
pre-built triple script given as input to extract the original modules that
were first used to build it, and trplkt can reliably achieve this with perfect
fidelity.  When a project adopts triple scripts for its "metatooling" (i.e.
the tools that project collaborators use for project development, including
the build tools), we describe such projects where this kind of roundtripping
is possible as *automorphic*.

The other benefit of a program being implemented as a triple script is that
you can run it both (a) without ever leaving the terminal, in case that's how
you prefer to get things done—and if you don't, then it's not a problem
because (b) you can always use run a triple script in a graphical environment,
which should work on most systems if you launch them by double clicking.  We
call programs that offer this kind of choice *homologous*.

trplkt is itself implemented as a triple script, so it should work with no
fuss, across all (sufficiently advanced) platforms.  (When we refer to "all"
platforms, that includes as a rule of thumb workstations which can run, say, a
modern web browser; but whether the underlying system is Mac OS, or a
freedesktop.org-related platform, or a system from some other vendor, you
should find that triple scripts are supported everywhere.)

Building
========

Using `Build.app.htm` (for example, after double clicking it and then pointing
it at the project source tree), build trplkt by running the following command:

    build ./main.src

(If you prefer to do the build from the terminal, you can also do so using
NodeJS; you'd type `node ./Build.app.htm $BUILDCOMMAND`, where `$BUILDCOMMAND`
is the command given above.)

Using
=====

If you've built trplkt from the steps above, then you've already used it.
This is because trplkt's `Build.app.htm` is a prebuilt copy of trplkt included
for bootstrapping.  It should not be surprising that trplkt—a build tool for
triple scripts—uses a trplkt-based build system itself.

Feel free to use the resulting file `out.app.htm` to run another build (try
using the option `-o two.app.htm` this time) and compare the outputs to verify
that they match.

That other build tools would be implemented as triple scripts is an explicit
goal for trplkt and triplescripts.org—even build tools for projects written in
other languages.  In that case, trplkt serves as both a tool and an example
for how to develop these other tools.
  
Can your projects' build systems be implemented as triple scripts?  If you
maintain or contribute to a project that's already meant to be a simple,
non-network-connected app or utility program, could your project itself be
implemented as a triple script?

trplkt options
--------------

The following commands and options are supported in trplkt:

  build

    -f  build from the given module(s) and their dependencies

    -m  put the given module in the shunting block position

    -o  save the output with the name given (defaults to out.app.htm)

  create-credits

    -o  save the output with the name given (defaults to credits.src)

  decompile

    -x  extract only the named module(s)

  init

    save a copy of trplkt to the given path

License
=======

This work is made available under the terms of the MIT License.

Copyright individual authors and contributors; see ACKNOWLEDGEMENTS.txt.
