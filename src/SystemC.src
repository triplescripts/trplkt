import { AcknowledgeMint } from "./AcknowledgeMint.src";
import { CLIParser } from "./CLIParser.src";
import { CommonErrors } from "./CommonErrors.src";
import { ConsoleOutput } from "./ConsoleOutput.src";
import { FeedbackLevels } from "./FeedbackLevels.src";
import { FileNodeFSAdapter } from "./FileNodeFSAdapter.src";
import { SystemA } from "./SystemA.src";
import { TripleKit } from "./TripleKit.src";
import { TrplktCommands } from "./TrplktCommands.src";
import { credits } from "./credits.src";

export
class SystemC extends SystemA {
  constructor() {
    super();

    if (typeof(process) == "undefined" || typeof(module) == "undefined") {
      throw new SystemA.ShuntingError("Not a NodeJS environment");
    }

    if (process.argv.indexOf("--version") >= 0) {
      console.log(credits);
      process.exit(0);
    }

    const fs = module.require("fs");
    const path = module.require("path");

    this._process = process;

    let [ engine, script, ...eargs ] = process.argv;
    this._engine = engine;
    this._script = script;
    this._args = eargs;

    this.input = null;
    let simutype = new ConsoleOutput({
      printLine: ((text) => void(console.log(text)))
    });
    this.output = new FeedbackLevels(simutype);

    this._files = new FileNodeFSAdapter(fs, path);

    this._cli = TrplktCommands.initDefinitions();

    this.$getOwnSource = (() => (
      fs.readFileSync(module.filename, { encoding: "utf8" })
    ));
  }

  get identity() {
    return this.$getOwnSource();
  }

  run() {
    this._processArguments(this._cli, this._args).then(() => {
      this._process.exit(SystemC.EXIT_SUCCESS);
    }).catch((err) => {
      if (err instanceof TrplktCommands.KnownFatalError) {
        var fatal = err;
        err = err.original;
      }
      if (err instanceof CLIParser.ArgError) {
        console.log(err.message + "\n");
        let commandList = "";
        let commands = this._cli.listCommands();
        for (let i = 0, n = commands.length; i < n; ++i) {
          commandList += "    " + commands[i] + "\n";
        }
        let engine = this._engine.substr(this._engine.lastIndexOf("/") + 1);
        let script = this._script.substr(this._script.lastIndexOf("/") + 1);
        console.log(
          "Usage: " + engine + " " + script + " <command> FILE" + "\n\n" +
          "Available commands:" + "\n\n" + commandList
        );
        var status = SystemC.EXIT_BADARGS;
      } else if (err instanceof AcknowledgeMint.InputFormatError) {
        var status = SystemC.EXIT_BADTEXT;
      } else if (err instanceof TripleKit.NoSuchFileError) {
        var status = SystemC.EXIT_BADFILE;
      } else if (err instanceof CommonErrors.DialectError) {
        var status = SystemC.EXIT_BADTEXT;
      } else {
        console.error("Unrecognized error:", err);
        var status = SystemC.EXIT_EXECERR;
      }

      if (fatal == undefined) {
        console.error("Unhandled error:", err);
        status = SystemC.EXIT_EXECERR;
      }
      this._process.exit(status);
    });
  }

  _processArguments(parser, args) {
    const { KnownFatalError } = TrplktCommands;
    let params = parser.handle(args);
    if (params) {
      let commands = new TrplktCommands(this);
      return commands.executeStructuredCommand(parser.command, params);
    }
    return Promise.reject(
      KnownFatalError.check(parser.error, CLIParser.ArgError) ||
      parser.error
    );
  }

  read(path) {
    return this._files.read(path).catch((err) => {
      return this._wrapFSError(err, path);
    });
  }

  write(path, contents) {
    return this._files.write(path, contents).catch((err) => {
      return this._wrapFSError(err, path);
    });
  }

  _wrapFSError(nodeError, path) {
    switch (nodeError.code) {
      case "ENOENT":
        return Promise.reject(new TripleKit.NoSuchFileError(path));
      break;
    }
    return Promise.reject(nodeError);
  }
}

SystemC.EXIT_EXECERR = -1;
SystemC.EXIT_SUCCESS = 0;
SystemC.EXIT_BADARGS = 1;
SystemC.EXIT_BADFILE = 2;

SystemC.EXIT_BADTEXT = 10;
