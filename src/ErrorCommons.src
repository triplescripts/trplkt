import { ProgramText } from "./ProgramText.src";
import { Token } from "./Token.src";
import { TokenScanner } from "./TokenScanner.src";

// TODO Transplant more stuff into this module (currently too spread out).

export
class ErrorCommons {
  // NB: parameterized constructor allows this file to avoid binding to
  // TripleKit by name (as an import).  This prevents a dependency cycle.
  static createNoSuchFileMessage(ex, importErrorConstructor) {
    let feedback = ex.message + ": " + ex.path;
    if (ex instanceof importErrorConstructor && ex.importer) {
      feedback += " (for import found in " + ex.importer.path + ")";
    }
    return feedback;
  }

  static makeSourceContextString(preamble, program, token) {
    let input = String(program);
    let no = ProgramText.getLineNumber(token.position, program.lines);

    let { start } = program.lines[no - 1];
    let till = input.indexOf("\n", start);
    if (input.charAt(till - 1) == "\r") {
      --till;
    }
    let code = input.substring(start, till); // XXX

    let where = token.position;
    let marker = [];
    for (let i = 0, n = where - start; i < n; ++i) {
      marker.push("-");
    }

    return (
      preamble + " (on line " + no + ")" + "\n\n" +
      ">>> " + code + "\n" +
      ">>> " + marker.join("") + "^"
    );
    // XXX line number placement is not ideal; check notes from the commit log
  }

  static createSyntaxErrorMessage(ex) {
    let feedback = ex.message + " for " + ex.path;
    if (ex.inner) {
      if (("detail" in ex.inner) && (ex.inner.detail != null)) {
        feedback += "\n\n" + ex.inner.detail;
      } else {
        feedback += "\n\n" + ex.inner.message;
      }
    }
    return feedback;
  }

  static createBadTokenMessage(have, want = null, why = null) {
    const { TokenHelper } = ErrorCommons;

    let helper = new TokenHelper();
    if (have == TokenScanner.ERROR) {
      var feedback = why; // a specific scanner "ERROR_*" constant string
    } else if (have == TokenScanner.EOF) {
      var feedback = "Unexpected EOF";
    } else {
      var feedback = "Unexpected token: " + helper.describeToken(have);
      // assert(want != null)
    }
    if (want) {
      feedback += " (wanted " + helper.describeToken(want) + ")";
    }

    return feedback;
  }
}

// For internal use only
ErrorCommons.TokenHelper = class TokenHelper {
  constructor(map = null) {
    if (!map) map = TokenScanner.addKeywords(new Map);

    this.kindToKeywordMap = new Map();
    for (let [ content, token ] of map) {
      this.kindToKeywordMap.set(token, content);
    }
  }

  describeToken(which) {
    switch (which) {
      case Token.IDENTIFIER_NAME: return "identifier";
      case Token.NUMBER_LITERAL: return "number";
      case Token.STRING_LITERAL: return "string";
      case Token.SEMISTATIC_FORM: return "semistatic";
      case Token.PATTERN_LITERAL: return "pattern";
      case Token.ATTRIBUTE_NAME: return "attribute";
      case Token.OPEN_DELIMITER: return "open delimiter";
      case Token.CLOSE_DELIMITER: return "open delimiter";
      case Token.WHITESPACE: return "whitespace";
      case Token.COMMENT: return "comment";

      case Token.AMPERSAND_EQUAL: return JSON.stringify("&=");
      case Token.AND: return JSON.stringify("&&");
      case Token.ARROW: return JSON.stringify("=>");
      case Token.ASTERISK_EQUAL: return JSON.stringify("*=");
      case Token.BAR_EQUAL: return JSON.stringify("|=");
      case Token.CARET_EQUAL: return JSON.stringify("^=");
      case Token.DOT_DOT_DOT: return JSON.stringify("...");
      case Token.DOUBLE_EQUALS: return JSON.stringify("==");
      case Token.GREATER_OR_EQUAL: return JSON.stringify(">=");
      case Token.LEFT_SHIFT: return JSON.stringify("<<");
      case Token.LEFT_SHIFT_EQUAL: return JSON.stringify("<<=");
      case Token.LESS_OR_EQUAL: return JSON.stringify("<=");
      case Token.MINUS_EQUAL: return JSON.stringify("-=");
      case Token.MINUS_MINUS: return JSON.stringify("--");
      case Token.NOT_EQUAL: return JSON.stringify("!=");
      case Token.NOT_STRICTLY_EQUAL: return JSON.stringify("!==");
      case Token.OR: return JSON.stringify("||");
      case Token.PERCENT_EQUAL: return JSON.stringify("%=");
      case Token.PLUS_EQUAL: return JSON.stringify("+=");
      case Token.PLUS_PLUS: return JSON.stringify("++");
      case Token.RIGHT_SHIFT: return JSON.stringify(">>");
      case Token.RIGHT_SHIFT_EQUAL: return JSON.stringify(">>=");
      case Token.SLASH_EQUAL: return JSON.stringify("/=");
      case Token.TRIPLE_EQUALS: return JSON.stringify("===");
      case Token.UNSIGNED_RIGHT_SHIFT: return JSON.stringify(">>>");
      case Token.UNSIGNED_RIGHT_SHIFT_EQUAL: return JSON.stringify(">>>=");

      default:
        if (0x20 <= which && which < 0x7F) {
          return "\"" + String.fromCharCode(which) + "\"";
        } else if (which > 0) {
          let keyword = this.kindToKeywordMap.get(which);
          if (keyword) {
            return "\"" + keyword + "\"";
          }
        }
      break;
    }
    throw new Error("Unknown token (" + which + ")");
  }
}
