/*
 * trplkt is the triplescripts.org group's reference compiler.  If you're
 * looking at this right now, it's most likely one of the following reasons...
 *
 * - you're working on trplkt, or (more likely):
 * - some other project you're dealing with is using triple scripts--maybe
 *   that project itself is a triple script
 *
 * One indicator that it's the latter would be if you opened a file named
 * Build.app.htm and that's what you're reading right now.  If so, there
 * should've also been a README explaining how to use Build.app.htm.  (If
 * there aren't any instructions, try filing a bug against the project.)
 */

export
class TripleKit {
  // More info about this stuff is available at <https://triplescripts.org>.
  //
  // The primary use case for trplkt involves two things:
  // - compiling a triple script
  // - decompiling a triple script
  //
  // It's here in `TripleKit` where the heart of the trplkt compiler resides--
  // particularly the `build` and `decompile` methods below.
  //
  // (If you want to understand where the real "entry point" for the compiler
  // is, refer to trplkt's function `main`--the so-called "shunting block",
  // which is the first thing that executes; it's responsible for initializing
  // the "system" layer.  From main.src, you can trace control flow through
  // the system layer(s) to the command processor, which is ultimately what
  // ends up dispatching one of our `build`, `decompile`, etc. methods here.)
  //
  // The system layer is how trplkt interfaces with the underlying host
  // environment for reading files, etc.  See the class `SystemA` for the
  // rationale for that design pattern and details about the operations we
  // depend on in the heart of the compiler here in the "application" layer.
  constructor(system) {
    this._system = system;
  }

  ////////////////////////////////////////////////////////////////////////////

  build(paths, shunt, fileName = "out.app.htm") {
    let shuntID = PathResolver.normalizePath(shunt);
    if (paths) {
      var roots = paths.map((x) => (PathResolver.normalizePath(x)));
    } else {
      var roots = [];
    }
    roots.push(shuntID);

    let cache = new Map();
    let processing = new Set();

    let pending = [];
    let parser = new RealParser();
    for (let i = 0, n = roots.length; i < n; ++i) {
      // NB: Every call here should be generating a de novo item.
      let item = CompilerItem.BuildItem.getCachedItem(roots[i], cache);
      pending.push(this._cueAsyncWorkOnInput(item, processing, parser));
    }

    let linkage = new ProgramLinkage(cache);
    let emitter = new Emitter(InputIR.GBlock);

    return Promise.all(pending).then(() => {
      const { DialectError } = CommonErrors;

      let order = linkage.collate(processing, roots, shuntID);

      let names = new Set();
      for (let i = 0, n = order.length; i < n; ++i) {
        let item = order[i];
        let err = linkage.validate(item, names);
        if (err) {
          return DialectError.handleLinkValidationError(this, err, item.path);
        }

        item.module = emitter.prepareForOutput(item.parsed);
        if (!item.reachable) {
          if (item.path != shuntID || item.path != item.inferFileName()) {
            item.module.addAttribute("path", item.path);
          }
        }
      }

      let text = emitter.textify(InputIR.FORM_TRIPLE);

      this._log("writing " + fileName);
      return this._system.write(fileName, text).then(() => (
        Promise.resolve(fileName)
      ));
    });
  }

  _cueAsyncWorkOnInput(item, set, parser) {
    // NB: This lets us avoid races, since 'read' is asynchronous.  Pay
    // careful attention to effect that the set membership check has on
    // timing!  And heed the name--resolution of the returned promise *only*
    // guarantees that work is cued up--not that the given item's input is
    // parsed and that the IR is available...
    if (set.has(item)) {
      return Promise.resolve();
    }
    set.add(item);

    return this._system.read(item.path).catch((err) => {
      if (err instanceof TripleKit.NoSuchFileError) {
        err = new TripleKit.ImportError(item)
      }
      return Promise.reject(err);
    }).then((contents) => {
      try {
        item.parsed = parser.ingestNormalText(contents);
      } catch (ex) {
        return Promise.reject(new CommonErrors.DialectError(item.path, ex));
      }

      return this._processDependencies(item, set, parser);
    });
  }

  // NB: Context matters; both this method and `_cueAsyncWorkOnInput` exist
  // *only* to make sure the module graph gets walked and the parser's results
  // made available (as IR).  Any substantial work predicated on the the IR
  // actually being ready for use should *not* happen here.  Instead, there is
  // an opportune moment to do it after units have been collated (and are
  // conveniently available deduplicated, in a list appearing in the exact
  // same module order that will eventually be written as compilation text).
  // So do it there, at the time when the input IR is converted to output IR.
  _processDependencies(item, set, parser) {
    let pending = [];

    const { ImportError } = TripleKit;

    let { imports } = item.parsed;
    for (let i = 0, n = imports.length; i < n; ++i) {
      let nu, twig = imports[i].path;
      let importee = item.getRelated(twig);

      if (PathResolver.isRelative(twig)) {
        nu = this._cueAsyncWorkOnInput(importee, set, parser).catch((err) => {
          if (err instanceof ImportError && err.importee == importee &&
              err.importer == null) {
            err.importer = item;
          }
          return Promise.reject(err);
        });
      } else {
        nu = Promise.reject(new ImportError(importee, item));
      }
      pending.push(nu);

      importee.reachable = true;
      item.dependIDs.push(importee.path);
      if (imports[i].name == item.link) {
        item.priority = importee;
      }
    }

    return Promise.all(pending);
  }

  ////////////////////////////////////////////////////////////////////////////

  decompile(path, filters = null) {
    const { DialectError } = CommonErrors;
    const $err = ((ex) => Promise.reject(new DialectError(path, ex)));
    return this._system.read(path).then((contents) => {
      this._log("decompiling " + path);

      let roots = [];
      try {
        let map = this._createNeonatalGroup(contents, roots);
        var writable = this._resolveNodesAndPaths(map, roots, filters);
      } catch (ex) {
        return $err(ex);
      }

      if (filters && writable.length != filters.length) {
        return $err(Error("Inextricable module(s)"));
      }

      return this._writeSourceFiles(writable);
    });
  }

  _createNeonatalGroup(text, roots) {
    const $err = ((why) => (new CommonErrors.DialectError(null, why)));
    let result = new Map();

    let parser = new RealParser();
    let parsed = parser.ingestCompilationText(text);
    for (let i = 0, n = parsed.length; i < n; ++i) {
      let item = CompilerItem.from(parsed[i]);

      try {
        if (result.has(item.name)) {
          throw new ProgramLinkage.NameCollisionError(item);
        }
      } catch (ex) {
        throw $err(ex);
      }

      result.set(item.name, item);
      if (item.path != null) {
        roots.push(item);
      }
    }

    return result;
  }

  _resolveNodesAndPaths(map, roots, filters) {
    const { DialectError } = CommonErrors;
    const $err = ((why) => (new DialectError(null, new Error(why))));
    let emitter = new Emitter();
    let result = [];

    let stack = [ ...roots ];
    let finished = new Set();

    while (stack.length) {
      let item = stack.pop();
      let branch = item.path;
      for (let i = 0, n = item.parsed.imports.length; i < n; ++i) {
        let declaration = item.parsed.imports[i];
        let path = PathResolver.normalizePath(declaration.path, branch);
        let dependency = map.get(declaration.name);
        if (dependency && dependency.path == null) {
          dependency.path = path;
          if (!finished.has(dependency)) {
            stack.push(dependency);
          }
        } else if (!dependency || dependency.path != path) {
          throw $err("Unresolvable module: " + declaration.name);
        }
      }

      if (!filters || filters.indexOf(item.name) >= 0) {
        item.module = emitter.prepareForOutput(item.parsed);
        result.push(item);
      }

      finished.add(item);
    }

    if (map.size != finished.size) {
      // XXX arguably not a dialect error?
      throw $err("Unreachable module(s)");
    }

    return result;
  }

  _writeSourceFiles(items) {
    let result = [], pending = [];

    for (let i = 0, n = items.length; i < n; ++i) {
      let { name, path, text } = items[i];
      this._log("  module " + name + "...");
      pending.push(this._system.write(path, text));
      result.push(path);
    }

    return new Promise((resolve) =>
      Promise.all(pending).then(() => resolve(result))
    );
  }

  ////////////////////////////////////////////////////////////////////////////

  publish(fileName) {
    const { TBlock } = InputIR;
    const { GBlock } = InputIR;

    // TODO Make sure decompile and build constraints are enforced here, too.
    return this._system.read(fileName).then((contents) => {
      try {
        var parsed = (new RealParser).ingestCompilationText(contents, GBlock);
        var emitter = Emitter.from(parsed, TBlock);
      } catch (ex) {
        return Promise.reject(new CommonErrors.DialectError(fileName, ex));
      }

      let text = emitter.textify(InputIR.FORM_TRIPLE);

      this._log("writing " + fileName);
      return this._system.write(fileName, text).then(() => (
        Promise.resolve(fileName)
      ));
    });
  }

  ////////////////////////////////////////////////////////////////////////////

  createCredits(inputPath, outputPath = "credits.src") {
    return this._system.read(inputPath).then((input) => {
      try {
        var output = AcknowledgeMint.process(input, inputPath);
      } catch (ex) {
        if (ex instanceof AcknowledgeMint.InputFormatError) {
          ex.path = inputPath;
        }
        return Promise.reject(ex);
      }

      this._log("writing " + outputPath);
      return this._system.write(outputPath, output);
    });
  }

  ////////////////////////////////////////////////////////////////////////////

  init(path) {
    return this._system.write(path, this._system.identity);
  }

  ////////////////////////////////////////////////////////////////////////////

  // Misc. utility methods; not necessarily a stable part of the interface
  // (even if given names that suggest they are "public").

  getProgramText(path) {
    return this._system.read(path).then((contents) => {
      return (new ProgramText(contents)).scanWhole();
    });
  }

  _log(text) {
    let { output } = this._system;
    if (output) {
      output.report(text);
    }
  }
}

TripleKit.NoSuchFileError = class NoSuchFileError extends Error {
  constructor(path, message = "No such file") {
    super(message);
    this.path = path;
  }
}

// NB: This is _not_ a general-purpose class for signalling problems with
// imports.  It is specifically meant for imports that refer to  files that
// don't exist.
TripleKit.ImportError = class ImportError extends TripleKit.NoSuchFileError {
  constructor(importee, importer = null) {
    super(importee.path, "Invalid path");
    this.importee = importee;
    this.importer = importer;
  }
}

TripleKit.NoFileSourceError = class NoFileSourceError extends Error {
  constructor() {
    super("No file source");
  }
}

import { PathResolver } from "./PathResolver.src";
import { RealParser } from "./RealParser.src";
import { CompilerItem } from "./CompilerItem.src";
import { ProgramLinkage } from "./ProgramLinkage.src";
import { Emitter } from "./Emitter.src";
import { InputIR } from "./InputIR.src";
import { CommonErrors } from "./CommonErrors.src";
import { AcknowledgeMint } from "./AcknowledgeMint.src";
import { ProgramText } from "./ProgramText.src";
