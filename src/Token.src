export
class Token {
  constructor(kind, position, content) {
    this.kind = kind;
    this.position = position;
    this.content = content;
  }
}

// NB: The constants here are carefully chosen not to overlap with the value
// of any printable ASCII character or NUL.
Token.IDENTIFIER_NAME = -1;
Token.NUMBER_LITERAL = -2;
Token.STRING_LITERAL = -3;
Token.SEMISTATIC_FORM = -4;
Token.PATTERN_LITERAL = -5;
Token.ATTRIBUTE_NAME = -6;

Token.OPEN_DELIMITER = -10;
Token.CLOSE_DELIMITER = -11;

Token.WHITESPACE = -20;
Token.COMMENT = -21;

Token.UNEXPECTED_JUNK = -99;

// NB: Single-character tokens' discriminants overlap with the ASCII value.
// This allows us to get some extra mileage out of these definitions so they
// can also be used for internal character matching during scanning.
Token.AMPERSAND = 0x26;
Token.ASTERISK = 0x2A;
Token.BANG = 0x21;
Token.BAR = 0x7C;
Token.CARET = 0x5E;
Token.CLOSE_CURLY = 0x7D;
Token.CLOSE_PAREN = 0x29;
Token.CLOSE_SQUARE = 0x5D;
Token.COLON = 0x3A;
Token.COMMA = 0x2C;
Token.EQUAL = 0x3D;
Token.GREATER_THAN = 0x3E;
Token.LESS_THAN = 0x3C;
Token.MINUS = 0x2D;
Token.OPEN_CURLY = 0x7B;
Token.OPEN_PAREN = 0x28;
Token.OPEN_SQUARE = 0x5B;
Token.PERCENT = 0x25;
Token.PERIOD = 0x2E;
Token.PLUS = 0x2B;
Token.QUESTION_MARK = 0x3F;
Token.SEMICOLON = 0x3B;
Token.SLASH = 0x2F;
Token.TILDE = 0x7E;

// Multi-character, non-keyword symbols
Token.AMPERSAND_EQUAL = 151;
Token.AND = 152;
Token.ARROW = 153;
Token.ASTERISK_EQUAL = 173;
Token.BAR_EQUAL = 154;
Token.CARET_EQUAL = 155;
Token.DOT_DOT_DOT = 156;
Token.DOUBLE_EQUALS = 157;
Token.GREATER_OR_EQUAL = 158;
Token.LEFT_SHIFT = 159;
Token.LEFT_SHIFT_EQUAL = 160;
Token.LESS_OR_EQUAL = 161;
Token.MINUS_EQUAL = 162;
Token.MINUS_MINUS = 163;
Token.NOT_EQUAL = 164;
Token.NOT_STRICTLY_EQUAL = 165;
Token.OR = 166;
Token.PERCENT_EQUAL = 167;
Token.PLUS_EQUAL = 168;
Token.PLUS_PLUS = 169;
Token.RIGHT_SHIFT = 170;
Token.RIGHT_SHIFT_EQUAL = 171;
Token.SLASH_EQUAL = 172;
Token.TRIPLE_EQUALS = 176;
Token.UNSIGNED_RIGHT_SHIFT = 177;
Token.UNSIGNED_RIGHT_SHIFT_EQUAL = 178;

// Reserved words
Token.ASYNC = 201;
Token.AWAIT = 202;
Token.BREAK = 203;
Token.CASE = 204;
Token.CATCH = 205;
Token.CLASS = 206;
Token.CONST = 207;
Token.CONTINUE = 208;
Token.DEBUGGER = 209;
Token.DEFAULT = 210;
Token.DELETE = 211;
Token.DO = 212;
Token.ELSE = 213;
Token.ENUM = 214;
Token.EXPORT = 215;
Token.EXTENDS = 216;
Token.FALSE = 217;
Token.FINALLY = 218;
Token.FOR = 219;
Token.FROM = 220;
Token.FUNCTION = 221;
Token.IF = 222;
Token.IMPLEMENTS = 223;
Token.IMPORT = 224;
Token.IN = 225;
Token.INSTANCEOF = 226;
Token.INTERFACE = 227;
Token.LET = 228;
Token.NEW = 229;
Token.NULL = 230;
Token.PACKAGE = 231;
Token.PRIVATE = 232;
Token.PROTECTED = 233;
Token.PUBLIC = 234;
Token.RETURN = 235;
Token.SUPER = 236;
Token.SWITCH = 237;
Token.THIS = 238;
Token.THROW = 239;
Token.TRUE = 240;
Token.TRY = 241;
Token.TYPEOF = 242;
Token.UNDEFINED = 243;
Token.VAR = 244;
Token.VOID = 245;
Token.WHILE = 246;
Token.WITH = 247;
Token.YIELD = 248;
