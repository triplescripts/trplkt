import { ProgramText } from "./ProgramText.src";
import { Token } from "./Token.src";
import { TokenScanner } from "./TokenScanner.src";

export
class SourceProcessor {
  constructor(text) {
    // NB: must be intialized before glider or scanner can do any work--and
    // they start doing a little work before our constructor even finishes,
    // so it makes the most sense to initialize this now.
    this._tokenLines = [];

    this._reifiedToken = null;
    this._tokenErrorReason = null;
    this._tokenKind = null;
    this._tokenStart = null;
    this._tokenStop = null;

    this._glider = new ProgramText(text);
    this._glider.onlinestart = ((line) => { this._recordLine(line); });

    this._scanner = new TokenScanner(this._glider);
    this._scanner.ontoken = ((...args) => { this._recordToken(...args) });
    this._scanner.onerror = ((...args) => { this._recordJunk(...args) });
    this._scanner.keepWhitespace = true;
  }

  _recordLine(line) {
    this._tokenLines.push(line);
  }

  _recordToken(kind, begin, end) {
    this._tokenKind = kind;
    this._tokenStart = begin;
    this._tokenStop = end;
    this._reifiedToken = null;
  }

  _recordJunk(which, where, scanner) {
    this._recordToken(Token.UNEXPECTED_JUNK, where, this.position);
    this._tokenErrorReason = which;
  }

  get error() {
    return this._tokenErrorReason;
  }

  get token() {
    if (!this._reifiedToken && this._tokenKind) {
      let content = this.text.substring(this._tokenStart, this._tokenStop);
      let token = new Token(this._tokenKind, this._tokenStart, content);
      this._reifiedToken = token;
    }
    return this._reifiedToken;
  }

  nextToken(allowWhitespace) {
    // NB: The scanner is already configured to include whitespace tokens, and
    // we control whether the caller sees them by filtering them here locally.
    do {
      this._tokenLines.length = 0;
      var kind = this._scanner.readNextToken();
    } while (kind == Token.WHITESPACE && !allowWhitespace);

    if (kind == null) {
      // Scanner will not dispatch ontoken for EOF, so we handle it ourselves.
      this._recordToken(null, this.position, this.position);
    } else if (kind != Token.WHITESPACE) {
      this._doMultilineValidation(
        this._tokenStart, this._tokenStop, this._tokenLines
      );
    }

    return kind;
  }

  _doMultilineValidation(start, stop, lines) {
    for (let i = 0, n = lines.length; i < n; ++i) {
      if (lines[i].start > start) {
        let run = this.position - lines[i].start;
        check(run, this, lines[i], `/// `)
        check(run, this, lines[i], `//? `)
        check(run, this, lines[i], `import`)
      }
    }

    function check(run, source, line, what) {
      if (run >= what.length) {
        if (source.text.substr(line.start, what.length) == what) {
          throw new SourceProcessor.LineViolation(source.token);
        }
      }
    }
  }

  isAtEOF() {
    return this._tokenKind == null && this.position >= this.text.length;
  }

  get position() {
    return this._scanner.position;
  }

  get text() {
    // Implements text interface (`charCodeAt`, `length`, `toString`...)
    return this._glider;
  }

  isAtNewStanza() {
    return (this.getMarginTail() != null);
  }

  getMarginTail() {
    if (this._tokenKind != Token.WHITESPACE && !this.isAtEOF() &&
        this._glider.lines.length > 2) {
      let text = this._glider;
      let current = this.findLineForTokenPosition(this._tokenStart);
      let before = this.findLineForTokenPosition(current.start - 1);
      // assert(before.token != null)
      if (isLineEmpty(before, current)) {
        return current.token;
      }

      function isLineEmpty(first, next) {
        // assert(SourceProcessor.prototype.isNewLine.call(null, first.token))
        return (next.start - first.start == first.token.content.length);
      }
    }
    return null;
  }

  findLineForTokenPosition(where) {
    let index = ProgramText.getLineNumber(where, this._glider.lines) - 1;
    if (index >= 0) {
      return this._glider.lines[index];
    }
    return null;
  }

  isNewLine(token = this.token) {
    const { UNIX_STYLE } = ProgramText;
    if (token && token.kind == Token.WHITESPACE) {
      return ProgramText.getLineConvention(token.content) >= UNIX_STYLE;
    }
    return false;
  }

  isLineFlush(token = this.token) {
    let line = this.findLineForTokenPosition(token.position);
    if (!line) throw new Error("Internal error");
    return (token.position == line.start);
  }
}

SourceProcessor.LineViolation = class LineViolation extends Error {
  constructor(token) {
    super("Illegal text placement");
    this.token = token;
  }
}
