import { Token } from "./Token.src";

export
class TokenScanner {
  // To understand the scanner implementation, it's a good idea to internalize
  // these three essential primitives that are used throughout:
  //
  // - tip
  //   The character at the scanner's current position
  // - move
  //   Same as next, but returns old tip while advancing the position
  // - next
  //   Same as move, but returns new tip while advancing the position
  //
  // Less commonly used:
  // - match
  //   A conditional move iff the tip matches; this advances the scanner, and
  //   the return value indicates which move occurred (null means no match)
  // - peek
  //   Looks ahead (by default) one character (not at the tip; the one
  //   *after*), without advancing; otherwise looks at the given position
  //
  // For the scanner's findings:
  // - emit
  //   Dispatches the ontoken callback
  // - eject
  //   Like emit, but for errors (dispatching onerror)
  //
  // Consumers can also use the return value from `readNextToken` for inline
  // monitoring.  (We eschew with creating `Token` objects ourselves to try
  // keeping GC churn rate low.  This may be a case of premature optimization,
  // but in any case we're still left with a pretty clean design overall.)
  constructor(text, onemit = null, oneject = null) {
    this.text = text;
    this.ontoken = onemit;
    this.onerror = oneject;

    this.oncomment = null;

    this.keepComments = false;
    this.keepWhitespace = false;

    this._characterCode = null;
    this._patternStart = null;

    this.keywords = TokenScanner.addKeywords(new Map);

    this.position = null;
    this.reseat(0);
  }

  static addKeywords(map) {
    const $ = ((kind, word) => void(map.set(word, kind)));

    $(Token.ASYNC, "async");
    $(Token.AWAIT, "await");
    $(Token.BREAK, "break");
    $(Token.CASE, "case");
    $(Token.CATCH, "catch");
    $(Token.CLASS, "class");
    $(Token.CONST, "const");
    $(Token.CONTINUE, "continue");
    $(Token.DEBUGGER, "debugger");
    $(Token.DEFAULT, "default");
    $(Token.DELETE, "delete");
    $(Token.DO, "do");
    $(Token.ELSE, "else");
    $(Token.ENUM, "enum");
    $(Token.EXPORT, "export");
    $(Token.EXTENDS, "extends");
    $(Token.FALSE, "false");
    $(Token.FINALLY, "finally");
    $(Token.FOR, "for");
    $(Token.FROM, "from");
    $(Token.FUNCTION, "function");
    $(Token.IF, "if");
    $(Token.IMPLEMENTS, "implements");
    $(Token.IMPORT, "import");
    $(Token.IN, "in");
    $(Token.INSTANCEOF, "instanceof");
    $(Token.INTERFACE, "interface");
    $(Token.LET, "let");
    $(Token.NEW, "new");
    $(Token.NULL, "null");
    $(Token.PACKAGE, "package");
    $(Token.PRIVATE, "private");
    $(Token.PROTECTED, "protected");
    $(Token.PUBLIC, "public");
    $(Token.RETURN, "return");
    $(Token.SUPER, "super");
    $(Token.SWITCH, "switch");
    $(Token.THIS, "this");
    $(Token.THROW, "throw");
    $(Token.TRUE, "true");
    $(Token.TRY, "try");
    $(Token.TYPEOF, "typeof");
    $(Token.UNDEFINED, "undefined");
    $(Token.VAR, "var");
    $(Token.VOID, "void");
    $(Token.WHILE, "while");
    $(Token.WITH, "with");
    $(Token.YIELD, "yield");

    return map;
  }

  reseat(value) {
    this.position = value;
    this._characterCode = (this.position < this.text.length) ?
      this.text.charCodeAt(this.position) :
      null;
  }

  get tip() {
    return this._characterCode;
  }

  _dispatch(handler, ...args) {
    if (handler) handler.apply(null, args);
  }

  readNextToken(keepWhitespace = this.keepWhitespace) {
    try {
      var where = this._skipWhitespace(keepWhitespace);
    } catch (ex) {
      return TokenScanner.UrgentError.handle(ex);
    }

    if (keepWhitespace && where >= 0) {
      return this._emit(Token.WHITESPACE, where);
    }

    // NB: ontoken not dispatched for EOF.
    if (!this.tip) return null;

    let start = this.position;
    let code = this._move();

    switch (code) {
      case Token.CLOSE_CURLY:
      case Token.CLOSE_PAREN:
      case Token.CLOSE_SQUARE:
      case Token.COLON:
      case Token.COMMA:
      case Token.OPEN_CURLY:
      case Token.OPEN_SQUARE:
      case Token.QUESTION_MARK:
      case Token.SEMICOLON:
      case Token.TILDE:
        return this._emit(code, start);
      break;
      case Token.AMPERSAND:
        if (this._match(Token.AMPERSAND)) {
          return this._emit(Token.AND, start);
        } else if (this._match(Token.EQUAL)) {
          return this._emit(Token.AMPERSAND_EQUAL, start);
        }
        return this._emit(Token.AMPERSAND, start);
      case Token.ASTERISK:
        if (this._match(Token.EQUAL)) {
          return this._emit(Token.ASTERISK_EQUAL, start);
        }
        return this._emit(Token.ASTERISK, start);
      case Token.BANG:
        if (this._match(Token.EQUAL)) {
          if (this._match(Token.EQUAL)) {
            return this._emit(Token.NOT_STRICTLY_EQUAL, start);
          }
          return this._emit(Token.NOT_EQUAL, start);
        }
        return this._emit(Token.BANG, start);
      case Token.BAR:
        if (this._match(Token.BAR)) {
          return this._emit(Token.OR, start);
        } else if (this._match(Token.EQUAL)) {
          return this._emit(Token.BAR_EQUAL, start);
        }
        return this._emit(Token.BAR, start);
      case Token.CARET:
        if (this._match(Token.EQUAL)) {
          return this._emit(Token.CARET_EQUAL, start);
        }
        return this._emit(Token.CARET, start);
      case Token.EQUAL:
        if (this._match(Token.EQUAL)) {
          if (this._match(Token.EQUAL)) {
            return this._emit(Token.TRIPLE_EQUALS, start);
          }
          return this._emit(Token.DOUBLE_EQUALS, start);
        } else if (this._match(Token.GREATER_THAN)) {
          return this._emit(Token.ARROW, start);
        }
        return this._emit(Token.EQUAL, start);
      case Token.GREATER_THAN:
        if (this._match(Token.EQUAL)) {
          return this._emit(Token.GREATER_OR_EQUAL, start);
        } else if (this._match(Token.GREATER_THAN)) {
          if (this._match(Token.EQUAL)) {
            return this._emit(Token.RIGHT_SHIFT_EQUAL, start);
          } else if (this._match(Token.GREATER_THAN)) {
            if (this._match(Token.EQUAL)) {
              return this._emit(Token.UNSIGNED_RIGHT_SHIFT_EQUAL, start);
            }
            return this._emit(Token.UNSIGNED_RIGHT_SHIFT, start);
          }
          return this._emit(Token.RIGHT_SHIFT, start);
        }
        return this._emit(Token.GREATER_THAN, start);
      case Token.LESS_THAN:
        if (this._match(Token.EQUAL)) {
          return this._emit(Token.LESS_OR_EQUAL, start);
        } else if (this._match(Token.LESS_THAN)) {
          if (this._match(Token.EQUAL)) {
            return this._emit(Token.LEFT_SHIFT_EQUAL, start);
          }
          return this._emit(Token.LEFT_SHIFT, start);
        }
        return this._emit(Token.LESS_THAN, start);
      case Token.MINUS:
        if (this._match(Token.EQUAL)) {
          return this._emit(Token.MINUS_EQUAL, start);
        } else if (this._match(Token.MINUS)) {
          return this._emit(Token.MINUS_MINUS, start);
        }
        return this._emit(Token.MINUS, start);
      case Token.OPEN_PAREN:
        // XXX Temporary hack to permit some PCRE-like literals while keeping
        // the lexer-to-parser relationship acyclical (i.e., there is no need
        // for feedback propagation from parser to scanner, e.g. by putting
        // the scanner into a different mode allowing patterns).  Under our
        // hacky scheme, pattern literals are permitted iff using parens.
        if (this.tip == Token.SLASH) {
          this._patternStart = this.position;
        }
        return this._emit(Token.OPEN_PAREN, start);
      case Token.PERCENT:
        if (this._match(Token.EQUAL)) {
          return this._emit(Token.PERCENT_EQUAL, start);
        }
        return this._emit(Token.PERCENT, start);
      case Token.PERIOD:
        if (this._peek(Token.PERIOD) && this._match(Token.PERIOD)) {
          this._next();
          return this._emit(Token.DOT_DOT_DOT, start);
        }
        return this._emit(Token.PERIOD, start);
      case Token.PLUS:
        if (this._match(Token.EQUAL)) {
          return this._emit(Token.PLUS_EQUAL, start);
        } else if (this._match(Token.PLUS)) {
          return this._emit(Token.PLUS_PLUS, start);
        }
        return this._emit(Token.PLUS, start);
      case Token.SLASH:
        if (this._patternStart != null) {
          return this._scanPattern(start);
        } else if (this._match(Token.SLASH)) {
          let isGeneric = !!this._match(Token.QUESTION_MARK);
          if ((isGeneric || this._match(Token.SLASH)) &&
               !this._match(Token.SLASH)) {
            return this._scanTripleSlashEntity(start, isGeneric);
          }
          this._skipSingleLineComment(start);
          return this._emit(Token.COMMENT, start, this.position);
        } else if (this._match(Token.ASTERISK)) {
          try {
            this._skipMultiLineComment(start);
          } catch (ex) {
            return TokenScanner.UrgentError.handle(ex);
          }
          return this._emit(Token.COMMENT, start, this.position);
        }
        return this._emit(Token.SLASH, start);
      case TokenScanner.BACKTICK:
        return this._scanTaggableThing(start);
      case TokenScanner.DOUBLE_QUOTE:
      case TokenScanner.SINGLE_QUOTE:
        return this._scanStringLiteral(start);
      default:
        if (TokenScanner.isIdentifierStart(code)) {
          return this._scanWord(start);
        } else if (TokenScanner.isDecimalDigit(code)) {
          return this._scanNumber(start);
        }
      break;
    }

    return this._eject(TokenScanner.ERROR_UNRECOGNIZED_TOKEN, start);
  }

  _skipWhitespace(atomicLinebreak = false, keepComments = this.keepComments) {
    // The return value indicates the token's start position.
    let where = this.position;

    while (this.tip) {
      let start = this.position;
      if (TokenScanner.isLineTerminator(this.tip)) {
        this._match(TokenScanner.CARRIAGE_RETURN);
        if (!this._match(TokenScanner.LINE_FEED)) {
          // XXX what can we do..?
        }
        if (atomicLinebreak) break;
      } else {
        while (TokenScanner.isWhitespace(this.tip, true)) {
          this._next();
        }
      }
      if (!keepComments) {
        while (this._skipPossibleComment()) { };
      }
      if (this.position == start) {
        break;
      }
    }

    if (this.position != where) {
      return where;
    }
    return -1;
  }

  static isWhitespace(code, inlineOnly = false) {
    return (
      code == TokenScanner.SPACE ||
      code == TokenScanner.TAB ||
      (!inlineOnly && TokenScanner.isLineTerminator(code))
    );
  }

  static isLineTerminator(code) {
    return (
      code == TokenScanner.LINE_FEED || code == TokenScanner.CARRIAGE_RETURN
    );
  }

  _skipPossibleComment() {
    // NB: Triple slash delimiters are *NOT* considered comments.
    let start = this.position;
    if (this._match(Token.SLASH)) {
      switch (this.tip) {
        case Token.SLASH:
          if (!this._peek(Token.QUESTION_MARK) &&
              this._next() != Token.SLASH) {
            return this._skipSingleLineComment(start), true;
          }
        break;
        case Token.ASTERISK:
          return this._skipMultiLineComment(start), true;
        break;
      }
      this.reseat(start);
    }
    return false;
  }

  _move() {
    let result = this.tip;
    this.reseat(this.position + 1);
    return result;
  }

  _next() {
    this.reseat(this.position + 1);
    return this.tip;
  }

  _match(which) {
    if (this.tip == which) {
      return this._move();
    }
    return null;
  }

  _peek(which, position = this.position + 1) {
    return this.text.charCodeAt(position) == which;
  }

  _skipSingleLineComment(from) {
    // NB: Entry into his method is permitted from any position, so long as
    // the correct offset where the comment starts is passed in.
    this.reseat(from + ("//").length);
    while (this.tip && !TokenScanner.isLineTerminator(this.tip)) {
      this._next();
    }
    this._dispatch(this.oncomment, from, this.position, this);
  }

  // NB: This can throw a `TokenScanner.UrgentError`, which should be caught
  // and handled somewhere up the stack.
  _skipMultiLineComment(from) {
    // NB: Entry into his method is permitted from any position, so long as
    // the correct offset where the comment starts is passed in.
    let finished = false;
    this.reseat(from + ("/*").length);
    while (this.tip) {
      if (this._match(Token.ASTERISK)) {
        if (this._match(Token.SLASH)) {
          finished = true;
          break;
        }
      } else {
        this._next();
      }
    }

    if (!finished) {
      throw new TokenScanner.UrgentError(
        this._eject.bind(this), TokenScanner.ERROR_COMMENT_ABRUPT_END, from
      );
    }

    this._dispatch(this.oncomment, from, this.position, this);
  }

  _emit(kind, begin, end = this.position) {
    this._dispatch(this.ontoken, kind, begin, end, this.text, this);
    return kind;
  }

  _eject(which, where) {
    this._dispatch(this.onerror, which, where, this);
    return TokenScanner.ERROR;
  }

  _scanStringLiteral(offset) {
    let terminator = this.text.charCodeAt(offset);
    let reason = this._skipStringContents(terminator);
    if (!reason) {
      return this._emit(Token.STRING_LITERAL, offset, this.position);
    }
    return this._eject(reason, offset);
  }

  _skipStringContents(terminator) {
    while (!this._match(terminator)) {
      if (TokenScanner.isLineTerminator(this.tip) || !this.tip) {
        return TokenScanner.ERROR_STRING_ABRUPT_END;
      } else if (this._move() == TokenScanner.BACKSLASH &&
                 !this._skipStringEscapeSequence()) {
        return TokenScanner.ERROR_INVALID_STRING_ESCAPE;
      }
    }

    return null;
  }

  _scanTaggableThing(offset) {
    let reason = null;
    do {
      switch (this._move()) {
        case TokenScanner.BACKSLASH:
          if (!this._skipStringEscapeSequence(true)) {
            reason = TokenScanner.ERROR_INVALID_STRING_ESCAPE; // XXX "string"
          }
        break;
        case TokenScanner.DOLLAR_SIGN:
          if (this._match(Token.OPEN_CURLY)) {
            let terminator; // TODO moar
            if (this._match(TokenScanner.SINGLE_QUOTE)) {
              terminator = TokenScanner.SINGLE_QUOTE;
            } else if (this._match(TokenScanner.DOUBLE_QUOTE)) {
              terminator = TokenScanner.DOUBLE_QUOTE;
            } else {
              reason = TokenScanner.ERROR_MALFORMED_SEMISTATIC_CONTENT;
            }

            if (terminator) {
              reason = this._skipStringContents(terminator);
              if (!reason && !this._match(Token.CLOSE_CURLY)) {
                reason = TokenScanner.ERROR_MALFORMED_SEMISTATIC_CONTENT;
              }
            }
          }
        break;
        case TokenScanner.EOF:
          reason = TokenScanner.ERROR_SEMISTATIC_ABRUPT_END;
        break;
        case TokenScanner.BACKTICK:
          return this._emit(Token.SEMISTATIC_FORM, offset, this.position);
        break;
      }
    } while (!reason);

    return this._eject(reason, offset);
  }

  // NB: We do not emit or eject here; we just return a boolean indicating
  // validity.
  _skipStringEscapeSequence(interpolatingContext = false) {
    switch (this.tip) {
      case TokenScanner.CARRIAGE_RETURN:
        this._move() && this._match(TokenScanner.LINE_FEED);
      return true;

      case TokenScanner.LINE_FEED:
      // -
      case TokenScanner.BACKSLASH:
      case TokenScanner.BACKTICK:
      case TokenScanner.DOUBLE_QUOTE:
      case TokenScanner.SINGLE_QUOTE:
      // -
      case TokenScanner.LOWER_N:
      case TokenScanner.LOWER_R:
      case TokenScanner.LOWER_T:
      case TokenScanner.DIGIT_0:
        this._next();
      return true;

      default:
        if (interpolatingContext && this._match(Token.OPEN_CURLY)) {
          return true;
        } else if (this._match(TokenScanner.LOWER_X)) {
          return this._matchHexDigit(true) && this._matchHexDigit(true);
        } else if (this._match(TokenScanner.LOWER_U)) {
          if (this._match(Token.OPEN_CURLY)) {
            while (TokenScanner.isHexDigit(this.tip, true)) {
              this._next();
            }
            return this._match(Token.CLOSE_CURLY);
          } else {
            return (
              this._matchHexDigit(true) && this._matchHexDigit(true) &&
              this._matchHexDigit(true) && this._matchHexDigit(true)
            );
          }
        }
      break;
    }

    return false;
  }

  _matchHexDigit(anyCase) {
    if (TokenScanner.isHexDigit(this.tip, anyCase)) {
      this._next();
      return true;
    }
    return false;
  }

  _scanTripleSlashEntity(offset, isGStyle) {
    if (this._match(TokenScanner.SPACE)) {
      if (this._match(Token.LESS_THAN)) {
        let kind = (this._match(Token.SLASH)) ?
          Token.CLOSE_DELIMITER :
          Token.OPEN_DELIMITER;
        if (this._match(TokenScanner.LOWER_S) &&
            this._match(TokenScanner.LOWER_C) &&
            this._match(TokenScanner.LOWER_R) &&
            this._match(TokenScanner.LOWER_I) &&
            this._match(TokenScanner.LOWER_P) &&
            this._match(TokenScanner.LOWER_T) &&
            this._match(Token.GREATER_THAN)) {
          return this._emit(kind, offset, this.position);
        }
      } else if (TokenScanner.isIdentifierStart(this.tip) && !isGStyle) {
        let nameStart = this.position;
        do {
          this._move();
        } while (TokenScanner.isIdentifierPart(this.tip));

        if (this._hasAttributeMarkers(nameStart)) {
          return this._emit(Token.ATTRIBUTE_NAME, offset);
        }

        let kind = this._mapTextExtractionToKeywordDiscriminant(nameStart);
        if (kind == Token.IMPORT || kind == Token.EXPORT) {
          return this._emit(kind, offset);
        }
      }
    }

    return this._eject(TokenScanner.ERROR_INVALID_TRIPLE_SLASH, offset);
  }

  // NB: returns undefined if not a keyword
  _mapTextExtractionToKeywordDiscriminant(begin, end = this.position) {
    return this.keywords.get(this.text.substring(begin, end));
  }

  _hasAttributeMarkers(begin, end = this.position) {
    return (
      this._peek(TokenScanner.DOLLAR_SIGN, begin) && (end - begin > 2) &&
      this._peek(TokenScanner.DOLLAR_SIGN, end - 1)
    );
  }

  _scanPattern(offset) {
    while (this.tip) {
      if (this._match(Token.SLASH)) {
        while (TokenScanner.LOWER_A <= this.tip &&
               this.tip <= TokenScanner.LOWER_Z) {
          this._next();
        }
        break;
      } else if (TokenScanner.isLineTerminator(this.tip)) {
        return this._eject(TokenScanner.ERROR_INCOMPLETE_PATTERN, offset);
      }
      this._next();
    }

    this._patternStart = null;
    return this._emit(Token.PATTERN_LITERAL, offset);
  }

  _scanWord(offset) {
    // TODO Allow Unicode escape sequences in identifiers?  (But not for
    // keywords.)
    while (TokenScanner.isIdentifierPart(this.tip)) {
      this._move();
    }

    if (this._hasAttributeMarkers(offset)) {
      return this._emit(Token.ATTRIBUTE_NAME, offset);
    }

    // TODO Optimize this?
    let kind = this._mapTextExtractionToKeywordDiscriminant(offset);
    return this._emit(kind || Token.IDENTIFIER_NAME, offset, this.position);
  }

  static isIdentifierStart(code) {
    return (
      (TokenScanner.UPPER_A <= code && code <= TokenScanner.UPPER_Z) ||
      (TokenScanner.LOWER_A <= code && code <= TokenScanner.LOWER_Z) ||
      code == TokenScanner.UNDERSCORE || code == TokenScanner.DOLLAR_SIGN
    );
    // TODO Unicode
  }

  static isIdentifierPart(code) {
    return (
      TokenScanner.isIdentifierStart(code) ||
      TokenScanner.isDecimalDigit(code)
    );
    // TODO Unicode
  }

  static isDecimalDigit(code) {
    return TokenScanner.DIGIT_0 <= code && code <= TokenScanner.DIGIT_9;
  }

  _scanNumber(offset) {
    let hasLeadingZero = this._peek(TokenScanner.DIGIT_0, offset);
    // TODO dispatch onnumbercharacter
    if (hasLeadingZero && this._match(TokenScanner.LOWER_X)) {
      let mark = this.position;
      if (TokenScanner.isHexDigit(this.tip)) {
        do {
          this._next();
        } while (TokenScanner.isHexDigit(this.tip));
      } else {
        return this._eject(TokenScanner.ERROR_HEXADECIMAL_DIGIT_EXPECTED);
      }

      let width = this.position - mark;
      if (width == 2 || width == 4 || width == 8) {
        return this._emit(Token.NUMBER_LITERAL, offset);
      }
    } else {
      if (TokenScanner.isDecimalDigit(this.tip)) {
        if (hasLeadingZero) {
          return this._eject(TokenScanner.ERROR_INVALID_NUMBER_LITERAL, offset);
        }

        do {
          this._next();
        } while (TokenScanner.isDecimalDigit(this.tip));
      }

      if (this._match(Token.PERIOD)) {
        if (TokenScanner.isDecimalDigit(this.tip)) {
          do {
            this._next();
          } while (TokenScanner.isDecimalDigit(this.tip));
        } else {
          return this._eject(TokenScanner.ERROR_DECIMAL_DIGIT_EXPECTED);
        }
      }
      if (this._match(TokenScanner.LOWER_E)) {
        if (this.tip == Token.PLUS || this.tip == Token.MINUS) {
          this._next();
        }
        if (TokenScanner.isDecimalDigit(this.tip)) {
          do {
            this._next();
          } while (TokenScanner.isDecimalDigit(this.tip));
        } else {
          return this._eject(TokenScanner.ERROR_DECIMAL_DIGIT_EXPECTED);
        }
      }

      return this._emit(Token.NUMBER_LITERAL, offset);
    }

    return this._eject(TokenScanner.ERROR_INVALID_NUMBER_LITERAL, offset);
  }

  static isHexDigit(code, anyCase = false) {
    const ASCII = TokenScanner;
    return (
      (ASCII.DIGIT_0 <= code && code <= ASCII.DIGIT_9) ||
      (ASCII.UPPER_A <= code && code <= ASCII.UPPER_F) ||
      (ASCII.LOWER_A <= code && code <= ASCII.LOWER_F && anyCase)
    );
  }
}

TokenScanner.ERROR = 0;

TokenScanner.CARRIAGE_RETURN = 0x0D;
TokenScanner.LINE_FEED = 0x0A;
TokenScanner.SPACE = 0x20;
TokenScanner.TAB = 0x09;

TokenScanner.DOLLAR_SIGN = 0x24;
TokenScanner.DOUBLE_QUOTE = 0x22;
TokenScanner.SINGLE_QUOTE = 0x27;
TokenScanner.BACKSLASH = 0x5C;
TokenScanner.UNDERSCORE = 0x5F;
TokenScanner.BACKTICK = 0x60;

TokenScanner.UPPER_A = 0x41;
TokenScanner.UPPER_F = 0x46;
TokenScanner.UPPER_Z = 0x5A;
TokenScanner.LOWER_A = 0x61;
TokenScanner.LOWER_C = 0x63;
TokenScanner.LOWER_E = 0x65;
TokenScanner.LOWER_F = 0x66;
TokenScanner.LOWER_I = 0x69;
TokenScanner.LOWER_N = 0x6E;
TokenScanner.LOWER_P = 0x70;
TokenScanner.LOWER_R = 0x72;
TokenScanner.LOWER_S = 0x73;
TokenScanner.LOWER_T = 0x74;
TokenScanner.LOWER_U = 0x75;
TokenScanner.LOWER_X = 0x78;
TokenScanner.LOWER_Z = 0x7A;

TokenScanner.DIGIT_0 = 0x30;
TokenScanner.DIGIT_9 = 0x39;

TokenScanner.EOF = null;

// XXX Need to separate l10n concerns from mux/demux concerns...
TokenScanner.ERROR_COMMENT_ABRUPT_END = "Unterminated block comment";
TokenScanner.ERROR_DECIMAL_DIGIT_EXPECTED = "Decimal digit expected";
TokenScanner.ERROR_HEXADECIMAL_DIGIT_EXPECTED = "Hexadecimal digit expected";
TokenScanner.ERROR_INVALID_NUMBER_LITERAL = "Invalid numeric literal";
TokenScanner.ERROR_INVALID_STRING_ESCAPE = "Invalid string escape sequence";
TokenScanner.ERROR_INVALID_TRIPLE_SLASH = "Invalid triple slash entity";
TokenScanner.ERROR_MALFORMED_SEMISTATIC_CONTENT = "Malformed semistatic form";
TokenScanner.ERROR_STRING_ABRUPT_END = "Unterminated string literal";
TokenScanner.ERROR_SEMISTATIC_ABRUPT_END = "Unterminated template";
TokenScanner.ERROR_UNRECOGNIZED_TOKEN = "Unrecognized token";

// For internal use only
TokenScanner.UrgentError = class UrgentError extends Error {
  constructor(ejector, reason, where) {
    super(reason);
    this.ejector = ejector;
    this.which = reason;
    this.where = where;
  }

  static handle(ex) {
    if (ex instanceof TokenScanner.UrgentError) {
      return ex.ejector.call(null, ex.which, ex.where);
    }
    throw ex;
  }
}
